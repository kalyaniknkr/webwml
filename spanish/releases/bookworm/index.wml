#use wml::debian::template title="Información sobre Debian &ldquo;bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="4a4923573595a8539cd9aca5189940d45bcbbb29"

<p>Debian <current_release_bookworm> se
publicó el <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 se publicó inicialmente el <:=spokendate('2023-06-10'):>."
/>
Esta versión incluía muchos cambios
importantes, que se describen en
nuestra <a href="$(HOME)/News/2023/20230610">nota de prensa</a> y
en las <a href="releasenotes">notas de publicación</a>.</p>

# <p><strong>Debian 12 ha sido reemplazada por
# <a href="../trixie/">Debian 13 (<q>trixie</q>)</a>.
# Las actualizaciones de seguridad han dejado de proporcionarse el <:=spokendate('2026-06-10'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
# <p><strong>Sin embargo, bookworm se beneficia del soporte a largo plazo (LTS, por sus siglas en inglés) hasta
# el 30 de junio de 2028. El LTS está limitado a i386, amd64, armel, armhf y arm64.
# El resto de arquitecturas ya no están soportadas en bookworm.
# Para más información, consulte la <a
# href="https://wiki.debian.org/LTS">sección LTS de la wiki de Debian</a>.
# </strong></p>

<p>Para obtener e instalar Debian, consulte
la página con <a href="debian-installer/">información para la instalación</a> y la
<a href="installmanual">guía de instalación</a>. Para actualizar desde una versión
anterior de Debian, consulte las instrucciones incluidas en las
<a href="releasenotes">notas de publicación</a>.</p>

### Activate the following when LTS period starts.
#<p>Arquitecturas soportadas durante el periodo de soporte a largo plazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Arquitecturas soportadas cuando se publicó inicialmente bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>En contra de nuestros deseos, puede haber algunos problemas en esta
versión, a pesar de haber sido declarada <em>estable</em>. Hemos hecho
<a href="errata">una lista de los principales problemas conocidos</a>, y siempre puede
<a href="../reportingbugs">informarnos de otros</a>.</p>

<p>Por último, pero no menos importante, tenemos una lista de las <a href="credits">personas que han
contribuido</a> a hacer posible esta publicación.</p>
