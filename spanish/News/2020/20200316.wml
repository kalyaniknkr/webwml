<define-tag pagetitle>Canales oficiales de comunicación de Debian</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="c1e997409ebbfee3f612efc60c0f1e3fe81c7e9c" maintainer="Laura Arjona Reina"

<p>
De cuando en cuando, recibimos preguntas en Debian acerca de nuestros canales
oficiales de comunicación y consultas acerca de la relación con Debian de
quien gestiona sitios web con nombres parecidos.
</p>

<p>
El principal sitio web de Debian <a href="https://www.debian.org">www.debian.org</a>
es nuestro medio de comunicación primordial. 
Aquellos que busquen información sobre lo que sucede actualmente y lo que está
en desarrollo en la comunidad de Debian puede que estén interesados en la sección
de
<a href="https://www.debian.org/News/">noticias del proyecto Debian</a> en la web.

Para anuncios menos formales, tenemos el blog oficial de Debian
<a href="https://bits.debian.org">Bits from Debian</a>,
y el servicio de <a href="https://micronews.debian.org">micronoticias de Debian, «Debian micronews»</a>
para noticias más cortas (publicadas en inglés).
</p>

<p>
Nuestro boletín oficial 
<a href="https://www.debian.org/News/weekly/">Noticias del proyecto Debian</a>
y todos los anuncios oficiales de noticias o cambios en el proyecto se publican
en nuestro sitio web y se envían a nuestras listas oficiales
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> o
<a href="https://lists.debian.org/debian-news/">debian-news</a>.
El envío a esas listas de correo está restringido.
</p>

<p>
También queremos aprovechar la oportunidad para anunciar cómo el proyecto Debian,
o para acortar, Debian, está estructurado.
</p>

<p>
Debian tiene una estructura regulada por nuestra
<a href="https://www.debian.org/devel/constitution">constitución</a>.
Los directores y miembros delegados están listados en la página sobre
<a href="https://www.debian.org/intro/organization">estructura organizativa</a>.
Los equipos adicionales están listados en nuestra página wiki de <a href="https://wiki.debian.org/Teams">Equipos</a>.
</p>

<p>
La lista completa de miembros oficiales de Debian puede encontrarse en nuestra
<a href="https://nm.debian.org/members">página de nuevos miembros</a>,
donde se gestiona la membresía. Una lista más amplia de contribuidores a Debian
puede encontrarse en nuestra página de
<a href="https://contributors.debian.org">contribuidores</a>.
</p>

<p>
Si tiene preguntas, le invitamos a contactar con el equipo de prensa en
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>Acerca de Debian</h2>
<p>El proyecto Debian es una asociación de desarrolladores de software libre 
que aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema
operativo Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>
<p>Para más información, visite las páginas web de Debian en 
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
