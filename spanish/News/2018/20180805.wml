#use wml::debian::translation-check translation="dded51c1e8b9c3bb765ff6b90a7689d19fa1942b"
<define-tag pagetitle>Clausura de DebConf18 en Hsinchu y anuncio de fechas para DebConf19</define-tag>

<define-tag release_date>2018-08-05</define-tag>
#use wml::debian::news

<p>
Hoy domingo, 5 de agosto de 2018, se ha clausurado la conferencia anual de desarrolladores
y contribuidores de Debian.
Con más de 306 asistentes de todo el mundo
y 137 eventos, incluyendo 100 charlas, 25 sesiones de debate o BoFs,
5 talleres y otras 7 actividades,
<a href="https://debconf18.debconf.org">DebConf18</a> ha sido aclamada como un éxito.
</p>

<p>
Entre lo más destacado se encuentra el DebCamp, con más de 90 participantes,
la <a href="https://debconf18.debconf.org/schedule/?day=2018-07-28">jornada de puertas abiertas</a>, 
en la que se celebraron actividades de interés para una audiencia más amplia,
sesiones plenarias como los tracicionales «bits» del líder del proyecto Debian,
una sesión de preguntas y respuestas con la ministra Audrey Tang,
un panel de debate sobre «ignorar la negatividad» («Ignoring negativity»), con Bdale Garbee, Chris Lamb, Enrico Zini y Steve McIntyre,
la charla «That's a free software issue!!» («¡¡Ese es un problema del software libre!!») impartida por Molly de Blanc y Karen Sandler,
charlas relámpago y demos en vivo,
y el anuncio de la DebConf del próximo año
(<a href="https://wiki.debian.org/DebConf/19">DebConf19</a> en Curitiba, Brasil).
</p>

<p>
La <a href="https://debconf18.debconf.org/schedule/">programación</a>
se ha actualizado a diario, incluyendo 27 nuevas actividades ad-hoc planificadas
por asistentes a la conferencia durante el desarrollo de la misma.
</p>

<p>
Para quienes no pudieron asistir, la mayoría de las charlas y sesiones se grabaron y se retransmitieron en directo;
además, se han puesto vídeos a disposición del público en el
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2018/DebConf18/">archivo web de encuentros Debian</a>.
Muchas sesiones permitieron también la participación remota a través de IRC o de un documento de texto colaborativo.
</p>

<p>
El sitio web <a href="https://debconf18.debconf.org/">DebConf18</a>
permanecerá activo como archivo, y continuará ofreciendo
enlaces a las presentaciones y vídeos de charlas y de actividades.
</p>

<p>
El próximo año, <a href="https://wiki.debian.org/DebConf/19">DebConf19</a> tendrá lugar en Curitiba, Brasil, entre 
el 21 y el 28 de julio de 2019. Será la segunda DebConf celebrada en Brasil (la primera fue DebConf4 en
Puerto Alegre).
Los organizadores locales volverán a instalar el DebCamp, una sesión de trabajo intenso sobre
la mejora de la distribución, para los días previos a la DebConf
(del 13 al 19 de julio),
y organizarán la jornada de puertas abiertas, dirigida al público en general, el 20 de julio de 2019.
</p>

<p>
Debconf está comprometida con el establecimiento de un ambiente seguro y acogedor para todos los participantes.
Vea el <a href="https://debconf.org/codeofconduct.shtml">código de conducta de la DebConf</a> 
y el <a href="https://www.debian.org/code_of_conduct">código de conducta de Debian</a> para más detalles.
</p>

<p>
Debian agradece el compromiso de numerosos
<a href="https://debconf18.debconf.org/sponsors/">patrocinadores</a> 
por su apoyo a DebConf18, especialmente el de nuestro patrocinador platino:
<a href="http://www.hpe.com/engage/opensource">Hewlett Packard Enterprise</a>.
</p>


<h2>Acerca de Debian</h2>

<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser
un proyecto comunitario verdaderamente libre. Desde entonces el proyecto
ha crecido hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios de todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido a 70 idiomas y soporta
una gran cantidad de arquitecturas de ordenadores, por lo que el proyecto
se refiere a sí mismo como <q>el sistema operativo universal</q>.
</p>

<h2>Acerca de DebConf</h2>

<p>DebConf es la conferencia de desarrolladores del proyecto Debian. Además de un
amplio programa de charlas técnicas, sociales y sobre reglamentación, DebConf proporciona una
oportunidad para que desarrolladores, contribuidores y otras personas interesadas se
conozcan en persona y colaboren más estrechamente. Se ha celebrado
anualmente desde 2000 en lugares tan diversos como Escocia, Argentina y
Bosnia-Herzegovina. Hay más información sobre DebConf disponible en
<a href="https://debconf.org/">https://debconf.org</a>.</p>

<h2>Acerca de Hewlett Packard Enterprise</h2>

<p>
<a href="http://www.hpe.com/engage/opensource">Hewlett Packard Enterprise (HPE)</a> 
es una empresa tecnológica líder de la industria,
que dispone de un extenso catálogo de productos tales como
sistemas integrados, servidores, almacenamiento, redes de comunicaciones y software.
La empresa ofrece consultoría, soporte operativo, servicios financieros
y soluciones completas para muchas industrias diferentes: móviles e IdC, 
datos &amp; análisis, manufactura o sectores públicos entre otras.
</p>

<p>
HPE es también socio de desarrollo de Debian
y proporciona hardware para el desarrollo de adaptaciones («ports»), réplicas de Debian y otros servicios de Debian
(en la página de <a href="https://db.debian.org/machines.cgi">máquinas del desarrollador de debian.org</a> están listadas las donaciones de hardware).
</p>

<h2>Información de contacto</h2>

<p>Para más información, visite la página web de DebConf18 en
<a href="https://debconf18.debconf.org/">https://debconf18.debconf.org/</a>
o envíe un correo electrónico a &lt;press@debian.org&gt;.</p>
