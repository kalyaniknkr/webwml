#use wml::debian::template title="Informationen zur Debian-<q>Trixie</q>-Veröffentlichung"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="ab3c0fa63d12dbcc8e7c3eaf4a72beb7b56d9741"

<if-stable-release release="trixie">

<p>Debian <current_release_trixie> wurde am
<a href="$(HOME)/News/<current_release_newsurl_trixie/>"><current_release_date_trixie></a> veröffentlicht.
<ifneq "13.0" "<current_release>"
  "(Debian 13.0 wurde ursprünglich am <:=spokendate('XXXXXXXX'):> freigegeben."
/>
Diese Veröffentlichung enthält größere Änderungen, die in
unserer <a href="$(HOME)/News/XXXX/XXXXXXXX">Pressemitteilung</a> und
in den <a href="releasenotes">Veröffentlichungshinweisen</a> beschrieben
sind.</p>

#<p><strong>Debian 13 wurde durch
#<a href="../forky/">Debian 14 (<q>Forky</q>)</a> abgelöst.
#Sicherheitsaktualisierungen wurden am <:=spokendate('xxxx-xx-xx'):> eingestellt.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Trixie profitiert jedoch zusätzlich bis Ende xxxxx 20xx vom Long Term Support (LTS), d. h.
#bis zu diesem Zeitpunkt werden Sicherheits-Updates für Trixie bereitgestellt.
#Dies ist allerdings beschränkt auf i386, amd64, armel, armhf und arm64. Weitere Informationen
#hierzu finden Sie im <a
#href="https://wiki.debian.org/LTS">LTS-Abschnitt des Debian-Wikis</a>.
#</strong></p>

<p>Um Debian zu beschaffen und zu installieren, lesen Sie die
<a href="debian-installer/">Webseite zum Debian-Installer</a> und die <a
href="installmanual">Installationsanleitung</a>. Wenn Sie ein Upgrade von einer
älteren Debian-Veröffentlichung durchführen möchten, lesen Sie die Anleitung in den
<a href="releasenotes">Veröffentlichungshinweisen</a>.</p>

### Activate the following when LTS period starts.
#<p>Während der Long-Term-Support-Periode unterstützte Architekturen:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Zur ursprünglichen Freigabe von Trixie unterstützte Architekturen:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Entgegen unseren Wünschen könnte es in der Veröffentlichung noch einige
Probleme geben, obwohl sie als <em>stabil</em> deklariert
wurde. Wir haben <a href="errata">eine Liste bekannter größerer Probleme</a>
erstellt und Sie können uns jederzeit <a href="../reportingbugs">weitere
Probleme berichten</a>.</p>

<p>Zu guter Letzt finden Sie hier auch <a href="credits">eine Liste der Personen,
denen Dank dafür gebührt</a>, dass diese Veröffentlichung erfolgen konnte.</p>
</if-stable-release>

<if-stable-release release="bookworm">

<p>Der Codename für das nächste große Debian-Release nach
<a href="../bookworm/">Bookworm</a> ist <q>Trixie</q>.</p>

<p>Diese Veröffentlichung hat als eine Kopie von Bookworm angefangen und befindet
sich im Augenblick in einem Status, der
<q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">Testing</a></q> genannt
wird. Das bedeutet, dass Probleme nicht so gravierend sein dürften wie in
der Unstable- oder Experimental-Distribution, da Pakete nur nach einer
bestimmten Zeitperiode und ohne berichtete veröffentlichungskritische Fehler
einfließen dürfen.</p>

<p>Bitte beachten Sie, dass Sicherheitsaktualisierungen für die
<q>Testing</q>-Distribution noch <strong>nicht</strong> vom Sicherheitsteam betreut
werden. Daher erhält <q>Testing</q> die Sicherheitsaktualisierungen
<strong>nicht</strong> auf zeitgerechte Weise.
#Für weitere Informationen lesen Sie bitte die
#<a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">Ankündigung</a>
#des Testing-Sicherheitsteams.
Falls Sie Sicherheitsunterstützung benötigen, wird Ihnen deshalb empfohlen,
die Einträge in Ihrer sources.list
von <q>testing</q> auf <q>bookworm</q> umzustellen. Lesen Sie dazu auch den Eintrag in der
<a href="$(HOME)/security/faq#testing">FAQ des Sicherheitsteams</a> für
die <q>Testing</q>-Distribution.</p>

<p>Es gibt evtl. einen <a href="releasenotes">Entwurf für die Veröffentlichungshinweise</a>. Bitte <a
href="https://bugs.debian.org/release-notes">überprüfen Sie auch die
vorgeschlagenen Änderungen für die Veröffentlichungshinweise</a>.</p>

<p>Hinsichtlich Installations-Images und Dokumentation über die Installation
von <q>Testing</q> schauen Sie bitte auf die Seite zum
<a href="$(HOME)/devel/debian-installer/">Debian-Installer</a>.</p>

<p>Um mehr darüber herauszufinden, wie die <q>Testing</q>-Distribution arbeitet, werfen Sie
einen Blick auf <a href="$(HOME)/devel/testing">die Informationen für
Entwickler</a>.</p>

<p>Es wird oft gefragt, ob es einen <q>Fortschrittszähler</q> für das Release gebe.
Unglücklicherweise gibt es keinen, aber wir können auf verschiedene Dinge
verweisen, die abgehandelt werden müssen, bevor das Release freigegeben werden kann:</p>

<ul>
<li><a href="https://release.debian.org/">Allgemeine Seite zum Status des
 Releases</a></li>
<li><a href="https://bugs.debian.org/release-critical/">Veröffentlichungskritische
 Fehler</a></li>
<li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Fehler im
 Basissystem</a></li>
<li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Fehler in
 standard- und task-Paketen</a></li>
</ul>

<p>Zusätzlich werden allgemeine Status-Berichte vom Release-Manager an die
<a href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce-Mailingliste</a> geschickt.</p>

</if-stable-release>
