#use wml::debian::template title="Wie Sie an das Debian-Projekt spenden können" MAINPAGE="true"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847"
# Translator: Thimo Neubauer <thimo@debian.org>
# Updated from scratch: Holger Wansing <linux@wansing-online.de>, 2014 - 2017.
# Translation update: Holger Wansing <hwansing@mailbox.org>, 2019, 2023.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Vielen Dank an alle Sponsoren, die Debian mit Ausrüstung oder Diensten unterstützen: 
         <a href="https://db.debian.org/machines.cgi">Hosting- und Hardware-Sponsoren</a>,
         <a href="mirror/sponsors">Spiegel-Sponsoren</a>,
         <a href="partners/">Entwicklungs- und Service-Partner</a>.<p>
</aside>

<p>
Spenden werden vom <a href="$(HOME)/devel/leader">Debian-Projektleiter</a> (DPL) verwaltet und
ermöglichen Debian die Finanzierung von <a href="https://db.debian.org/machines.cgi">Maschinen</a>,
<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">weiterer Hardware</a>, Domains,
Verschlüsselungszertitikaten, <a href="https://www.debconf.org">der Debian-Konferenz</a>,
<a href="https://wiki.debian.org/MiniDebConf">Debian Mini-Konferenzen</a>,
<a href="https://wiki.debian.org/Sprints">Entwickler-Sprints</a>, der Präsenz auf weiteren
Veranstaltungen, usw.
</p>

<p id="default">
Mehrere <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">Organisationen</a>
verwalten treuhänderisch Vermögen für Debian und nehmen in seinem Namen Spenden entgegen.
Diese Seite listet verschiedene Möglichkeiten auf, Spenden an das Debian-Projekt zu richten.
Die einfachste Methode ist über PayPal an
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>, eine
gemeinnützige Organisation, die Spenden für Debian entgegennimmt.
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Spenden via PayPal</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Organisation</th>
<th>Methoden</th>
<th>Hinweise</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>,
 <a href="#spi-cheque">Scheck</a>,
 <a href="#spi-other">weitere</a>
</td>
<td>USA, steuerbefreit, gemeinnützig</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">Banküberweisung</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>Frankreich, steuerbefreit, gemeinnützig</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">Banküberweisung</a>,
 <a href="#debianch-other">weitere</a>
</td>
<td>Schweiz, gemeinnützig</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">Ausrüstung</a>,
 <a href="#debian-time">Zeit</a>,
 <a href="#debian-other">weiteres</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
ist eine steuerbefreite gemeinnützige Institution in den Vereinigten
Staaten von Amerika, die 1997 von Debian-Leuten gegründet wurde,
um Organisationen im Freie-Software-/Hardware-Umfeld zu unterstützen.
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
Einmalige und regelmäßig wiederkehrende Spenden können über die
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">SPI-Seite</a>
auf der Webseite von Paypal getätigt werden. Um eine regelmäßige Spende einzurichten,
wählen Sie das Kästchen <em>Make this a monthly donation</em> aus.
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
Einmalige und regelmäßig wiederkehrende Spenden können über die
<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">SPI-Seite</a>
auf der Webseite von Click &amp; Pledge getätigt werden. Um eine regelmäßige
Spende einzurichten, geben Sie auf der rechten Seite ein, wie oft Sie spenden
möchten (<em>Repeat payment</em>), scrollen nach unten bis <em>Debian Project
Donation</em>, tragen den Betrag ein, klicken auf <em>Add to cart</em> und
folgen dem weiteren Abwicklungsprozess.
</p>

<h3 id="spi-cheque">Scheck</h3>

<p>
Spenden können auch per Scheck oder Geldanweisung in
<abbr title="United States Dollar">USD</abbr> und <abbr title="Canadian Dollar">CAD</abbr>
getätigt werden. Bitte geben Sie im Verwendungszweck-Feld <em>Debian</em> an und senden Sie
den Scheck an die auf der <a href="https://www.spi-inc.org/donations/">SPI-Spenden-Webseite</a>
angegebene Adresse.
</p>

<h3 id="spi-other">Weitere</h3>

<p>
Spenden über elektronische Banküberweisung und andere Methoden
sind ebenfalls möglich. In einigen Teilen der Erde könnte es
einfacher sein, Spenden an Partnerorganisationen von Software in the Public
Interest zu richten. Weitere Details finden Sie auf der
<a href="https://www.spi-inc.org/donations/">SPI-Spenden-Webseite</a>.
</p>

<h2 id="debianfrance">Debian France</h2>

<p>
Die <a href="https://france.debian.net/">Debian France Association</a>
ist eine gemäß dem <q>Gesetz von 1901</q> registrierte Organisation in
Frankreich, gegründet zur Unterstützung und Förderung des
Debian-Projekts in Frankreich.
</p>

<h3 id="debianfrance-bank">Banküberweisung</h3>

<p>
Spenden sind per elektronischer Banküberweisung an das auf der
<a href="https://france.debian.net/soutenir/#compte">Spenden-Webseite von Debian France</a>
genannte Bankkonto möglich.
Eine Spendenquittung erhalten Sie auf Anfrage, indem Sie eine E-Mail an
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a> senden.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
Sie können auch über die
<a href="https://france.debian.net/galette/plugins/paypal/form">PayPal-Seite von Debian France</a>
eine Spende übermitteln. Diese Spenden können entweder Debian France zugewandt werden oder
dem Debian-Projekt insgesamt.
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> wurde gegründet, um das
Debian-Projekt in der Schweiz und dem Fürstentum Liechtenstein zu
repräsentieren.
</p>

<h3 id="debianch-bank">Banküberweisung</h3>

<p>
Spenden sind per Banküberweisung von Schweizer und internationalen
Banken zugunsten der auf der <a href="https://debian.ch/">debian.ch-Website</a>
genannten Konten möglich.
</p>

<h3 id="debianch-other">Weitere</h3>

<p>
Für weitere Möglichkeiten zu spenden kontaktieren Sie bitte die
<q>donate</q>-Mail-Adresse auf der <a href="https://debian.ch/">Website</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
Debian kann derzeit lediglich Spenden von <a href="#debian-equipment">Ausrüstung (Hardware)</a>
direkt entgegennehmen, aber keine <a href="#debian-other">anderen</a> Spenden.
</p>

<h3 id="debian-equipment">Ausrüstung und Dienste</h3>

<p>
Debian ist auch auf die Spende von Ausrüstung und Diensten angewiesen
(von Einzelpersonen, Firmen, Universitäten usw.), um die Verbindung
zum Rest der Welt aufrecht erhalten zu können.
</p>

<p>
Wenn Ihre Firma ungenutzte Maschinen oder Reserveausrüstung (wie Festplatten,
SCSI-Controller, Netzwerkkarten usw.) rumliegen hat, erwägen Sie bitte,
diese an Debian zu spenden. Kontaktieren Sie dazu bitte unsere
<a href="mailto:hardware-donations@debian.org">Hardware-Spenden-Koordinatoren</a>.
</p>

<p>
Debian betreut eine
<a href="https://wiki.debian.org/Hardware/Wanted">Liste mit erwünschter
Hardware</a>, die für verschiedene Dienste und Gruppen innerhalb des
Projekts nützlich wäre.
</p>

<h3 id="debian-time">Zeit</h3>

<p>
Es gibt viele Wege, <a href="$(HOME)/intro/help">Debian zu helfen</a>,
indem Sie Ihre private oder Arbeitszeit zu Verfügung stellen.
</p>

<h3 id="debian-other">Anderes</h3>

<p>
Debian kann derzeit keine Digitalwährungen akzeptieren, aber wir
suchen nach Möglichkeiten, diese Methode für Spenden in Zukunft
unterstützen zu können.
# Brian Gupta requested we discuss this before including it:
# If you have cryptocurrency to donate, or insights to share, please
# get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>
