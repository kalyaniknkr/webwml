#use wml::debian::translation-check translation="b827d01d6caf2b68e865bd2c4ff78fab56ab8eb4" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 12 aktualisiert: 12.2 veröffentlicht</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>


<p>
Das Debian-Projekt freut sich, die zweite Aktualisierung seiner Stable-Distribution
Debian <release> (Codename <q><codename></q>) bekanntgeben zu können. Diese
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen
veröffentlicht worden, auf die, wo möglich, verwiesen wird.
</p>


<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt.
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>
<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction amd64-microcode "Enthaltenen Mikrocode aktualisiert, inklusive Korrekturen für <q>AMD Inception</q> bei AMD-Zen4-Prozessoren [CVE-2023-20569]">
<correction arctica-greeter "Konfiguration des Themas für die Bildschirmtastatur durch die ArcticaGreeter-Einstellungen unterstützen; <q>Kompaktes</q> BST-Layout (statt »Klein«) verwenden, weil dieses auch Spezialtasten wie die deutschen Umlaute enthält; Darstellung der Anmeldefehler-Meldungen verbessert; statt Emerald aktives Thema benutzen">
<correction autofs "Rückschritt beim Feststellen der Erreichbarkeit von Dual-Stack-Hosts behoben">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 12.2">
<correction batik "Serverseitige Abfragefälschungen behoben [CVE-2022-44729 CVE-2022-44730]">
<correction boxer-data "Aufhören, https-everywhere für Firefox zu installieren">
<correction brltty "xbrlapi: brltty nicht mit ba+a2 starten, falls nicht verfügbar; Cursor-Routing und Braille-Panning in Orca für den Fall verbessert, dass xbrlapi installiert ist, aber der a2-Bildschirmtreiber nicht">
<correction ca-certificates-java "Während neuer Installationen nicht von unkonfigurierter JRE aufhalten lassen">
<correction cairosvg "»data:«-URLs im abgesicherten Modus handhaben">
<correction calibre "Export-Funktionalität überarbeitet">
<correction clamav "Neue stabile Version der Originalautoren; Sicherheitskorrekturen [CVE-2023-20197 CVE-2023-20212]">
<correction cryptmount "Probleme mit Speicher-Initialisierung in Befehlszeilen-Verarbeitungsroutine vermeiden">
<correction cups "Heap-basierten Pufferüberlauf behoben [CVE-2023-4504]; unangemeldeten Zugriff unterbunden [CVE-2023-32360]">
<correction curl "Zusammen mit OpenLDAP kompiliert, um fehlerhaften Abrufen von binären LDAP-Attributen zu begegnen; exzessiven Speicherbedarf behoben [CVE-2023-38039]">
<correction cyrus-imapd "Sicherstellen, dass beim Upgrade von Bullseye aus keine Postfächer verloren gehen">
<correction dar "Probleme beim Erstellen isolierter Kataloge, wenn dar mit einer aktuellen gcc-Version kompiliert wurde, behoben">
<correction dbus "Neue stabile Version der Originalautoren; dbus-Daemon-Absturz beim Neuladen von Richtlinien behoben, der auftrat, wenn eine Verbindung einem Benutzer gehörte, der bereits gelöscht war, bzw. bei einem beschädigten Name-Service-Switch-Plugin oder auf Kernels, die kein SO_PEERGROUPS unterstützen; Fehler korrekt melden, wenn das Abfragen der Gruppen einer UID fehlschlägt; dbus-user-session: XDG_CURRENT_DESKTOP in die Aktivierungs-Umgebung kopieren">
<correction debian-archive-keyring "Übrig gebliebene Schlüsselbunde in trusted.gpg.d aufräumen">
<correction debian-edu-doc "Handbuch zu Debian Edu Bookworm aktualisiert">
<correction debian-edu-install "Neue Version der Originalautoren; Größen bei der D-I-Autopartitionierung korrigiert">
<correction debian-installer "Linux-Kernel-ABI auf 6.1.0-13 angehoben; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-parl "Neukompilierung mit neuerem boxer-data; nicht länger von webext-https-everywhere abhängen">
<correction debianutils "Doppeleinträge in /etc/shells behoben; /bin/sh in der State-File verwalten; Kanonialisierung der Shells in Lokationen mit Alias überarbeitet">
<correction dgit "Alte /updates-Sicherheits-Mapping nur für Buster verwenden; Pushen alter Versionen, die bereits im Archiv liegen, verhindert">
<correction dhcpcd5 "Upgrades mit Überresten von Wheezy erleichtern; missbilligte ntpd-Integration abgeschafft; Version im Aufräum-Skript korrigiert">
<correction dpdk "Neue stabile Version der Originalautoren">
<correction dput-ng "Erlaubte Upload-Ziele aktualisiert; Fehlschlag beim Kompilieren aus der Quelle behoben">
<correction efibootguard "Unzureichende oder fehlende Validierung und Bereinigung von Eingaben aus nicht vertrauenswürdigen Dateien in der Bootloader-Umgebung überarbeitet [CVE-2023-39950]">
<correction electrum "Lightning-Sicherheitsproblem behoben">
<correction filezilla "Kompilate für 32-Bit-Umgebungen überarbeitet; Absturz beim Entfernen von Dateitypen von der Liste behoben">
<correction firewalld "Keine IPv4- und IPv6-Adressen in einer einzelnen nftables-Regel vermischen">
<correction flann "Überzähliges -llz4 von flann.pc entfernt">
<correction foot "XTGETTCAP-Abfragen mit ungültigen Hex-Enkodierungen ignorieren">
<correction freedombox "n= in apt-Einstellungen verwenden, damit Upgrades glatt laufen">
<correction freeradius "Sicherstellen, dass in TLS-Client-Cert-Common-Name korrekte Daten stehen">
<correction ghostscript "Pufferüberlauf behoben [CVE-2023-38559]; versucht, den IJS-Server-Start abzusichern [CVE-2023-43115]">
<correction gitit "Neukompilierung gegen neues pandoc">
<correction gjs "Endlosschleifen von Idle-Callbacks, wenn ein Idle-Handler während der GC aufgerufen wird, vermeiden">
<correction glibc "Auf ppc64el den Wert von F_GETLK/F_SETLK/F_SETLKW mit __USE_FILE_OFFSET64 korrigiert; Stapel-Leseüberlauf in getaddrinfo im no-aaaa-Modus behoben [CVE-2023-4527]; Use-after-free in getcanonname behoben [CVE-2023-4806 CVE-2023-5156]; _dl_find_object dazu gebracht, auch früh im Startprozess richtige Werte zurückzuliefern">
<correction gosa-plugins-netgroups "Missbilligungs-Warnungen auf der Webschnittstelle abgestellt">
<correction gosa-plugins-systems "Verwaltung der DHCP-/DNS-Einträge im Default-Theme überarbeitet; Hinzufügen von (Standalone-) <q>Netzwerkdrucker</q>-Systemen überarbeitet; Erstellung von Ziel-DNs für verschiedene Systemtypen überarbeitet; Symboldarstellung im DHCP-Servlet korrigiert; unqualifizierten Hostnamen für Arbeitsstationen erzwingen">
<correction gtk+3.0 "Neue stabile Version der Originalautoren; mehrere Abstürze behoben; auf der <q>Inspector</q>-Fehersuch-Schnittstelle mehr Infos ausgeben; GFileInfo-Warnungen bei Verwendung mit einer zurückportierten Version von GLib abgestellt; für das Caret bei dunklen Themen eine helle Farbe gewählt, damit es in diversen Programmen, vor allem Evince, viel leichter zu sehen ist">
<correction gtk4 "Abschneidungen in der Orte-Seitenleiste, die auftreten, wenn die Schriftvergrößerung aktiv ist, behoben">
<correction haskell-hakyll "Neukompilierung gegen neues pandoc">
<correction highway "Unterstützung für armhf-Systeme ohne NEON verbessert">
<correction hnswlib "Doppel-Free in init_index behoben, das auftrat, wenn das M-Argument eine große Ganzzahl war [CVE-2023-37365]">
<correction horizon "Anfälligkeit für offene Weiterleitungen behoben [CVE-2022-45582]">
<correction icingaweb2 "Nicht wünschenswerte Missbilligungs-Benachrichtigungen unterdrückt">
<correction imlib2 "Beibehaltung des Alpha-Channel-Markierung verbessert">
<correction indent "Lesezugriff außerhalb des Puffers behoben; übermäßigen Puffer-Schreibzugriff behoben [CVE-2023-40305]">
<correction inetutils "Rückgabewerte beim Abgeben von Privilegien überprüfen [CVE-2023-40303]">
<correction inn2 "nnrpd-Hänger bei eingeschalteter Kompression behoben; Unterstützung für hochpräzise Syslog-Zeitstempel hinzugefügt; inn-{radius,secrets}.conf nicht für alle Welt lesbar machen">
<correction jekyll "YAML-Aliase unterstützen">
<correction kernelshark "Speicherzugriffsfehler in libshark-tepdata behoben; Aufzeichnung in Zielverzeichnis mit Leerzeichen korrigiert">
<correction krb5 "Freigabe nicht-initialisierter Zeiger überarbeitet [CVE-2023-36054]">
<correction lemonldap-ng "Anmelde-Kontrolle auf Auth-Slave-Anfragen anwenden; offene Weiterleitungen wegen fehlerhafter Zeichenmaskierung behoben; offene Weiterleitungen durch nicht vorhandene Weiterleitungs-URIs in OIDC RP behoben; serverseitige Abfragefälschungen behoben [CVE-2023-44469]">
<correction libapache-mod-jk "Funktion für implizite Mappings abgeschafft, die zu unbeabsichtigter Freigabe des Status-Workers und/oder Umgehung der Sicherheitsbeschränkungen führen konnte [CVE-2023-41081]">
<correction libclamunrar "Neue stabile Version der Originalautoren">
<correction libmatemixer "Heap-Korruptionen/Anwendungsabstürze beim Entfernen von Audiogeräten behoben">
<correction libpam-mklocaluser "pam-auth-update: sichergestellt, dass das Modul vor den anderen Sitzungstyp-Modulen an der Reihe ist">
<correction libxnvctrl "Neues Quellpaket, das von nvidia-settings abgespaltet wurde">
<correction linux "Neue stabile Version der Originalautoren">
<correction linux-signed-amd64 "Neue stabile Version der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Version der Originalautoren">
<correction linux-signed-i386 "Neue stabile Version der Originalautoren">
<correction llvm-defaults "Symbolischen /usr/include/lld-Verweis korrigiert; Beschädigt-Abhängigkeit von nicht gleichzeitig instalierbaren Paketen hinzugefügt, um Upgrades von Bullseye zu erleichtern">
<correction ltsp "Anwendung von mv auf Init-Symlink vermeiden">
<correction lxc "Nftables-Syntax für IPv6-NAT korrigiert">
<correction lxcfs "CPU-Meldung innerhalb eines arm32-Containers mit vielen CPUs korrigiert">
<correction marco "Compositing nur in dem Fall einschalten, dass es verfügbar ist">
<correction mariadb "Neue Fehlerkorrektur-Veröffentlichung der Originalautoren">
<correction mate-notification-daemon "Zwei Speicherlecks geflickt">
<correction mgba "Kaputten Ton in libretro core behoben; Absturz auf Hardware, die kein OpenGL 3.2 beherrscht, verhindert">
<correction modsecurity "Dienstblockaden unterbunden [CVE-2023-38285]">
<correction monitoring-plugins "check_disk: Beim Suchen nach passenden Einhängepunkten ein Einhängen vermeiden, um eine Verlangsamung gegenüber der Bullseye-Version zu lösen">
<correction mozjs102 "Neue stabile Version der Originalautoren; <q>inkorrekten Wert während WASM-Kompilierung verwendet</q> behoben [CVE-2023-4046], potenzielles Use-after-Free [CVE-2023-37202], Probleme mit Speichersicherheit [CVE-2023-37211 CVE-2023-34416] behoben">
<correction mutt "Neue stabile Version der Originalautoren">
<correction nco "Unterstützung für udunits2 wieder aktiviert">
<correction nftables "Inkorrekte Bytecode-Generierung behoben, die von einer neuen Kernel-Prüfung ausgelöst wird, welche das Ergänzen von Regeln zu verbundenen Chains verhindert">
<correction node-dottie "Sicherheitskorrektur (Prototype Pollution) [CVE-2023-26132]">
<correction nvidia-settings "Neue Fehlerkorrektur-Veröffentlichung der Originalautoren">
<correction nvidia-settings-tesla "Neue Fehlerkorrektur-Veröffentlichung der Originalautoren">
<correction nx-libs "Fehlenden symbolischen Verweis /usr/share/nx/fonts hinzugefügt; Handbuchseite überarbeitet">
<correction open-ath9k-htc-firmware "Richtige Firmware laden">
<correction openbsd-inetd "Probleme bei Speicherhandhabung behoben">
<correction openrefine "Anfälligkeit für eigenmächtige Codeausführung abgestellt [CVE-2023-37476]">
<correction openscap "Abhängigkeiten von openscap-utils und python3-openscap korrigiert">
<correction openssh "Anfälligkeit für Codeausführung aus der Ferne durch einen weitergeleiteten Agent-Socket unterbunden [CVE-2023-38408]">
<correction openssl "Neue stabile Version der Originalautoren; Sicherheitskorrekturen [CVE-2023-2975 CVE-2023-3446 CVE-2023-3817]">
<correction pam "pam-auth-update --disable überarbeitet; Türkisch-Übersetzung aktualisiert">
<correction pandoc "Problem mit eigenmächtigen Datei-Schreibzugriffen behoben [CVE-2023-35936]">
<correction plasma-framework "Plasmashell-Abstürze behoben">
<correction plasma-workspace "Absturz in krunner behoben">
<correction python-git "Anfälligkeit für Codeausführung aus der Ferne [CVE-2023-40267] und blindes Inkludieren von lokalen Dateien behoben [CVE-2023-41040]">
<correction pywinrm "Kompatibilität mit Python 3.11 verbessert">
<correction qemu "Aktualisierung auf Originalautoren-Baum 7.2.5; ui/vnc-clipboard: Endlosschleife in inflate_buffer [CVE-2023-3255] behoben; Nullzeiger-Dereferenzierung behoben [CVE-2023-3354]; Pufferüberlauf abgestellt [CVE-2023-3180]">
<correction qtlocation-opensource-src "Einfrieren beim Laden von Kartenkacheln behoben">
<correction rar "Fehlerkorrektur-Veröffentlichung der Originalautoren [CVE-2023-40477]">
<correction reprepro "Race Condition beim Einsatz externer Dekompressoren behoben">
<correction rmlint "Fehler in anderen Paketen, die durch eine ungültige Version des Pakets python verursacht wurden, behoben; Fehlschläge bei GUI-Starts mit aktuellem python3.11 behoben">
<correction roundcube "Neue stabile Version der Originalautoren; OAuth2-Authentifizierung überarbeitet; Anfälligkeit für seitenübergreifendes Skripting behoben [CVE-2023-43770]">
<correction runit-services "dhclient: Verwendung von eth1 nicht hartkodieren">
<correction samba "Neue stabile Version der Originalautoren">
<correction sitesummary "Neue Version der Originalautoren; Installation des sitesummary-maintenance-CRON/systemd-timerd-Skripts überarbeitet; unsichere Erstellung von Temporärdateien und -Verzeichnissen korrigiert">
<correction slbackup-php "Fehlerkorrekturen: Fernbefehle in Standardausgabe protokollieren; SSH-Known-Hosts-Dateien deaktiviert; PHP-8-Kompatibilität">
<correction spamprobe "Abstürze beim Verarbeiten von JPEG-Anhängen beseitigt">
<correction stunnel4 "Umgang mit Gegenstellen, welche die TLS-Verbindung ohne ordentliche Vorwarnung abbauen, überarbeitet">
<correction systemd "Neue stabile Version der Originalautoren; kleineres Sicherheitsproblem beim Laden der Device-Trees während des arm64- und riscv64-systemd-Boots (EFI) behoben">
<correction testng7 "Für zukünftige openjdk-17-Kompilierungen auf Stable zurückportiert">
<correction timg "Anfälligkeit für Pufferüberläufe beseitigt [CVE-2023-40968]">
<correction transmission "openssl3-Kompatibilitäts-Änderung ersetzt, um Speicherleck zu beheben">
<correction unbound "Fehlerprotokoll-Flutung bei Verwendung von DNS over TLS mit openssl 3.0 behoben">
<correction unrar-nonfree "Fernausführung von Code abgestellt [CVE-2023-40477]">
<correction vorta "Ctime- und mtime-Änderungen in Diffs abhandeln">
<correction vte2.91 "Ring-View öfter als nötig ungültig machen, um diverse Assertions-Fehlschläge während der Event-Handhabung zu vermeiden">
<correction x2goserver "x2goruncommand: Unterstützung für KDE Plasma 5 hinzugefügt; x2gostartagent: Logdatei-Beschädigung verhindert; keystrokes.cfg: mit nx-libs abgleichen; Enkodierung der Finnisch-Übersetzung korrigiert">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2023 5454 kanboard>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5458 openjdk-17>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5460 curl>
<dsa 2023 5462 linux-signed-amd64>
<dsa 2023 5462 linux-signed-arm64>
<dsa 2023 5462 linux-signed-i386>
<dsa 2023 5462 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5466 ntpsec>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5469 thunderbird>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5477 samba>
<dsa 2023 5479 chromium>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5487 chromium>
<dsa 2023 5488 thunderbird>
<dsa 2023 5491 chromium>
<dsa 2023 5492 linux-signed-amd64>
<dsa 2023 5492 linux-signed-arm64>
<dsa 2023 5492 linux-signed-i386>
<dsa 2023 5492 linux>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5497 libwebp>
<dsa 2023 5498 thunderbird>
<dsa 2023 5501 gnome-shell>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>


<h2>Entfernte Pakete</h2>
<p>Die folgenden Pakete wurden wegen Umständen entfernt, die sich unserer Kontrolle entziehen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction https-everywhere "obsolet, die gängigsten Browser bieten native Unterstützung">
</table>

<h2>Debian-Installer</h2>
<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält,
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern
Freier Software, die ihre Zeit und Mühen einbringen, um das
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das
Stable-Veröffentlichungs-Team (auf Englisch)
unter &lt;debian-release@lists.debian.org&gt;.</p>
