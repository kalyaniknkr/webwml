#use wml::debian::template title="Introduction à Debian" MAINPAGE="true" FOOTERMAP="true"
#use wml::debian::recent_list

#use wml::debian::translation-check translation="1cf898ff877f907fd4f3471e33d7d506701907f3" maintainer="Jean-Pierre Giraud"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian est une communauté de personnes</h2>
       <p>Des milliers de volontaires à travers le monde travaillent ensemble en privilégiant le logiciel libre et à code source ouvert. Rejoignez le projet Debian.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Les acteurs</a> :
          qui sommes nous, que faisons nous ?
        </li>
        <li>
          <a href="philosophy">Philosophie</a> :
          pourquoi et comment nous le faisons
        </li>
        <li>
          <a href="../devel/join/">S'impliquer</a> :
          comment devenir contributeur de Debian
        </li>
        <li>
          <a href="help">Contribuer</a> :
          comment pouvez-vous aider Debian
        </li>
        <li>
          <a href="../social_contract">Le contrat social</a> :
          nos intentions morales
        </li>
        <li>
          <a href="diversity">Tout le monde est bienvenu</a> :
          diversité et équité selon Debian
        </li>
        <li>
          <a href="../code_of_conduct">Pour les participants</a> :
          code de conduite de Debian
        </li>
        <li>
          <a href="../partners/">Partenariat</a> :
          entreprises et organisations qui fournissent une assistance
          au projet Debian
        </li>
        <li>
          <a href="../donations">Dons</a> :
          comment parrainer le projet Debian
        </li>
        <li>
          <a href="../legal/">Informations légales</a> :
          licences, marques déposées, protection des données, charte sur les
          brevets, etc.
        </li>
        <li>
          <a href="../contact">Contact</a> :
          comment nous contacter
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian est un système d'exploitation</h2>
      <p>Debian est un système d'exploitation libre, développé et géré par le projet Debian. C'est une distribution Linux libre à laquelle nous avons ajouté plusieurs milliers d'applications pour répondre aux besoins des utilisateurs.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Téléchargement</a> :
          où obtenir Debian
        </li>
        <li>
          <a href="why_debian">Pourquoi Debian ?</a>
          les raisons du choix de Debian
        </li>
        <li>
          <a href="../support">Assistance</a> :
          où obtenir de l'aide
        </li>
        <li>
          <a href="../security">Sécurité</a> :
          annonces récentes et sources des informations de sécurité
        </li>
        <li>
          <a href="../distrib/packages">Logiciels</a> :
          rechercher et naviguer dans la longue liste des paquets Debian
        </li>
        <li>
          <a href="../doc">Documentation</a> :
          manuel d'installation, FAQ, HOWTO, Wiki et plus encore
        </li>
        <li>
          <a href="../Bugs">Système de suivi des bogues de Debian (BTS)</a> :
          comment signaler un bogue, documentation sur le BTS
        </li>
        <li>
          <a href="https://lists.debian.org/">Listes de diffusion</a> :
          toutes les listes Debian pour les utilisateurs, les développeurs, etc.
        </li>
        <li>
          <a href="../blends">Pure Blends</a> :
          métapaquets pour des besoins spécifiques
        </li>
        <li>
          <a href="../devel">Le coin du développeur</a> :
          informations intéressant principalement les développeurs Debian
        </li>
        <li>
          <a href="../ports">Portages et architectures</a> :
          architectures de processeur prises en charge
        </li>
        <li>
          <a href="search">Moteur de recherche</a> :
          comment utiliser le moteur de recherche de Debian
        </li>
        <li>
          <a href="cn">Différentes langues</a> :
          choisir la langue du site web de Debian
        </li>
      </ul>
    </div>
  </div>

</div>

