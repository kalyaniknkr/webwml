#use wml::debian::translation-check translation="ca2375b4ef2eebfd1c7fb93df901295ca6ea6586" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Annonce de la fin de vie du suivi à long terme de Debian 10</define-tag>
<define-tag release_date>2024-06-15</define-tag>
#use wml::debian::news

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
L'équipe du suivi à long terme (LTS) de Debian annonce par ce message
que la prise en charge de Debian 10 <q>Buster</q> atteindra sa fin de vie
le 30 juin 2024, presque cinq ans après sa publication initiale le
6 juillet 2019.
</p>

<p>
À partir de juillet, Debian ne fournira plus de mises à jour de sécurité
pour Debian 10. Un sous-ensemble de paquets de <q>Buster</q> sera pris en
charge par des intervenants extérieurs. Vous trouverez des informations
détaillées sur la page
<a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.
</p>

<p>
L'équipe LTS préparera ensuite la transition vers Debian 11 <q>Bullseye</q>
qui est la version <q>oldstable</q> actuelle. Grâce à l'effort conjugué
de plusieurs équipes dont l'équipe en charge de la sécurité, l'équipe de
publication et l'équipe LTS, le cycle de vie de Debian 11 couvrira aussi
cinq années. Pour que le cycle de vie des versions de Debian soit plus
simple à suivre, les équipes concernées se sont mises d'accord sur le
programme suivant : trois ans de suivi normal, puis deux ans de suivi à
long terme (LTS). L'équipe LTS prendra le relai de l'équipe en charge de
la sécurité et de l'équipe de publication le 14 août 2024, trois ans
après la publication initiale du 14 août 2021. La dernière mise à jour
intermédiaire de <q>Bullseye</q> sera publiée peu de temps après que la
dernière annonce de sécurité pour Debian 11 ait été publiée.
</p>

<p>
Debian 11 recevra un suivi à long terme jusqu'au 31 août 2026. Les
architectures prises en charges comprennent toujours amd64, i386, arm64
et armhf.
</p>

<p>
Pour plus d'informations sur l'utilisation de <q>Bullseye</q> LTS et la
mise à niveau à partir de <q>Buster</q> LTS, veuillez vous référer à la
page du wiki <a href="https://wiki.debian.org/fr/LTS/Using">Utiliser
Debian Long Term Support (LTS)</a>.
</p>

<p>
Debian et l'équipe LTS aimeraient remercier tous ceux, utilisateurs
contributeurs, développeurs, parrains et autres équipes de Debian, qui
rendent possible le prolongement de la vie des anciennes distributions
<q>stable</q>.
</p>

<p>Si vous dépendez de Debian LTS, vous pouvez envisager de
<a href="https://wiki.debian.org/fr/LTS/Development">rejoindre l'équipe</a>,
en fournissant des correctifs, en réalisant des tests ou en
<a href="https://wiki.debian.org/fr/LTS/Funding">finançant l'initiative</a>.</p>


<h2>À propos de Debian</h2>

<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code
source ouvert. Des milliers de volontaires du monde entier travaillent
ensemble pour créer et maintenir les logiciels Debian.
Traduite en soixante-dix langues et prenant en charge un grand nombre
de types d'ordinateurs, la distribution Debian est conçue
pour être le <q>système d'exploitation universel</q>.
</p>

<h2>Contact</h2>

<p>Pour de plus amples informations, veuillez consulter le site internet
de Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un
courrier électronique à &lt;press@debian.org&gt;.</p>

<h2>S'inscrire ou se désinscrire</h2>
<p><a href="https://lists.debian.org/debian-news-french/">S'inscrire ou
se désinscrire</a> de la liste de diffusion des Nouvelles pour les
utilisateurs francophones (debian-new-french)</p>
