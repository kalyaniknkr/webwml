#use wml::debian::translation-check translation="67e7c0f3759a5673d880c47304c13ca117ee5d0c" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.10</define-tag>
<define-tag release_date>2024-06-29</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la dixième mise à jour de sa
distribution oldstable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version oldstable. Les annonces
de sécurité ont déjà été publiées séparément et sont simplement référencées
dans ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>


<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version oldstable apporte quelques corrections importantes
aux paquets suivants :

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction allegro5 "Correction de problèmes de dépassement de tampons [CVE-2021-36489]">
<correction amavisd-new "Gestion de plusieurs paramètres de limites qui contiennent des valeurs conflictuelles [CVE-2024-28054]">
<correction bart "Correction d'échec de tests de construction en assouplissant une comparaison de nombres en virgule flottante">
<correction bart-cuda "Correction d'échec de tests de construction en assouplissant une comparaison de nombres en virgule flottante">
<correction base-files "Mise à jour pour cette version">
<correction cloud-init-22.4.2 "Introduction d'un remplaçant d'une version supérieure du paquet cloud-init">
<correction cpu "Fourniture d'exactement une définition de globalLdap dans le greffon de LDAP">
<correction curl "Correction d'une fuite de mémoire quand un <q>push</q> de serveur HTTP/2 est interrompu [CVE-2024-2398]">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 5.10.0-30 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debsig-verify "Reconstruction pour corriger un Built-Using obsolète">
<correction deets "Reconstruction pour corriger un Built-Using obsolète">
<correction distro-info-data "Déclaration des intentions pour Bullseye/Bookworm ; correction de données anciennes ; ajout d'Ubuntu 24.10">
<correction django-mailman3 "Nettoyage des messages avant l'archivage">
<correction dns-root-data "Mise à jour de root.hints ; mise à jour des informations de sécurité expirées">
<correction emacs "Protection contre des ressources distantes non sûres [CVE-2024-30203 CVE-2024-30204 CVE-2024-30205] ; correction d'une fuite de mémoire dans le correctif pour le CVE-2022-48337">
<correction galera-4 "Nouvelle version amont de correction de bogues ; mise à jour de la clé de signature de la version amont ; échec des tests relatifs aux dates évités">
<correction gdk-pixbuf "ANI : rejet des fichiers avec de multiples morceaux anih [CVE-2022-48622] ; ANI : rejet des fichiers avec de multiples morceaux INAM ou IART ; ANI : validation de la taille des morceaux anih">
<correction glib2.0 "Correction d'une (rare) fuite de mémoire">
<correction gnutls28 "Correction d'un échec d'assertion lors de la vérification d'une chaîne de certificats avec un cycle de signatures croisées [CVE-2024-0567] ; correction d'une attaque temporelle par canal auxiliaire dans l'échange de clés RSA-PSK [CVE-2024-0553]">
<correction gross "Correction d'un dépassement de pile [CVE-2023-52159]">
<correction hovercraft "Dépendance à python3-setuptools">
<correction imlib2 "Correction d'une vulnérabilité de dépassement de tas lors de l'utilisation de la fonction tgaflip dans loader_tga.c [CVE-2024-25447 CVE-2024-25448 CVE-2024-25450]">
<correction intel-microcode "Corrections pour INTEL-SA-INTEL-SA-00972 [CVE-2023-39368], INTEL-SA-INTEL-SA-00982 [CVE-2023-38575], INTEL-SA-INTEL-SA-00898 [CVE-2023-28746], INTEL-SA-INTEL-SA-00960 [CVE-2023-22655] et INTEL-SA-INTEL-SA-01045 [CVE-2023-43490] ; atténuation pour INTEL-SA-01051 [CVE-2023-45733], INTEL-SA-01052 [CVE-2023-46103], INTEL-SA-01036 [CVE-2023-45745,  CVE-2023-47855] et de problèmes fonctionnels non précisés sur divers processeurs Intel">
<correction jose "Correction d'un problème de déni de service potentiel [CVE-2023-50967]">
<correction json-smart "Correction d'une récursion excessive menant à un dépassement de pile [CVE-2023-1370] ; correction d'un déni de service au moyen d'une requête contrefaite [CVE-2021-31684]">
<correction lacme "Correction de la logique de validation post-émission">
<correction libapache2-mod-auth-openidc "Correction d'une absence de validation d'entrée menant à un déni de service [CVE-2024-24814]">
<correction libjwt "Correction d'une attaque temporelle par canal auxiliaire au moyen de strcmp() [CVE-2024-25189]">
<correction libkf5ksieve "Fuite de mots de passe évitée dans les journaux côté serveur">
<correction libmicrohttpd "Correction d'une lecture hors limites au moyen de requêtes POST contrefaites [CVE-2023-27371]">
<correction libssh2 "Correction d'une vérification de mémoire hors limites dans _libssh2_packet_add [CVE-2020-22218]">
<correction links2 "Reconstruction pour corriger un Built-Using obsolète">
<correction nano "Correction d'un problème de lien symbolique malveillant [CVE-2024-5742]">
<correction ngircd "Respect de l'option <q>SSLConnect</q> pour les connexions entrantes ; validation du certificat de serveur sur les liens de serveur (S2S-TLS) ; METADATA : correction de la désactivation de <q>cloakhost</q>">
<correction nvidia-graphics-drivers "Fin de la prise en charge des pilotes Tesla 450 ; construction de libnvidia-fbc1 pour arm64 ; corrections de sécurité amont [CVE-2022-42265 CVE-2024-0074 CVE-2024-0078] ; correction de la construction pour ppc64el">
<correction nvidia-graphics-drivers-tesla-450 "Conversion en paquets de transition">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version LTS amont [CVE-2024-0074 CVE-2024-0078 CVE-2022-42265] ; nouvelle version amont stable ; corrections de sécurité [CVE-2024-0090 CVE-2024-0092] ; correction de la construction pour ppc64el">
<correction nvidia-settings "Version amont de correction de bogues ; construction pour ppc64el">
<correction org-mode "Protection contre des ressources distantes non sûres [CVE-2024-30203 CVE-2024-30204 CVE-2024-30205]">
<correction php-composer-xdebug-handler "Chargement des dépendances système obligé">
<correction php-doctrine-annotations "Chargement des dépendances système obligé">
<correction php-phpseclib "Chargement des dépendances système obligé ; protection de isPrime() et randomPrime() pour BigInteger [CVE-2024-27354] ; limitation de longueur d'OID dans ASN1 [CVE-2024-27355] ; correction de getLength() de BigInteger">
<correction php-proxy-manager "Chargement des dépendances système obligé">
<correction php-symfony-contracts "Chargement des dépendances système obligé">
<correction php-zend-code "Chargement des dépendances système obligé">
<correction phpseclib "Chargement des dépendances système obligé ; protection de isPrime() et randomPrime() pour BigInteger [CVE-2024-27354] ; limitation de longueur d'OID dans ASN1 [CVE-2024-27355] ; correction de getLength() de BigInteger">
<correction postfix "Version amont de correction de bogues">
<correction postgresql-13 "Nouvelle version amont stable">
<correction pypdf2 "Correction d'un temps d'exécution quadratique avec un PDF mal formé manquant de marqueur xref [CVE-2023-36810] ; correction d'une boucle infinie avec une entrée contrefaite [CVE-2022-24859]">
<correction python-aiosmtpd "Correction d'un problème de dissimulation SMTP [CVE-2024-27305] ; correction d'un problème d'injection de commande STARTTLS non chiffrée [CVE-2024-34083]">
<correction python-dnslib "Validation d'identifiant de transaction dans client.py">
<correction python-idna "Correction d'un problème de déni de service [CVE-2024-3651]">
<correction python-stdnum "Correction d'un échec de construction à partir des sources quand la date du test n'est pas assez éloignée dans le futur">
<correction qtbase-opensource-src "Corrections de sécurité [CVE-2022-25255 CVE-2023-24607 CVE-2023-32762 CVE-2023-32763 CVE-2023-33285 CVE-2023-34410 CVE-2023-37369 CVE-2023-38197 CVE-2023-51714 CVE-2024-25580]">
<correction reportbug "Correction du nom de suite pour le mappage des noms de code pour refléter la version de Bookworm">
<correction rust-cbindgen-web "Nouveau paquet source pour prendre en charge la construction des nouvelles versions de Firefox ESR">
<correction rustc-web "Prise en charge de Firefox ESR et de Thunderbird dans Bullseye pour LTS">
<correction sendmail "Correction d'un problème de dissimulation SMTP [CVE-2023-51765] ; ajout d'une configuration oubliée pour rejeter NULL par défaut">
<correction symfony "Chargement des dépendances système obligé ; DateTypTest : assurance que l'année soumise est un choix acceptable">
<correction systemd "Meson : suppression du filtre arch dans la liste d'appels système ; pas de définition de TZ avant l'exécution de tests d'unité sensibles au fuseau horaire">
<correction wpa "Correction d'un problème de contournement d'authentification [CVE-2023-52160]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2022 5146 puma>
<dsa 2023 5360 emacs>
<dsa 2023 5575 webkit2gtk>
<dsa 2023 5580 webkit2gtk>
<dsa 2024 5596 asterisk>
<dsa 2024 5616 ruby-sanitize>
<dsa 2024 5618 webkit2gtk>
<dsa 2024 5619 libgit2>
<dsa 2024 5620 unbound>
<dsa 2024 5621 bind9>
<dsa 2024 5622 postgresql-13>
<dsa 2024 5624 edk2>
<dsa 2024 5625 engrampa>
<dsa 2024 5627 firefox-esr>
<dsa 2024 5628 imagemagick>
<dsa 2024 5630 thunderbird>
<dsa 2024 5631 iwd>
<dsa 2024 5632 composer>
<dsa 2024 5635 yard>
<dsa 2024 5637 squid>
<dsa 2024 5638 libuv1>
<dsa 2024 5640 openvswitch>
<dsa 2024 5641 fontforge>
<dsa 2024 5643 firefox-esr>
<dsa 2024 5644 thunderbird>
<dsa 2024 5645 firefox-esr>
<dsa 2024 5646 cacti>
<dsa 2024 5647 samba>
<dsa 2024 5650 util-linux>
<dsa 2024 5651 mediawiki>
<dsa 2024 5652 py7zr>
<dsa 2024 5653 gtkwave>
<dsa 2024 5657 xorg-server>
<dsa 2024 5659 trafficserver>
<dsa 2024 5660 php7.4>
<dsa 2024 5662 apache2>
<dsa 2024 5663 firefox-esr>
<dsa 2024 5664 jetty9>
<dsa 2024 5666 flatpak>
<dsa 2024 5667 tomcat9>
<dsa 2024 5669 guix>
<dsa 2024 5670 thunderbird>
<dsa 2024 5671 openjdk-11>
<dsa 2024 5672 openjdk-17>
<dsa 2024 5673 glibc>
<dsa 2024 5678 glibc>
<dsa 2024 5679 less>
<dsa 2024 5681 linux-signed-amd64>
<dsa 2024 5681 linux-signed-arm64>
<dsa 2024 5681 linux-signed-i386>
<dsa 2024 5681 linux>
<dsa 2024 5682 glib2.0>
<dsa 2024 5682 gnome-shell>
<dsa 2024 5684 webkit2gtk>
<dsa 2024 5685 wordpress>
<dsa 2024 5686 dav1d>
<dsa 2024 5688 atril>
<dsa 2024 5690 libreoffice>
<dsa 2024 5691 firefox-esr>
<dsa 2024 5692 ghostscript>
<dsa 2024 5693 thunderbird>
<dsa 2024 5695 webkit2gtk>
<dsa 2024 5698 ruby-rack>
<dsa 2024 5700 python-pymysql>
<dsa 2024 5702 gst-plugins-base1.0>
<dsa 2024 5703 linux-signed-amd64>
<dsa 2024 5703 linux-signed-arm64>
<dsa 2024 5703 linux-signed-i386>
<dsa 2024 5703 linux>
<dsa 2024 5704 pillow>
<dsa 2024 5707 vlc>
<dsa 2024 5709 firefox-esr>
<dsa 2024 5711 thunderbird>
<dsa 2024 5713 libndp>
<dsa 2024 5714 roundcube>
<dsa 2024 5715 composer>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction phppgadmin "Problèmes de sécurité">
<correction pytest-salt-factories "Nécessaire uniquement pour salt qui va être retiré">
<correction pytest-testinfra "Nécessaire uniquement pour salt qui va être retiré">
<correction salt "Prise en charge impossible, plus entretenu">
<correction snort "Problèmes de sécurité, plus entretenu">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de oldstable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Mises à jour proposées à la distribution oldstable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>


