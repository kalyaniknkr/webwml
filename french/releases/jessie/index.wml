#use wml::debian::template title="Informations sur la version &ldquo;Jessie&rdquo; de Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ff4acdb89311338bebdbb71b28dcc479d29029e3" maintainer="Jean-Paul Guillonneau"

<p>La version de Debian <current_release_jessie> a été publiée le
<a href="$(HOME)/News/<current_release_newsurl_jessie/>"><current_release_date_jessie></a>.
<ifneq "8.0" "<current_release>"
  "La version 8.0 a été initialement publiée le <:=spokendate('2015-04-26'):>."
/>
Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2015/20150426">communiqué de presse</a> et les
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian 8 a été remplacée par
<a href="../stretch/">Debian 9 (<q>Stretch</q>)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis le <:=spokendate('2018-06-17'):>.
</strong></p>

<p><strong>Jessie a bénéficié de la prise en charge à long terme (LTS) jusqu’à
la fin juin 2020. Cette prise en charge était limitée aux architectures i386,
amd64, armel et armhf.
Pour plus d’informations, veuillez consulter la <a
href="https://wiki.debian.org/LTS">section LTS du wiki de Debian</a>.
</strong></p>

<p>Pour obtenir et installer Debian, veuillez vous reporter à la page des
<a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à
partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.</p>

<p>Les architectures suivantes étaient prises en charge par LTS :</p>

<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
</ul>

<p>Lors de la publication initiale de Jessie, les architectures suivantes
étaient prises en charge :</p>

<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">ARM 64 bits (AArch64)</a>
<li><a href="../../ports/ppc64el/">Processeurs POWER</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="../reportingbugs">signaler d'autres problèmes</a>.
</p>
