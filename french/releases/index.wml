#use wml::debian::template title="Les versions de Debian"
#include "$(ENGLISHDIR)/releases/info"

#use wml::debian::translation-check translation="4f2d95fd2e045fcc86178b7b4255c4b5ac4b6967" maintainer="Jean-Paul Guillonneau"

#  Original translation by Jérôme Marant
# Autres traductions : voir journal

<p>Debian a toujours au moins trois versions activement entretenues :
<q>stable</q>, <q>testing</q> et <q>unstable</q>.

<dl>
<dt><a href="stable/">stable</a></dt>
<dd>
  <p>La distribution <q>stable</q> contient la dernière distribution
  officiellement sortie de Debian.</p>

  <p>
  C'est la version de production de Debian, celle que nous recommandons
  en premier d'utiliser.
  </p>

  <p>
  Actuellement, la distribution <q>stable</q> de Debian est la
  version <:=substr '<current_initial_release>', 0, 2:>, nom de code <em><:=ucfirst <current_release_name>:></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "Elle a été publiée le <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "Elle a été initialement publiée en tant que version <current_initial_release>
  le <current_initial_release_date> et sa dernière
  mise à jour, version <current_release>, a été publiée le <current_release_date>."
/>
  </p>
</dd>

<dt><a href="testing/">testing</a></dt>
<dd>
  <p>
  La distribution <q>testing</q> contient les paquets qui
  n'ont pas encore été acceptés dans la distribution
  <q>stable</q>, mais qui sont en attente de l'être. Le
  principal avantage d'utiliser cette distribution est qu'elle
  contient des versions plus récentes des logiciels.
  </p>

  <p>
  Consultez la <a href="$(DOC)/manuals/debian-faq/">FAQ Debian</a> pour plus
  de renseignements sur <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">ce
  qu'est <q>testing</q></a> et <a
  href="$(DOC)/manuals/debian-faq/ftparchives#frozen">comment elle devient
  <q>stable</q></a>.
  </p>

  <p>
  Actuellement la distribution <q>testing</q> est
  <em><:=ucfirst <current_testing_name>:></em>.
  </p>

</dd>

<dt><a href="unstable/">unstable</a></dt>
<dd>
  <p>
  La distribution <q>unstable</q> est celle sur laquelle les activités de
  développement se déroulent. Généralement, cette distribution est utilisée par
  les développeurs et par ceux qui aiment vivre sur le fil. Il est recommandé
  que les personnes qui l’utilisent s’abonnent à liste de diffusion
  debian-devel-announce pour recevoir les notifications de modification majeure,
  par exemple, les mises à niveau pouvant casser le système.
  </p>

  <p>
  La distribution <q>unstable</q> est toujours appelée <em>Sid</em>.
  </p>

</dd>
</dl>

<h2>Cycle de vie des distributions</h2>
<p>
  Debian annonce régulièrement une nouvelle version stable. Le cycle de vie d'une
  version couvre cinq ans : une prise en charge complète pendant les trois
  premières années suivies de deux années supplémentaires de prise en charge à long
  terme (Long Term Support – LTS).
</p>

<p>
  Consulter la page du Wiki
  <a href="https://wiki.debian.org/fr/DebianReleases">Debian Releases</a>
  et la page du Wiki
  <a href="https://wiki.debian.org/fr/LTS">Debian LTS</a> pour des informations
  détaillées.
</p>

<h2>Index des versions</h2>

<ul>

  <li><a href="<current_testing_name>/">La prochaine version de Debian a pour nom de
  code <q><:=ucfirst <current_testing_name>:></q></a>
      &mdash; <q>testing</q>, aucune date de sortie n'a été fixée ;
      </li>

  <li><a href="bookworm/">Debian 12 (<q>Bookworm</q>)</a>
      &mdash; actuelle version <q>stable</q> ;
      </li>

  <li><a href="bullseye/">Debian 11 (<q>Bullseye</q>)</a>
      &mdash; actuelle version <q>oldstable</q> ;
      </li>

  <li><a href="buster/">Debian 10 (<q>Buster</q>)</a>
      &mdash; actuelle version <q>oldoldstable</q> avec une prise en charge à
      long terme <a href="https://wiki.debian.org/LTS">LTS</a> ;
      </li>

  <li><a href="stretch/">Debian 9 (<q>Stretch</q>)</a>
      &mdash; version archivée avec une extension de la prise en charge à long
      terme <a href="https://wiki.debian.org/LTS/Extended">LTS</a> financée
      par des tierces parties ;
      </li>

  <li><a href="jessie/">Debian 8 (<q>Jessie</q>)</a>
      &mdash; version archivée avec une extension de la prise en charge à long
      terme <a href="https://wiki.debian.org/LTS/Extended">LTS</a> financée
      par des tierces parties ;
      </li>

  <li><a href="wheezy/">Debian 7 (<q>Wheezy</q>)</a>
      &mdash; version stable obsolète&nbsp;;
      </li>
  <li><a href="squeeze/">Debian 6.0 (<q>Squeeze</q>)</a>
      &mdash; version stable obsolète&nbsp;;
     </li>
  <li><a href="lenny/">Debian GNU/Linux&nbsp;5.0 (<q>Lenny</q>)</a>
      &mdash; version stable obsolète&nbsp;;
     </li>
  <li><a href="etch/">Debian GNU/Linux&nbsp;4.0 (<q>Etch</q>)</a>
      &mdash; version stable obsolète&nbsp;;
      </li>
  <li><a href="sarge/">Debian GNU/Linux&nbsp;3.1 (<q>Sarge</q>)</a>
      &mdash; version stable obsolète&nbsp;;
      </li>
  <li><a href="woody/">Debian GNU/Linux&nbsp;3.0 (<q>Woody</q>)</a>
      &mdash; version stable obsolète&nbsp;;
      </li>
  <li><a href="potato/">Debian GNU/Linux&nbsp;2.2 (<q>Potato</q>)</a>
      &mdash; version stable obsolète&nbsp;;
      </li>
  <li><a href="slink/">Debian GNU/Linux&nbsp;2.1 (<q>Slink</q>)</a>
      &mdash; version stable obsolète&nbsp;;
      </li>
  <li><a href="hamm/">Debian GNU/Linux&nbsp;2.0 (<q>Hamm</q>)</a>
      &mdash; version stable obsolète.
      </li>
</ul>

<p>Les pages web pour les versions obsolètes de Debian sont gardées
intactes, mais les distributions elles-mêmes ne se trouvent que dans
une <a href="$(HOME)/distrib/archive">archive</a> séparée.</p>

<p>Voyez la <a href="$(HOME)/doc/manuals/debian-faq/">FAQ Debian</a> pour savoir
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">d'où
viennent ces noms de code</a>.</p>

<h2>Intégrité des données dans les versions</h2>

<p>L'intégrité des données est assurée par un fichier <code>Release</code>
signé numériquement. Pour garantir que tous les fichiers d'une version lui
appartiennent bien, des sommes de contrôle de tous les fichiers
<code>Packages</code> sont copiées dans le fichier <code>Release</code>.</p>

<p>Les signatures numériques de ce fichier sont mises dans le fichier
<code>Release.gpg</code>, en utilisant la version actuelle de la clé signant
l'archive. Pour <q>stable</q> et <q>oldstable</q>, une signature additionnelle
est générée en utilisant une clé hors ligne spécialement générée pour une
version par un membre de <a href="$(HOME)/intro/organization#release-team">
l'équipe de publication de la version « stable »</a>.</p>
