#use wml::debian::template title="Informations sur la version « Lenny » de Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="256bbbf3eb181693a0ebf3daf93179f79f7e38a0" maintainer="Jean-Paul Guillonneau"


<p>La version de Debian GNU/Linux <current_release_lenny> a été publiée le
<a href="$(HOME)/News/<current_release_newsurl_lenny/>"><current_release_date_lenny></a>.
Debian 5.0.0 a été initialement publiée le <:=spokendate('2009-02-14'):>.

Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2009/20090214">communiqué de presse</a> et nos
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian GNU/Linux 5.0 a été remplacée par
<a href="../squeeze/">Debian 6.0 (<q>Squeeze</q>)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis le 6 février 2012.
</strong></p>

<p>Pour obtenir et installer Debian, veuillez vous reporter à la page des
<a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à
partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.</p>

<p>Les architectures suivantes étaient prises en charge dans cette publication :</p>

<ul>

<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
<li><a href="../../ports/mipsel/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="../reportingbugs">signaler d'autres problèmes</a>.
</p>
