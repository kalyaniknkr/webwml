#use wml::debian::template title="Debian 10 — Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="386dee7e7c04f5460c0e5f7224721a444cb7d863" maintainer="Jean-Paul Guillonneau"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problèmes connus</toc-add-entry>
<toc-add-entry name="security">Problèmes de sécurité</toc-add-entry>

<p>
L'équipe de sécurité Debian produit des mises à jour de paquets de la
version stable dans lesquels elle a identifié des problèmes de
sécurité. Merci de consulter les <a href="$(HOME)/security/">pages concernant
la sécurité</a> pour plus d'informations concernant les problèmes de ce type
identifiés dans <q>Buster</q>.
</p>

<p>
Si vous utilisez APT, ajoutez la ligne suivante à votre fichier
<tt>/etc/apt/sources.list</tt> pour récupérer les dernières mises à jour de
sécurité&nbsp;:
</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Ensuite, lancez <kbd>apt update</kbd> suivi de <kbd>apt upgrade</kbd>.</p>

<toc-add-entry name="pointrelease">Les versions intermédiaires</toc-add-entry>

<p>
Dans le cas où il y a plusieurs problèmes critiques ou plusieurs mises
à jour de sécurité, la version de la distribution est parfois mise à jour.
Généralement, ces mises à jour sont indiquées comme étant des versions
intermédiaires.</p>

<ul>
  <li>La première version intermédiaire, 10.1, a été publiée le
      <a href="$(HOME)/News/2019/20190907">7 septembre 2019</a>.</li>
  <li>La seconde version intermédiaire, 10.2, a été publiée le
      <a href="$(HOME)/News/2019/20191116">16 novembre 2019</a>.</li>
 <li>La troisième version intermédiaire, 10.3, a été publiée le
      <a href="$(HOME)/News/2020/20200208">8 février 2020</a>.</li>
 <li>La quatrième version intermédiaire, 10.4, a été publiée le
      <a href="$(HOME)/News/2020/20200509">9 mai 2020</a>.</li>
 <li>La cinquième version intermédiaire, 10.5, a été publiée le
      <a href="$(HOME)/News/2020/20200801">1er août 2020</a>.</li>
 <li>La sixième version intermédiaire, 10.6, a été publiée le
      <a href="$(HOME)/News/2020/20200926">26 septembre 2020</a>.</li>
 <li>La septième version intermédiaire, 10.7, a été publiée le
      <a href="$(HOME)/News/2020/20201205">5 décembre 2020</a>.</li>
 <li>La huitième version intermédiaire, 10.8, a été publiée le
      <a href="$(HOME)/News/2021/20210206">6 février 2021</a>.</li>
 <li>La neuvième version intermédiaire, 10.9, a été publiée le
      <a href="$(HOME)/News/2021/20210327">27 mars 2021</a>.</li>
 <li>La dixième version intermédiaire, 10.10, a été publiée le
      <a href="$(HOME)/News/2021/20210619">19 juin 2021</a>.</li>
 <li>La onzième version intermédiaire, 10.11, a été publiée le
      <a href="$(HOME)/News/2021/2021100902">9 octobre 2021</a>.</li>
 <li>La douzième version intermédiaire, 10.12, a été publiée le
      <a href="$(HOME)/News/2022/2022032602">26 mars 2022</a>.</li>
 <li>La treizième version intermédiaire, 10.13, a été publiée le
      <a href="$(HOME)/News/2022/20220910">10 septembre 2022</a>.</li>
</ul>

<ifeq <current_release_buster> 10.0 "

<p>
À l'heure actuelle, il n'y a pas de mise à jour pour Debian 9.
</p>

<p>Veuillez consulter le <a
href="http://http.us.debian.org/debian/dists/buster/ChangeLog">\
journal des modifications</a> pour obtenir le détail des modifications
entre la version 10 et la version <current_release_buster/>.</p>"/>

<p>
Les corrections apportées à la version stable de la distribution passent
souvent par une période de test étendue avant d'être acceptées dans l'archive.
Cependant, ces corrections sont disponibles dans le répertoire <a
href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> de tout miroir de l'archive Debian.
</p>

<p>
Si vous utilisez APT pour mettre à jour vos paquets, vous pouvez installer les
mises à jour proposées en ajoutant la ligne suivante dans votre fichier
<tt>/etc/apt/sources.list</tt>&nbsp;:
</p>

<pre>
  \# Ajouts proposés pour une version intermédiaire 10
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Ensuite, exécutez <kbd>apt update</kbd> suivi de <kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Système d’installation</toc-add-entry>

<p>
Pour plus d'informations à propos des errata et des mises à jour du système
d'installation, consultez la page de l’installateur.</p>
