#use wml::debian::translation-check translation="43bcb91067c2d95af59dbb1b8684ba56b15a7a7a" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bookworm RC3</define-tag>
<define-tag release_date>2023-05-16</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la troisième version candidate de l’installateur pour Debian 12 <q>Bookworm</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>finish-install :
  <ul>
    <li>ajustement du nettoyage du cache d'APT pour éviter de casser la
      complétion de bash
      (<a href="https://bugs.debian.org/1034650">nº 1034650</a>).</li>
  </ul>
  </li>
  <li>grub-installer :
  <ul>
    <li>détection des variables d'amorçage d'EFI avec des chiffres hexadécimaux
      et plus seulement des nombres décimaux.</li>
  </ul>
  </li>
  <li>hw-detect :
  <ul>
    <li>rétablissement de l'affichage des alertes sur les licences des
      microprogrammes
      (<a href="https://bugs.debian.org/1033921">nº 1033921</a>).</li>
  </ul>
  </li>
  <li>linux :
  <ul>
    <li>construction avec la version mise à jour de dwarves, réduisant sa taille
      et son empreinte mémoire
      (<a href="https://bugs.debian.org/1033301">nº 1033301</a>).</li>
  </ul>
  </li>
  <li>partman-base :
  <ul>
    <li>ajout de la prise en charge des entrées soumises en utilisant les
      unités puissances de deux : Kio, Mio, Gio, etc.
      (<a href="https://bugs.debian.org/913431">nº 913431</a>). Notez que les
      tailles en sortie restent en utilisant les unités puissances de dix :
      ko, Mo, Go, etc. ;</li>
    <li>ajout de la prise en charge des préfixes supérieurs : pétaoctet (Po),
      pébioctet (Pio), exaoctet (Eo) et exbioctet (Eio) ;</li>
    <li>grand merci à Vincent Danjean !</li>
  </ul>
  </li>
  <li>preseed :
  <ul>
    <li>prise en compte par netcfg assurée des noms d'hôte fournis par DHCP
      utilisant le paramètre hostname sur la ligne de commande du noyau
      seulement comme solution de repli
     (<a href="https://bugs.debian.org/1035349">nº 1035349</a>).</li>
  </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
  <ul>
    <li>fourniture de modules DRM dédiés pour bochs et cirrus pour éviter la
      casse de l'affichage graphique sous UEFI/Secure Boot
      (<a href="https://bugs.debian.org/1036019">nº 1036019</a>).</li>
  </ul>
  </li>
  <li>linux :
  <ul>
    <li>contournement d'un problème d'écran noir sur ppc64el
      (<a href="https://bugs.debian.org/1033058">nº 1033058</a>).</li>
  </ul>
  </li>
  <li>xorg-server :
  <ul>
    <li>fourniture à nouveau de modesetting_drv.so dans le paquet udeb,
      corrigeant la prise en charge de l'installateur graphique sur UTM
      (<a href="https://bugs.debian.org/1035014">nº 1035014</a>).</li>
  </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 41 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres médias d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
