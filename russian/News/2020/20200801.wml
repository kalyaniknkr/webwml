#use wml::debian::translation-check translation="846effa858f46cf6429f61e241ec96292032972f" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.5</define-tag>
<define-tag release_date>2020-08-01</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о пятом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Кроме того, в данной редакции приняты меры в соответствии с важной рекомендацией по безопасности Debian,
<a href="https://www.debian.org/security/2020/dsa-4735">DSA-4735-1 grub2 -- обновление безопасности</a>,
которая касается нескольких CVE, связанных с
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/">уязвимостью UEFI SecureBoot 'BootHole' в GRUB2</a>.</p>


<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction appstream-glib "Исправление ошибок сборки в 2020 году и более поздних">
<correction asunder "Использование по умолчанию gnudb вместо freedb">
<correction b43-fwcutter "Проверка того, что удаление при использовании отличных от английской локалей успешно; не прерывать удаление в случае, если более не существуют некоторые файлы; исправление отсутствующих зависимостей от pciutils и ca-certificates">
<correction balsa "Предоставление идентификации сервера при проверке сертификатов, что позволяет успешно выполнять проверку при использовании заплаты glib-networking для CVE-2020-13645">
<correction base-files "Обновление для текущей редакции">
<correction batik "Исправление подделки запроса на стороне сервера через атрибуты xlink:href [CVE-2019-17566]">
<correction borgbackup "Исправление повреждения указателя, приводящего к потере данных">
<correction bundler "Обновление необходимой версии ruby-molinillo">
<correction c-icap-modules "Добавление поддержку для ClamAV 0.102">
<correction cacti "Исправление проблемы, при которой временные метки UNIX после 13 сентября 2020 отклоняются в качестве начала/конца графика; исправление удалённого выполнения кода [CVE-2020-7237], межсайтового скриптинга [CVE-2020-7106], проблемы CSRF [CVE-2020-13231]; отключение пользовательской учётной записи теперь не приводит сразу же к неправильным правам доступа [CVE-2020-13230]">
<correction calamares-settings-debian "Включение модуля displaymanager, исправляющего опции автоматического входа; использование xdg-user-dir для определения каталога рабочего стола">
<correction clamav "Новый выпуск основной ветки разработки; исправления безопасности [CVE-2020-3327 CVE-2020-3341 CVE-2020-3350 CVE-2020-3327 CVE-2020-3481]">
<correction cloud-init "Новый выпуск основной ветки разработки">
<correction commons-configuration2 "Предотвращение создания объекта при загрузке YAML-файлов [CVE-2020-1953]">
<correction confget "Исправление обработки с помощью модуля Python значений, содержащих <q>=</q>">
<correction dbus "Новый стабильный выпуск основной ветки разработки; предотвращение отказа в обслуживании [CVE-2020-12049]; предотвращение использования указателей после освобождения памяти, если у двух имён пользователей имеется один идентификатор">
<correction debian-edu-config "Исправление потери динамически выделенных IPv4-адресов">
<correction debian-installer "Обновление ABI Linux до версии 4.19.0-10">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction debian-ports-archive-keyring "Увеличение срока действия ключ 2020 (84C573CD4E1AFD6C) на один год; добавление ключа для автоматических подписей архива Debian Ports (2021); перенос ключа 2018 (ID: 06AED62430CB581C) в брелок удалённых ключей">
<correction debian-security-support "Обновление статуса поддержки некоторых пакетов">
<correction dpdk "Новый выпуск основной ветки разработки">
<correction exiv2 "исправление чрезмерно ограничительной заплаты [CVE-2018-10958 и CVE-2018-10999]; исправление отказа в обслуживании [CVE-2018-16336]">
<correction fdroidserver "Исправление проверки адреса Litecoin">
<correction file-roller "Исправление безопасности [CVE-2020-11736]">
<correction freerdp2 "Исправление входа с помощью смарткарт; исправления безопасности [CVE-2020-11521 CVE-2020-11522 CVE-2020-11523 CVE-2020-11524 CVE-2020-11525 CVE-2020-11526]">
<correction fwupd "Новый выпуск основной ветки разработки; исправление возможной проблемы с проверкой подписи [CVE-2020-10759]; использование изменённых ключей Debian для подписывания">
<correction fwupd-amd64-signed "Новый выпуск основной ветки разработки; исправление возможной проблемы с проверкой подписи [CVE-2020-10759]; использование изменённых ключей Debian для подписывания">
<correction fwupd-arm64-signed "Новый выпуск основной ветки разработки; исправление возможной проблемы с проверкой подписи [CVE-2020-10759]; использование изменённых ключей Debian для подписывания">
<correction fwupd-armhf-signed "Новый выпуск основной ветки разработки; исправление возможной проблемы с проверкой подписи [CVE-2020-10759]; использование изменённых ключей Debian для подписывания">
<correction fwupd-i386-signed "Новый выпуск основной ветки разработки; исправление возможной проблемы с проверкой подписи [CVE-2020-10759]; использование изменённых ключей Debian для подписывания">
<correction fwupdate "Использование изменённых ключей Debian для подписывания">
<correction fwupdate-amd64-signed "Использование изменённых ключей Debian для подписывания">
<correction fwupdate-arm64-signed "Использование изменённых ключей Debian для подписывания">
<correction fwupdate-armhf-signed "Использование изменённых ключей Debian для подписывания">
<correction fwupdate-i386-signed "Использование изменённых ключей Debian для подписывания">
<correction gist "Прекращение использования устаревшего API для авторизации">
<correction glib-networking "Возвращение ошибки плохой идентификации, если идентификация не задана [CVE-2020-13645]; ломает balsa старее версии 2.5.6-2+deb10u1, поскольку исправления для CVE-2020-13645 приводит к поломке проверки сертификата в balsa">
<correction gnutls28 "Исправление ошибок возобновления TL1.2; исправление утечки памяти; обработка сессионных билетов с нулевой длиной, что исправляет ошибки соединения в сессиях TLS1.2 при подключении к некоторым крупным поставщикам хостинга; исправление ошибки проверки с изменёнными цепочками">
<correction intel-microcode "Откат некоторых микрокодов до предыдущих редакций, обход зависаний при загрузке на Skylake-U/Y и Skylake Xeon E3">
<correction jackson-databind "Исправление многочисленных проблем безопасности, касающихся BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 и CVE-2019-17267]">
<correction jameica "Добавление mckoisqldb в classpath, что позволяет использовать дополнение SynTAX">
<correction jigdo "Исправление поддержки HTTPS в jigdo-lite и jigdo-mirror">
<correction ksh "Исправление проблемы с ограничением переменных окружения [CVE-2019-14868]">
<correction lemonldap-ng "Исправление регрессии в настройке nginx, добавленной в исправлении для CVE-2019-19791">
<correction libapache-mod-jk "Переименование файла настройки Apache, чтобы его можно было включать и выключать автоматически">
<correction libclamunrar "Новый стабильный выпуск основной ветки разработки; добавление метапакета без указания версии в имени">
<correction libembperl-perl "Обработка страниц ошибок из &gt;= 2.4.40">
<correction libexif "Исправления безопасности [CVE-2020-12767 CVE-2020-0093 CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; исправление переполнения буфера [CVE-2020-0182] и переполнения целых чисел [CVE-2020-0198]">
<correction libinput "Quirks: добавление атрибута интеграции trackpoint">
<correction libntlm "Исправление переполнения буфера [CVE-2019-17455]">
<correction libpam-radius-auth "Исправление переполнения буфера в поле для ввода пароля [CVE-2015-9542]">
<correction libunwind "Исправление ошибок сегментирования на mips; включение поддержки исключений C++ вручную только на архитектурах i386 и amd64">
<correction libyang "Исправление аварийной остановки из-за повреждения кэша, CVE-2019-19333, CVE-2019-19334">
<correction linux "Новый стабильный выпуск основной ветки разработки">
<correction linux-latest "Обновление ABI ядра до версии 4.19.0-10">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки">
<correction lirc "Исправление управления файлом настройки">
<correction mailutils "maidag: сброс setuid-привилегий для всех операций доставки кроме mda [CVE-2019-18862]">
<correction mariadb-10.3 "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2020-2752 CVE-2020-2760 CVE-2020-2812 CVE-2020-2814 CVE-2020-13249]; исправление регрессии в определении RocksDB ZSTD">
<correction mod-gnutls "Исправление возможной ошибки сегментирования при неудачном рукопожатии TLS; исправление ошибок тестирования">
<correction multipath-tools "kpartx: использование правильного пути к partx в правиле udev">
<correction mutt "Не выполнять проверку шифрования IMAP PREAUTH, если используется $tunnel">
<correction mydumper "Компоновка с libm">
<correction nfs-utils "statd: использование идентификатора пользователя из /var/lib/nfs/sm [CVE-2019-3689]; не устанавливать владельцем /var/lib/nfs пользователя statd">
<correction nginx "Исправление подделки запроса страницы ошибки [CVE-2019-20372]">
<correction nmap "Обновление размера ключа по умолчанию до 2048 бит">
<correction node-dot-prop "Исправление регрессии, введённой в исправлении CVE-2020-8116">
<correction node-handlebars "Запрет прямых вызовов <q>helperMissing</q> и <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-minimist "Исправление загрязнения прототипа [CVE-2020-7598]">
<correction nvidia-graphics-drivers "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2020-5963 CVE-2020-5967]">
<correction nvidia-graphics-drivers-legacy-390xx "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2020-5963 CVE-2020-5967]">
<correction openstack-debian-images "Установка resolvconf, если устанавливается cloud-init">
<correction pagekite "Предотвращение проблем с истёкшим сроком действия поставляемых SSL-сертификатов путём использования сертификатов из пакета ca-certificates">
<correction pdfchain "Исправление аварийной остановки при запуске">
<correction perl "Исправления многочисленных проблем безопасности, связанных с регулярными выражениями [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Исправление межсайтового скриптинга [CVE-2020-8035]">
<correction php-horde-gollem "Исправление межсайтового скриптинга в выводе breadcrumb [CVE-2020-8034]">
<correction pillow "Исправление чтения за пределами выделенного буфера памяти [CVE-2020-11538 CVE-2020-10378 CVE-2020-10177]">
<correction policyd-rate-limit "Исправление проблем учёта из-за повторного использования сокета">
<correction postfix "Новый стабильный выпуск основной ветки разработки; исправление ошибки сегментирования в клиентской роли tlsproxy, если отключена серверная роль; исправление ошибки <q>по умолчанию значение maillog_file_rotate_suffix использует минуты вместо месяцев</q>; исправление нескольких связанных с TLS проблем; исправления README.Debian">
<correction python-markdown2 "Исправление межсайтового скриптинга [CVE-2020-11888]">
<correction python3.7 "Предотвращение бесконечного цикла при чтении специально сформированных TAR-файлов с помощью модуля tarfile [CVE-2019-20907]; исправление столкновений хэша для IPv4Interface и IPv6Interface [CVE-2020-14422]; исправление отказа в обслуживании в urllib.request.AbstractBasicAuthHandler [CVE-2020-8492]">
<correction qdirstat "Исправлено сохранение настроенных пользователем категорий MIME">
<correction raspi3-firmware "Исправление опечатки, которая может приводить к невозможности загрузить систему">
<correction resource-agents "IPsrcaddr: опциональное использование <q>proto</q>, чтобы исправить регрессию при использовании без NetworkManager">
<correction ruby-json "Исправление небезопасного создания объектов [CVE-2020-10663]">
<correction shim "Использование изменённых ключей Debian для подписывания">
<correction shim-helpers-amd64-signed "Использование изменённых ключей Debian для подписывания">
<correction shim-helpers-arm64-signed "Использование изменённых ключей Debian для подписывания">
<correction shim-helpers-i386-signed "Использование изменённых ключей Debian для подписывания">
<correction speedtest-cli "Передача правильных заголовков для исправления теста скорости загрузки">
<correction ssvnc "Исправление записи за пределами выделенного буфера памяти [CVE-2018-20020], бесконечного цикла [CVE-2018-20021], неправильной инициализации [CVE-2018-20022], потенциального отказа в обслуживании [CVE-2018-20024]">
<correction storebackup "Исправление потенциального повышения привилегий [CVE-2020-7040]">
<correction suricata "Исправление сброса привилегий в режиме исполнения nflog">
<correction tigervnc "Прекращение использования libunwind на armel, armhf и arm64">
<correction transmission "Исправление возможного отказа в обслуживании [CVE-2018-10756]">
<correction wav2cdr "Использование целочисленных типов фиксированной длины по стандарту C99 для исправления утверждения времени исполнения на 64-битных архитектурах, отличных от amd64 и alpha">
<correction zipios++ "Исправление безопасности [CVE-2019-13453]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2020 4626 php7.3>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4679 keystone>
<dsa 2020 4680 tomcat9>
<dsa 2020 4681 webkit2gtk>
<dsa 2020 4682 squid>
<dsa 2020 4683 thunderbird>
<dsa 2020 4684 libreswan>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4690 dovecot>
<dsa 2020 4691 pdns-recursor>
<dsa 2020 4692 netqmail>
<dsa 2020 4694 unbound>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4696 nodejs>
<dsa 2020 4697 gnutls28>
<dsa 2020 4699 linux-signed-amd64>
<dsa 2020 4699 linux-signed-arm64>
<dsa 2020 4699 linux-signed-i386>
<dsa 2020 4699 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4707 mutt>
<dsa 2020 4708 neomutt>
<dsa 2020 4709 wordpress>
<dsa 2020 4710 trafficserver>
<dsa 2020 4711 coturn>
<dsa 2020 4712 imagemagick>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4714 chromium>
<dsa 2020 4716 docker.io>
<dsa 2020 4718 thunderbird>
<dsa 2020 4719 php7.3>
<dsa 2020 4720 roundcube>
<dsa 2020 4721 ruby2.5>
<dsa 2020 4722 ffmpeg>
<dsa 2020 4723 xen>
<dsa 2020 4724 webkit2gtk>
<dsa 2020 4725 evolution-data-server>
<dsa 2020 4726 nss>
<dsa 2020 4727 tomcat9>
<dsa 2020 4728 qemu>
<dsa 2020 4729 libopenmpt>
<dsa 2020 4730 ruby-sanitize>
<dsa 2020 4731 redis>
<dsa 2020 4732 squid>
<dsa 2020 4733 qemu>
<dsa 2020 4735 grub-efi-amd64-signed>
<dsa 2020 4735 grub-efi-arm64-signed>
<dsa 2020 4735 grub-efi-ia32-signed>
<dsa 2020 4735 grub2>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction golang-github-unknwon-cae "Проблема безопасности; не сопровождается">
<correction janus "Не поддерживается в стабильном выпуске">
<correction mathematica-fonts "Использует недоступный адрес для скачивания шрифтов">
<correction matrix-synapse "Проблемы безопасности; не поддерживается">
<correction selenium-firefoxdriver "Несовместим с новыми версиями Firefox ESR">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
