#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.2</define-tag>
<define-tag release_date>2019-11-16</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о втором обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction aegisub "Исправление аварийной остановки при выборе языка из конца списка <q>Язык для проверки правописания</q>; исправление аварийной остановки при клике правой кнопкой мыши в окне субтитров">
<correction akonadi "Исправление различных аварийных остановок / зависаний">
<correction base-files "Обновление /etc/debian_version для текущей редакции">
<correction capistrano "Исправление ошибки при удалении старых выпусков, если их слишком много">
<correction cron "Прекращение использования устаревшего API SELinux">
<correction cyrus-imapd "Исправление проблемы с потерей данных при обновлении с версии 3.0.0 или более ранних">
<correction debian-edu-config "Обработка файлов настройки для более новых версий Firefox ESR; добавление условной строки post-up в /etc/network/interfaces для eth0">
<correction debian-installer "Исправление проблемы с нечитаемыми шрифтами на экранах с высокой разрешающей способностью в образах сетевой установки при загрузке через EFI">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction distro-info-data "Добавление Ubuntu 20.04 LTS, Focal Fossa">
<correction dkimpy-milter "Новый стабильный выпуск основной ветки разработки; исправление поддержки sysvinit; отлов большего числа кодированных ASCII ошибок для улучшения устойчивости к плохим данным; исправление распаковки сообщений, чтобы проверка правильно работала при подписывании и одновременной передаче через milter">
<correction emacs "Обновление ключа пакетов EPLA">
<correction fence-agents "Исправление неполного удаления fence_amt_ws">
<correction flatpak "Новый стабильный выпуск основной ветки разработки">
<correction flightcrew "Исправления безопасности [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk "Исправление слишком агрессивного выбора шрифта из шрифтов Noto CJK в современных веб-браузерах в китайской локали">
<correction freetype "Правильная обработка фантомных точек для шрифтов с переменным хинтингом">
<correction gdb "Повторная сборка с учётом libbabeltrace с более высоким номером версии, чтобы избежать конфликта с загруженными ранее пакетами">
<correction glib2.0 "Проверка того, что libdbus-клиенты могут аутентифицироваться в GDBusServer также, как и в ibus">
<correction gnome-shell "Новый стабильный выпуск основной ветки разработки; исправление обрезания длинных сообщений в модальных диалогах Shell; исправление аварийной остановки при повторном выделении мёртвых активных объектов">
<correction gnome-sound-recorder "Исправление аварийной остановки при выборе записи">
<correction gnustep-base "Отключение службы gdomap, которая случайно была включена при обновлении с выпуска stretch">
<correction graphite-web "Удаление неиспользуемой функции <q>send_email</q> [CVE-2017-18638]; исправление ошибки в задаче cron, когда отсутствует база данных whisper">
<correction inn2 "Исправление взаимодействия наборов шифрования DHE">
<correction libapache-mod-auth-kerb "Исправление использования указателей после освобождения памяти, приводящего к аварийной остановке">
<correction libdate-holidays-de-perl "Пометка Международного дня детей (20-ое сентября) как праздника в Тюрингии, начиная с 2019 года">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libofx "Исправление разыменования null-указателя [CVE-2019-9656]">
<correction libreoffice "Исправление драйвера postgresql с целью обеспечения совместимости с PostgreSQL 12">
<correction libsixel "Исправление нескольких проблем безопасности [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt "Исправление висящего указателя в xsltCopyText [CVE-2019-18197]">
<correction lucene-solr "Отключение устаревшего вызова ContextHandler в solr-jetty9.xml; исправление прав доступа Jetty в указателе SOLR">
<correction mariadb-10.3 "Новый стабильный выпуск основной ветки разработки">
<correction modsecurity-crs "Исправление PHP-сценария загрузки правил [CVE-2019-13464]">
<correction mutter "Новый стабильный выпуск основной ветки разработки">
<correction ncurses "Исправление нескольких проблем безопасности [CVE-2019-17594 CVE-2019-17595] и других проблем в tic">
<correction ndppd "Прекращение использования PID-файлов, открытых для записи всем пользователям, что ранее приводило к поломке сценариев инициализации службы">
<correction network-manager "Исправление прав доступа к <q>/var/lib/NetworkManager/secret_key</q> и /var/lib/NetworkManager">
<correction node-fstream "Исправление перезаписи произвольных файлов [CVE-2019-13173]">
<correction node-set-value "Исправление загрязнения прототипа [CVE-2019-10747]">
<correction node-yarnpkg "Обязательное использование HTTPS для обычных регистров">
<correction nx-libs "Исправление регрессий, добавленных в предыдущей загрузке и касающихся x2go">
<correction open-vm-tools "Исправление утечек памяти и обработки ошибок">
<correction openvswitch "Обновление debian/ifupdown.sh с целью разрешения настройки максимального размера пакета; исправление зависимостей для использования Python 3">
<correction picard "Обновление переводов для исправления аварийной остановки при использовании испанской локали">
<correction plasma-applet-redshift-control "Исправление ручного режима при использовании с redshift версий новее 1.12">
<correction postfix "Новый стабильный выпуск основной ветки разработки; обходное решение проблемы низкой производительности интерфейса обратной петли TCP">
<correction python-cryptography "Исправление ошибок тестов при сборке с новыми версиями OpenSSL; исправление утечки памяти, возникающей при выполнении грамматического разбора расширений x509-сертификатов типа AIA">
<correction python-flask-rdf "Добавление зависимости от python{3,}-rdflib">
<correction python-oslo.messaging "Новый стабильный выпуск основной ветки разработки; исправление переключения адресата соединения в случае исчезновения кластерной ноды rabbitmq">
<correction python-werkzeug "Проверка того, чтобы контейнеры Docker имели уникальные отладочные PIN [CVE-2019-14806]">
<correction python2.7 "Исправление нескольких проблем безопасности [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota "Исправление потребления 100% ресурсов ЦП процессом rpc.rquotad">
<correction rpcbind "Разрешение включения удалённых вызовов во время исполнения">
<correction shelldap "Починка SASL-аутентификаций, добавление опции 'sasluser'">
<correction sogo "Исправление отображения сообщений, подписанных ключом PGP">
<correction spf-engine "Новый стабильный выпуск основной ветки разработки; исправление поддержки sysvinit">
<correction standardskriver "Исправление устаревшего предупреждения из config.RawConfigParser; использование внешней команды <q>ip</q> вместо устаревшей команды <q>ifconfig</q>">
<correction swi-prolog "Использование HTTPS при обращении к pack-серверам основной ветки разработки">
<correction systemd "core: не передавать ошибку выполнения reload в результат работы службы; исправление ошибок sync_file_range в контейнерах nspawn на архитектурах arm, ppc; исправление неработающего параметра RootDirectory при использовании вместе с параметром User; проверка, чтобы ограничения доступа в интерфейсе D-Bus systemd-resolved работали правильно [CVE-2019-15718]; исправление параметра StopWhenUnneeded=true для mount-юнитов; восстановление работы параметра MountFlags=shared">
<correction tmpreaper "Предотвращение поломки служб systemd, использующих параметр PrivateTmp=true">
<correction trapperkeeper-webserver-jetty9-clojure "Восстановление SSL-совместимости с более новыми версиями Jetty">
<correction tzdata "Новая версия основной ветки разработки">
<correction ublock-origin "Новая версия основной ветки разработки, совместимая с Firefox ESR68">
<correction uim "Восстановление libuim-data в качестве переходного пакета, что исправляет некоторые проблемы, возникающие после обновления до выпуска buster">
<correction vanguards "Новый стабильный выпуск основной ветки разработки; предотвращение повторной загрузки настроек tor через SIGHUP, что приводило к отказу в обслуживании для защиты vanguards">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>


<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction firefox-esr "[armel] Более не поддерживается из-за сборочной зависимости от nodejs">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
