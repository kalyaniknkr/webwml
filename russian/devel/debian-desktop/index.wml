#use wml::debian::template title="Debian на рабочих станциях" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="a1eaa8012e6459917b80adb073e753fab27bfdb5" maintainer="Lev Lamberov"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">Наша философия</a></li>
<li><a href="#help">Как вы можете помочь</a></li>
<li><a href="#join">Присоединяйтесь к нам</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian Desktop &mdash; это группа добровольцев, желающих создать наилучшую операционную систему для использования на домашних и корпоративных рабочих станциях. Наш девиз: <q>Программное обеспечение просто работает.</q> Наша цель: донести Debian, GNU и Linux до мира.</p>
</aside>

<h2><a id="philosophy">Наша философия</a></h2>

<p>
Мы осознаём, что существует целое множество
<a href="https://wiki.debian.org/DesktopEnvironment">окружений
рабочего стола</a>, и мы будем поддерживать их использование. В том числе мы
должны убедиться, что они хорошо работают в Debian. Наша цель состоит в том, чтобы
сделать графически интерфейсы простыми для их использования начинающими пользователями,
но так, чтобы они позволяли продвинутым пользователям и экспертам настраивать
систему так, как они хотят.
</p>

<p>
Мы попытаемся гарантировать, что программы настроены для целей наиболее
распространённого использования. Например, учётная запись обычного пользователя, создаваемая
в ходе установки, должна иметь права на проигрывание аудио и видео, печать
и управление системой через команду sudo. Кроме того, мы бы хотели свести вопросы
<a href="https://wiki.debian.org/debconf">debconf</a> (систему управления
настройками Debian) к абсолютному минимуму.
Нет нужды в том, чтобы во время установки предоставлять пользователю сложные технические
подробности. Вместо этого мы попытаемся гарантировать, что вопросы debconf
будут понятны пользователям. Новичое может совсем не понять, что значат
эти вопросы. Однако эксперт вполне может настроить окружение рабочего стола
и после завершения установки.
</p>

<h2><a id="help">Как вы можете помочь</a></h2>

<p>
Мы ищем целеустремлённых людей, которые добиваются успеха. Вам не обязательно быть
разработчиком Debian (DD) для того, чтобы отправлять заплаты или создавать пакеты.
Ядро команды Debian для рабочих станций гарантирует, что ваша работа будет включена в дистрибутив.
Вот кое-что из того, с чем вы можете помочь:
</p>

<ul>
  <li>Тестирование окружения рабочего стола по умолчанию (или любого другого окружения рабочего стола) для готовящегося выпуска. Возьмите один из <a href="$(DEVEL)/debian-installer/">тестируемых образов</a> и отправьте ваши комментарии в <a href="https://lists.debian.org/debian-desktop/">список рассылки debian-desktop</a>.</li>
  <li>Присоединитесь к <a href="https://wiki.debian.org/DebianInstaller/Team">команде установщика Debian</a> и помогите улучшить <a href="$(DEVEL)/debian-installer/">debian-installer</a> &mdash; интерфейс GTK+ нуждается в вас.</li>
  <li>Вы можете помочь <a href="https://wiki.debian.org/Teams/DebianGnome">команде Debian GNOME</a>, <a href="https://qt-kde-team.pages.debian.net/">сопровождающим Debian Qt/KDE и команде Debian KDE Extras</a>, а также <a href="https://salsa.debian.org/xfce-team/">Группе Debian Xfce</a>, создавая пакеты, исправляя ошибки, улучшая документацию, тестируя ПО и делая много другое.</li>
  <li>Помогите улучшить <a href="https://packages.debian.org/debconf">debconf</a>, снижая приоритет вопросов или удаляя ненужные вопросы из пакетов. Сделайте необходимые вопросы debconf простыми для понимания.</li>
  <li>У вас есть навыки дизайна? Тогда почему бы не поработать над <a href="https://wiki.debian.org/DebianDesktop/Artwork">оформлением рабочего стола Debian</a>.</li>
</ul>

<h2><a id="join">Присоединяйтесь к нам</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li><strong>Вики:</strong> посетите нашу вики <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> (некоторые статьи могут быть устаревшими).</li>
  <li><strong>Список рассылки:</strong> обсудите с нами ваши вопросы в списке рассылки <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.</li>
  <li><strong>Канал IRC:</strong> поболтайте с нами в IRC. Присоединяйтесь к каналу #debian-desktop в <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org)</li>
</ul>
