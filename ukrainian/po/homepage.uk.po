# translation of homepage to Ukrainian
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net> 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: homepage\n"
"PO-Revision-Date: 2021-01-16 16:00+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: Ukrainian <ukrainian>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Універсальна операційна система"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr ""

#: ../../english/index.def:15
#, fuzzy
#| msgid "DebConf19 Group Photo"
msgid "DebConf Logo"
msgstr "Групове фото DebConf19"

#: ../../english/index.def:19
#, fuzzy
#| msgid "DC19 Group Photo"
msgid "DC22 Group Photo"
msgstr "Групове фото DC19"

#: ../../english/index.def:22
#, fuzzy
#| msgid "DebConf19 Group Photo"
msgid "DebConf22 Group Photo"
msgstr "Групове фото DebConf19"

#: ../../english/index.def:26
msgid "Debian Reunion Hamburg 2023"
msgstr ""

#: ../../english/index.def:29
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr ""

#: ../../english/index.def:33
msgid "Mini DebConf Regensburg 2021"
msgstr ""

#: ../../english/index.def:36
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr ""

#: ../../english/index.def:40
msgid "Screenshot Calamares Installer"
msgstr "Знімок екрану встановлювача Calamares"

#: ../../english/index.def:43
msgid "Screenshot from the Calamares installer"
msgstr "Знімок екрану встановлювача Calamares"

#: ../../english/index.def:47 ../../english/index.def:50
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian подібний до швейцарського армійського ніжа"

#: ../../english/index.def:54
msgid "People have fun with Debian"
msgstr "Люди весело проводять час з Debian"

#: ../../english/index.def:57
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr ""

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Трохи від Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Блог"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Мікроновини"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Мікроновини від Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Планета"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Планета Debian"
