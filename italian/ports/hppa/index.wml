#use wml::debian::template title="Port su PA-RISC" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hppa/menu.inc"
#use wml::debian::translation-check translation="c995e5898bf6fd285240c8f234c7e7c3865112fc" maintainer="Luca Monducci"

<h1>Debian per PA-RISC</h1>

<h2>Stato</h2>

<p>HPPA è stata un'architettura supportata ufficialmente da Debian 
a partirte da Debian 3.0 (woody) e fino al rilascio di Debian 6.0
(squeeze). Ulteriori informazioni possono essere trovate su
<a href="https://parisc.wiki.kernel.org/">https://parisc.wiki.kernel.org/</a>.</p>

<p>Se si hanno domande o si vuole contribuire per prima cosa occorre
iscriversi alla lista di messaggi debian-hppa come descritto sotto!</p>

<h2>Contatti</h2>

<p>Il principale istigatore di questo port è stato Bdale Garbee, ma adesso
non contribuisce più attivamente.
Il miglior luogo in cui fare domande è la lista di messaggi.</p>

<h2>Lista di messaggi</h2>

<p>Per iscriversi alla lista di messaggi di questo port inviare un messaggio
con la parola "subscribe" nell'oggetto a
<a href="mailto:debian-hppa-request@lists.debian.org">\
debian-hppa-request@lists.debian.org</a>, oppure usare
<a href="$(HOME)/MailingLists/subscribe">la pagina d'iscrizione alla lista di
messaggi</a>.</p>

<p>La lista è archiviata nell'<a href="https://lists.debian.org/debian-hppa/">\
archivio della lista</a>.</p>

<h2>Riferimenti</h2>

<ul>
	<li><a href="https://parisc.wiki.kernel.org/">Sito web del progetto
	PA-RISC Linux</a></li>

	<li><a href="https://www.openpa.net/">Progetto OpenPA</a></li>

	<li><a href="https://web.archive.org/web/20120119051222/http://h20000.www2.hp.com/bc/docs/support/SupportManual/c02722594/c02722594.pdf">
	Documentazione dei sistemi (da archive.org)</a>

	<li><a href="https://web.archive.org/web/20070613192257/http://h21007.www2.hp.com/portal/site/dspp">
	Manuali, documentazione, ecc. sull'architettura HP PA-RISC (da archive.org)</a>
</ul>
