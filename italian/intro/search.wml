#use wml::debian::template title="Informazioni su come usare il motore di ricerca Debian"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Ceppo"

<p>
Il progetto Debian offre il proprio motore di ricerca a <a
href="https://search.debian.org/">https://search.debian.org/</a>. Ecco alcune
indicazioni su come usarlo e avviare ricerche semplici, così come ricerche più
complesse con gli operatori booleani.
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>Ricerca semplice</h3>
      <p>
      Il modo più semplice per usare il motore è inserire una singola parola
      nel campo di ricerca e premere [Invio]. In alternativa, si può
      cliccare il pulsante <em>Cerca</em>. Il motore di ricerca elencherà tutte
      le pagine del sito che contengono tale parola. Per la maggior parte delle
      ricerche, questo metodo dovrebbe dare buoni risultati.
      </p>
      <p>
      In alternativa, si può cercare più di una parola. Anche in questo caso,
      dovrebbero essere mostrate tutte le pagine del sito di Debian che
      contengono le parole inserite. Per cercare frasi, inserirle tra
      virgolette ("). Si noti che il motore di ricerca non tiene conto delle
      maiuscole, quindi una ricerca con <code>gcc</code> trova sia «gcc», sia
      «GCC».
      </p>
      <p>
      Sotto il campo di ricerca si può scegliere quanti risultati mostrare per
      pagina. È possibile anche scegliere una lingua diversa; la ricerca nel
      sito di Debian supporta quasi 40 lingue differenti.
      </p>
    </div>
  </div>

  <!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>Ricerca booleana</h3>
      <p>
      Se una ricerca semplice non è sufficiente, si possono usare gli operatori
      di ricerca booleani. Si può scegliere tra <em>AND</em>, <em>OR</em> e
      <em>NOT</em> o combinarli tutti e tre. Bisogna usare lettere maiuscole
      per tutti gli operatori, in modo che il motore di ricerca li riconosca.
      </p>
      <ul>
        <li>
          <b>AND</b> combina due espressioni e mostra le pagine che contengono
          entrambe le parole. Ad esempio, <code>gcc AND patch</code> trova
          tutte le pagine che contengono sia «gcc» che «patch». In questo caso
          si otterrà lo stesso risultato che cercando <code>gcc patch</code>,
          ma un <code>AND</code> esplicito può essere usato in combinazione con
          altri operatori.
        </li>
        <li>
          <b>OR</b> mostra le pagine che contengono una delle parole. <code>gcc
          OR patch</code> trova qualunque pagina che contenga «gcc» o «patch».
        </li>
        <li>
          <b>NOT</b> si usa per escludere termini di ricerca dai risultati. Ad
          esempio, <code>gcc NOT patch</code> trova tutte le pagine che
          contengono «gcc» ma non «patch». <code>gcc AND NOT PATCH</code> dà lo
          stesso risultato, ma cercare <code>NOT patch</code> non è supportato.
        </li>
        <li>
          <b>(...)</b> si può usare per raggruppare espressioni. Ad esempio,
          <code>(gcc OR make) NOT patch</code> trova tutte le pagine che
          contengono «gcc» o «make» ma non contengono «patch».
        </li>
      </ul>
    </div>
  </div>
</div>
