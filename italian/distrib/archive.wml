#use wml::debian::template title="Archivi delle distribuzioni"
#use wml::debian::translation-check translation="b4319ab75a5556391ce61af8f3457785e5efdbe7" maintainer="Luca Monducci"
#first translator and maintainer Johan Haggi
#use wml::debian::toc
<toc-display />

<toc-add-entry name="old-archive">debian-archive</toc-add-entry>

<p>Se si ha la necessità di accedere a una delle vecchie distribuzioni di
Debian, la si può trovare negli <a href="https://archive.debian.org/debian/">Archivi
Debian</a>, <tt>https://archive.debian.org/debian/</tt>.</p>

<p>Le release sono archiviate secondo i loro nomi in codice sotto la directory dists/</p>
<ul>
  <li><a href="../releases/stretch/">stretch</a> è Debian 9</li>
  <li><a href="../releases/jessie/">jessie</a> è Debian 8.0</li>
  <li><a href="../releases/wheezy/">wheezy</a> è Debian 7.0</li>
  <li><a href="../releases/squeeze/">squeeze</a> è Debian 6.0</li>
  <li><a href="../releases/lenny/">lenny</a> è Debian 5.0</li>
  <li><a href="../releases/etch/">etch</a> è Debian 4.0</li>
  <li><a href="../releases/sarge/">sarge</a> è Debian 3.1</li>
  <li><a href="../releases/woody/">woody</a> è Debian 3.0</li>
  <li><a href="../releases/potato/">potato</a> è Debian 2.2</li>
  <li><a href="../releases/slink/">slink</a> è Debian 2.1</li>
  <li><a href="../releases/hamm/">hamm</a> è Debian 2.0</li>
  <li>bo è Debian 1.3</li>
  <li>rex è Debian 1.2</li>
  <li>buzz è Debian 1.1</li>
</ul>


<p>Attualmente sono disponibili solo i sorgenti per i rilasci precedenti
a bo e sono disponibili sorgenti e binari per bo e per i rilasci successivi.
Col passare del tempo saranno cancellati i pacchetti binari dei rilasci più
vecchie.</p>

<p>Se si usa APT le righe per il file sources.list sono simili a queste:</p>
<pre>
  deb http://archive.debian.org/debian/ hamm contrib main non-free
</pre>
<p>o</p>
<pre>
  deb http://archive.debian.org/debian/ bo bo-unstable contrib main non-free
</pre>

<p>L'accesso con rsync è disponibile tramite <pre>rsync.archive.debian.org</pre></p>

<p>Nell'elenco seguente ci sono i mirror che contengono archive:</p>

#include "$(ENGLISHDIR)/distrib/archive.mirrors"
<archivemirrors>

<toc-add-entry name="non-us-archive">debian-non-US archive</toc-add-entry>

<p>In passato, c'erano dei software pacchettizzati per Debian che non potevano
essere distribuiti negli USA (e in altri paesi) a causa di restrizioni legali
sull'esportazione di programmi crittografici o a causa di brevetti software.
Debian manteneva un archivio speciale chiamato archivio <q>non-US</q>.</p>

<p>Questi pacchetti sono stati inclusi nell'archivio <q>main</q> in Debian 3.1
e l'archivio <q>debian-non-US</q> non è più utilizzato; è ora
<em>archiviato</em> su archive.debian.org</p>

<p>Questi sono ancora disponibili dalla macchina archive.debian.org.
Si possono usare i seguenti metodi di accesso:</p>
<blockquote><p>
<a href="https://archive.debian.org/debian-non-US/">https://archive.debian.org/debian-non-US/</a><br>
rsync://rsync.archive.debian.org/debian-non-US/
</p></blockquote>

<p>Per usare questi pacchetti con APT, usare righe simili a queste in sources.list:</p>

<pre>
  deb http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
  deb-src http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
</pre>
