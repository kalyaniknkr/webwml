#use wml::debian::template title="Debian &ldquo;bookworm&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"

<p>Debian <current_release_bookworm> was
released on <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 was initially released on <:=spokendate('2023-06-10'):>."
/>
The release included many major
changes, described in
our <a href="$(HOME)/News/2023/20230610">press release</a> and
the <a href="releasenotes">Release Notes</a>.</p>

# <p><strong>Debian 12 has been superseded by
# <a href="../trixie/">Debian 13 (<q>trixie</q>)</a>.
# </strong></p>

### This paragraph is orientative, please review before publishing!
# <p><strong>However, bookworm benefits from Long Term Support (LTS) until
# 30th June, 2028. The LTS is limited to i386, amd64, armel, armhf and arm64.
# All other architectures are no longer supported in bookworm.
# For more information, please refer to the <a
# href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
# </strong></p>

<p>
The Debian 12 life cycle encompasses five years: the initial three years of
full Debian support, until <:=spokendate('2026-06-10'):>, and two years of
Long Term Support (LTS), until <:=spokendate('2028-06-30'):>. The set of
supported architectures is reduced during the LTS term. For more information,
please refer to the <a href="security/">Security Information</a> webpage and
the <a  href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
</p>

<p>To obtain and install Debian, see
the <a href="debian-installer/">installation information</a> page and the
<a href="installmanual">Installation Guide</a>. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Computer architectures supported at initial release of bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
<a href="../reportingbugs">report other issues</a> to us.</p>

<p>Last but not least, we have a list of <a href="credits">people who take
credit</a> for making this release happen.</p>
