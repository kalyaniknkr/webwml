<define-tag pagetitle>General Resolution: tag2upload</define-tag>
<define-tag status>W</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Discussion Period:</th>
	<td>2024-06-27</td>
	<td></td>
      </tr>
#      <tr>
#	<th>Voting period:</th>
#	<td>Saturday 2023-12-09 00:00:00 UTC</td>
#	<td>Friday 2023-12-22 23:59:59 UTC</td>
#      </tr>
    </table>

    <vproposera />
    <p>Sean Whitton [<email spwhitto@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2024/06/msg00561.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2024/07/msg00025.html'>Withdrawal</a>]
    </p>
    <vsecondsa />
    <ol>
        <li>Didier Raboud [<email odyx@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/06/msg00562.html'>mail</a>]</li>
        <li>Jonathan McDowell [<email noodles@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/06/msg00566.html'>mail</a>]</li>
        <li>Ian Jackson [<email iwj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/06/msg00568.html'>mail</a>]</li>
        <li>Aníbal Monsalve Salazar [<email anibal@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/06/msg00571.html'>mail</a>]</li>
        <li>Sam Hartman [<email hartmans@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/06/msg00581.html'>mail</a>]</li>
        <li>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2024/06/msg00585.html'>mail</a>]</li>
    </ol>
    <vtexta />
<h3>Choice 1</h3>

<p>tag2upload allows DDs and DMs to upload simply by using the
git-debpush(1) script to push a signed git tag.</p>

<ol>
<li>tag2upload, in the form designed and implemented by Sean Whitton and
   Ian Jackson, and design reviewed by Jonathan McDowell and Russ
   Allbery, should be deployed to official Debian infrastructure.</li>

<li>Under Constitution §4.1(3), we overrule the ftpmaster delegate's
   decision: the Debian Archive should be configured to accept and trust
   uploads from the tag2upload service.</li>

<li>Future changes to tag2upload should follow normal Debian processes.</li>

<li>Nothing in this resolution should be taken as requiring maintainers
   to use any particular git or salsa workflows.</li>
</ol>

#    <vquorum />
#
#     <p>
#        With the current list of <a href="vote_002_quorum.log">voting
#          developers</a>, we have:
#     </p>
#    <pre>
##include 'vote_002_quorum.txt'
#    </pre>
##include 'vote_002_quorum.src'
#
#
#    <vstatistics />
#    <p>
#	For this GR, like always,
##                <a href="https://vote.debian.org/~secretary/gr_cra_pld/">statistics</a>
#               <a href="suppl_002_stats">statistics</a>
#             will be gathered about ballots received and
#             acknowledgements sent periodically during the voting
#             period.
#               Additionally, the list of <a
#             href="vote_002_voters.txt">voters</a> will be
#             recorded. Also, the <a href="vote_002_tally.txt">tally
#             sheet</a> will also be made available to be viewed.
#         </p>
#
#    <vmajorityreq />
#    <p>
#      All proposal need a simple majority.
#    </p>
##include 'vote_002_majority.src'
#
#    <voutcome />
##include 'vote_002_results.src'
#
    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Debian Project Secretary</a>
      </address>

