-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2        abhijith	Abhijith PA
    3           aboll	Andreas Boll
    4            adsb	Adam D. Barratt
    5           alexm	Alex Muntada
    6           alexp	Alex Pennace
    7            alfs	Stefan Alfredsson
    8        alteholz	Thorsten Alteholz
    9        amacater	Andrew Martin Adrian Cater
   10        ametzler	Andreas Metzler
   11             ana	Ana Beatriz Guerrero López
   12         anarcat	Antoine Beaupré
   13            anbe	Andreas Beckmann
   14        andrewsh	Andrej Shadura
   15        angdraug	Dmitry Borodaenko
   16           angel	Angel Abad
   17          anibal	Anibal Monsalve Salazar
   18          ansgar	Ansgar
   19        anuradha	Anuradha Weeraman
   20             apo	Markus Koschany
   21            ardo	Ardo van Rangelrooij
   22             asb	Andrew Starr-Bochicchio
   23           asias	Asias He
   24         aurel32	Aurelien Jarno
   25              az	Alexander Zangerl
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26        azekulic	Alen Zekulic
   27            bage	Bastian Germann
   28        ballombe	Bill Allombert
   29             bap	Barak A. Pearlmutter
   30           bartm	Bart Martens
   31           bayle	Christian Bayle
   32          bbaren	Benjamin Barenblat
   33         bblough	William Blough
   34           bdale	Bdale Garbee
   35            benh	Ben Hutchings
   36           biebl	Michael Biebl
   37         bigeasy	Sebastian Andrzej Siewior
   38           blade	Eduard Bloch
   39           bluca	Luca Boccassi
   40           bmarc	Bertrand Marc
   41           bootc	Chris Boot
   42         bottoms	Maitland Bottoms
   43         bremner	David Bremner
   44         broonie	Mark Brown
   45            bunk	Adrian Bunk
   46            bzed	Bernd Zeimetz
   47        calculus	Jerome Georges Benoit
   48          carnil	Salvatore Bonaccorso
   49           cavok	Domenico Andreoli
   50          cbiedl	Christoph Biedl
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51       chronitis	Gordon Ball
   52        cjwatson	Colin Watson
   53             ckk	Christian Kastner
   54           cklin	Chuan-kai Lin
   55           clint	Clint Adams
   56        codehelp	Neil Williams
   57         coucouf	Aurélien Couderc
   58          csmall	Craig Small
   59             cts	Christian T. Steigies
   60           cwryu	Changwoo Ryu
   61          czchen	ChangZhuo Chen
   62             dai	Daisuke Higuchi
   63          daniel	Daniel Baumann
   64           dapal	David Paleino
   65            dave	Dave Holland
   66       debalance	Philipp Huebner
   67        deltaone	Patrick Franz
   68       dktrkranz	Luca Falavigna
   69          dlange	Daniel Lange
   70           dlehn	David I. Lehn
   71         dmartin	Dale Martin
   72             dmn	Damyan Ivanov
   73             dod	Dominique Dumont
   74         dogsleg	Lev Lamberov
   75             don	Don Armstrong
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76         donkult	David Kalnischkies
   77       dtorrance	Douglas Andrew Torrance
   78            duck	Marc Dequènes
   79        ehashman	Elana Hashman
   80          elbrus	Paul Mathijs Gevers
   81        emollier	Étienne Mollier
   82          enrico	Enrico Zini
   83        eriberto	Joao Eriberto Mota Filho
   84            eric	Eric Dorland
   85           eriks	Erik Schanze
   86           eriol	Daniele Tricoli
   87             evo	Davide Puricelli
   88           fabbe	Fabian Fagerholm
   89             faw	Felipe Augusto van de Wiel
   90        federico	Federico Ceratto
   91           felix	Félix Sipma
   92          fgeyer	Felix Geyer
   93             flo	Florian Lohoff
   94         florian	Florian Ernst
   95        formorer	Alexander Wirt
   96         fpeters	Frederic Peters
   97        francois	Francois Marier
   98         frankie	Francesco Lovergine
   99         ftobich	Fabio Augusto De Muzio Tobich
  100           fuddl	Bruno Kleinert
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101           georg	Georg Faerber
  102        georgesk	Georges Khaznadar
  103          ginggs	Graham Inggs
  104             gio	Giovanni Mascellani
  105         giovani	Giovani Augusto Ferreira
  106           gladk	Anton Gladky
  107        glandium	Mike Hommey
  108        glaubitz	John Paul Adrian Glaubitz
  109          glondu	Stéphane Glondu
  110         godisch	Martin A. Godisch
  111           gotom	Masanori Goto
  112          gregoa	Gregor Herrmann
  113            gspr	Gard Spreemann
  114         guilhem	Guilhem Moulin
  115         guillem	Guillem Jover
  116          gusnan	Andreas Rönnquist
  117            guus	Guus Sliepen
  118           gwolf	Gunnar Wolf
  119        hartmans	Sam Hartman
  120         helmutg	Helmut Grohne
  121         henrich	Hideki Yamane
  122         hertzog	Raphaël Hertzog
  123      hlieberman	Harlan Lieberman-Berg
  124             hmh	Henrique de Moraes Holschuh
  125        hntourne	Henry-Nicolas Tourneur
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126         hoexter	Sven Hoexter
  127          holger	Holger Levsen
  128      hvhaugwitz	Hannes von Haugwitz
  129            ianw	Ian Wienand
  130             ijc	Ian James Campbell
  131       intrigeri	Intrigeri
  132        ishikawa	Ishikawa Mutsumi
  133           ivodd	Ivo De Decker
  134        iwamatsu	Nobuhiro Iwamatsu
  135             jak	Julian Andres Klode
  136         jaldhar	Jaldhar H. Vyas
  137           jandd	Jan Dittberner
  138          jaqque	John Robinson
  139          jathan	Jonathan Bustillos
  140          jbicha	Jeremy Bicha
  141             jcc	Jonathan Cristopher Carter
  142            jcfp	Jeroen Ploemen
  143        jcristau	Julien Cristau
  144          jelmer	Jelmer Vernooij
  145          jlines	John Lines
  146             jmm	Moritz Muehlenhoff
  147            jmtd	Jonathan Dowland
  148           joerg	Joerg Jaspert
  149         joostvb	Joost van Baal
  150           jordi	Jordi Mallach
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151           josch	Johannes Schauer Marin Rodrigues
  152           josue	Josué Ortega
  153       jpmengual	Jean-Philippe MENGUAL
  154          jpuydt	Julien Puydt
  155          jrtc27	Jessica Clarke
  156              js	Jonas Smedegaard
  157        jspricke	Jochen Sprickerhof
  158       jvalleroy	James Valleroy
  159            kaol	Kari Pahula
  160          keithp	Keith Packard
  161          kenhys	HAYASHI Kentaro
  162          khalid	Khalid Aziz
  163            kibi	Cyril Brulebois
  164        kilobyte	Adam Borowski
  165            knok	Takatsugu Nokubi
  166           kobla	Ondřej Kobližek
  167          koster	Kanru Chen
  168         kreckel	Richard Kreckel
  169      kritzefitz	Sven Bartscher
  170            kula	Marcin Kulisz
  171           lamby	Chris Lamb
  172           laney	Iain Lane
  173           lange	Thomas Lange
  174         larjona	Laura Arjona Reina
  175        lavamind	Jerome Charaoui
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176         lechner	Felix Lechner
  177         legoktm	Kunal Mehta
  178         lenharo	Daniel Lenharo de Souza
  179             leo	Carsten Leonhardt
  180        lfaraone	Luke Faraone
  181          lkajan	Laszlo Kajan
  182         lopippo	Filippo Rusconi
  183           lucas	Lucas Nussbaum
  184         lyknode	Baptiste Beauplat
  185         madduck	Martin F. Krafft
  186             mak	Matthias Klumpp
  187           mattb	Matthew Brown
  188         matthew	Matthew Vernon
  189     matthieucan	Matthieu Caneill
  190          mattia	Mattia Rizzolo
  191            maxx	Martin Wuertele
  192            maxy	Maximiliano Curia
  193           mazen	Mazen Neifer
  194          mbanck	Michael Banck
  195         mbehrle	Mathias Behrle
  196              md	Marco d'Itri
  197            mejo	Jonas Meurer
  198          merker	Karsten Merker
  199          merkys	Andrius Merkys
  200          meskes	Michael Meskes
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201           metal	Marcelo Jorge Vieira
  202             mfv	Matteo F. Vescovi
  203           micah	Micah Anderson
  204           micha	Micha Lenk
  205             mih	Michael Hanke
  206            mika	Michael Prokop
  207           milan	Milan Kupcevic
  208         mitya57	Dmitry Shachnev
  209        mjeanson	Michael Jeanson
  210         moeller	Steffen Möller
  211           mones	Ricardo Mones Lastra
  212           morph	Sandro Tosi
  213           mpitt	Martin Pitt
  214     mtecknology	Michael Lustfield
  215        mwhudson	Michael Hudson-Doyle
  216            myon	Christoph Berg
  217          nattie	Nattie Mayer-Hutchings
  218          nbreen	Nicholas Breen
  219           neilm	Neil McGovern
  220           nickm	Nick Morrott
  221         nicolas	Nicolas Boulenguez
  222          nilesh	Nilesh Patra
  223           noahm	Noah Meyerhans
  224          nodens	Clément Hermann
  225            noel	Noèl Köthe
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226         noodles	Jonathan McDowell
  227        nthykier	Niels Thykier
  228           ntyni	Niko Tyni
  229            odyx	Didier Raboud
  230           ohura	Makoto OHURA
  231           olasd	Nicolas Dandrimont
  232            olek	Olek Wojnar
  233            olly	Olly Betts
  234         onlyjob	Dmitry Smirnov
  235             orv	Benda Xu
  236            pabs	Paul Wise
  237    paddatrapper	Kyle Robbertze
  238        paravoid	Faidon Liambotis
  239          paride	Paride Legovini
  240         paultag	Paul Richards Tagliamonte
  241             peb	Pierre-Elliott Bécue
  242             pgt	Pierre Gruet
  243           philh	Philip Hands
  244            phls	Paulo Henrique de Lima Santana
  245            piem	Paul Brossier
  246            pini	Gilles Filippini
  247             pjb	Phil Brooke
  248           pkern	Philipp Kern
  249          plessy	Charles Plessy
  250       pmatthaei	Patrick Matthäi
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251           pollo	Louis-Philippe Véronneau
  252        porridge	Marcin Owsiany
  253         praveen	Praveen Arimbrathodiyil
  254             qjb	Jay Berkenbilt
  255             ras	Russell Stuart
  256    rattusrattus	Andy Simpkins
  257         rbalint	Balint Reczey
  258          rbasak	Robie Basak
  259             reg	Gregory Colpart
  260         reichel	Joachim Reichel
  261      rfrancoise	Romain Francoise
  262         rlaager	Richard Laager
  263         roberto	Roberto C. Sanchez
  264        roehling	Timo Röhling
  265          roland	Roland Rosenfeld
  266            rosh	Roger Shimizu
  267        rousseau	Ludovic Rousseau
  268           rover	Roberto Lumbreras
  269             rra	Russ Allbery
  270             rrs	Ritesh Raj Sarraf
  271     rvandegrift	Ross Vandegrift
  272       samueloph	Samuel Henrique
  273        santiago	Santiago Ruano Rincón
  274           satta	Sascha Steinbiss
  275          sbadia	Sebastien Badia
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276             seb	Sebastien Delafond
  277       sebastien	Sébastien Villemot
  278        sergiodj	Sergio Durigan Junior
  279         serpent	Tomasz Rybak
  280           sesse	Steinar H. Gunderson
  281              sf	Stefan Fritsch
  282          sfrost	Stephen Frost
  283        siretart	Reinhard Tartler
  284             sjr	Simon Richter
  285           skitt	Stephen Kitt
  286           slomo	Sebastian Dröge
  287            smcv	Simon McVittie
  288           smurf	Matthias Urlichs
  289         sophieb	Sophie Brun
  290       spwhitton	Sean Whitton
  291       sramacher	Sebastian Ramacher
  292            srud	Sruthi Chandran
  293          ssgelm	Stephen Gelman
  294        stappers	Geert Stappers
  295        stefanor	Stefano Rivera
  296            sten	Nicholas D Steeves
  297  stephanlachnit	Stephan Lachnit
  298       sthibault	Samuel Thibault
  299          stuart	Stuart Prescott
  300            sune	Sune Vuorela
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  301       sunweaver	Mike Gabriel
  302           sur5r	Jakob Haufe
  303       sylvestre	Sylvestre Ledru
  304           szlin	SZ Lin
  305            tach	Taku Yasui
  306          taffit	David Prévot
  307          takaki	Takaki Taniguchi
  308           taowa	Taowa
  309          tassia	Tássia Camões Araújo
  310             tbm	Martin Michlmayr
  311        terceiro	Antonio Terceiro
  312          tfheen	Tollef Fog Heen
  313              tg	Thorsten Glaser
  314             thk	Thomas Koch
  315           tiago	Tiago Bortoletto Vaz
  316          tianon	Tianon Gravi
  317           tille	Andreas Tille
  318            tina	Martina Ferrari
  319            tiwe	Timo Weingärtner
  320        tjhukkan	Teemu Hukkanen
  321        tmancill	Tony Mancill
  322           toddy	Tobias Quathamer
  323            toni	Toni Mueller
  324         treinen	Ralf Treinen
  325           troyh	Troy Heber
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  326        tvainika	Tommi Vainikainen
  327        tvincent	Thomas Vincent
  328         tzafrir	Tzafrir Cohen
  329            ucko	Aaron M. Ucko
  330        ukleinek	Uwe Kleine-König
  331        umlaeute	IOhannes m zmölnig
  332           urbec	Judit Foglszinger
  333         utkarsh	Utkarsh Gupta
  334         vagrant	Vagrant Cascadian
  335        valhalla	Elena Grandi
  336          vcheng	Vincent Cheng
  337           viiru	Arto Jantunen
  338          vilmar	Francisco Vilmar Cardoso Ruviaro
  339          vorlon	Steve Langasek
  340          vvidic	Valentin Vidic
  341          wagner	Hanno Wagner
  342           waldi	Bastian Blank
  343        weinholt	Göran Weinholt
  344          wijnen	Bas Wijnen
  345          wookey	Wookey
  346          wouter	Wouter Verhelst
  347            wrar	Andrey Rahmatullin
  348          xluthi	Xavier Lüthi
  349            yadd	Xavier Guimard
  350            zack	Stefano Zacchiroli
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  351            zeha	Christian Hofstaedtler
  352            zhsj	Shengjing Zhu
  353            zigo	Thomas Goirand
  354       zugschlus	Marc Haber
