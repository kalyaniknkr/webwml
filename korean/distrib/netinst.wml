#use wml::debian::template title="인터넷을 통한 데비안 설치" BARETITLE=true
#use wml::debian::translation-check translation="65bd97b2cb00e0ef96c0166b86b98831e72e6592"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<p>데비안을 설치하는 이 방법은 설치하는 <em>동안</em> 인터넷 연결이 필요합니다.
요구 사항에 맞춰 처리되기 때문에 다른 방법과 비교해 더 적은 데이터를 다운로드하게 됩니다.
이더넷과 무선 연결이 지원됩니다. 내부 ISDN 카드는 불행히도 지원되지 <em>않습니다</em>.
</p>
<p>네트워크에서 설치하는 세 가지 옵션이 있습니다:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">직은 CD 또는 USB 메모리</toc-add-entry>

<p>아래는 이미지 파일입니다.
아래에서 여러분 프로세서의 아키텍처를 고르세요
</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>자세한 내용은 다음을 보세요: <a href="../CD/netinst/">최소 CD에서 네트워크 설치</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">작은 CD, 유연한 USB 메모리 등</toc-add-entry>

<p>USB 메모리 및 비슷한 장치에 적합한 작은 크기의 이미지 파일을 다운로드해,
미디어에 쓰고, 거기서 부팅하여 설치를 시작할 수 있습니다.</p>

<p>아키텍처에 따라 여러가지 작은 이미지에서 설치하는 것에 대한 지원은 다양함이
있습니다.</p>

<p>더 자세한 정보는
<a href="$(HOME)/releases/stable/installmanual">여러분의 아키텍처 설치 매뉴얼</a>,
특히 <q>시스템 설치 미디어 구하기</q> 장을 참고하세요.
</p>

<p>다음은 사용 가능 이미지 파일의 링크입니다.
(정보는 MANIFEST 파일을 보세요):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">네트워크 부팅</toc-add-entry>

<p>TFTP 및 DHCP (또는 BOOTP, 또는 RARP) 서버를 설치하여 로컬 네트워크의
컴퓨터에 설치 미디어를 제공하도록 만듭니다. 컴퓨터의 BIOS가 네트워크 부팅을
지원하면 (PXE 및 TFTP 사용), 이러한 방법으로 네트워크에서 데비안을 부팅하고,
네트워크에서 데비안의 나머지를 설치합니다.</p>

<p>모든 컴퓨터가 네트워크 부팅을 지원하지는 않습니다.
추가 작업이 필요하므로, 초보자에게는 이 방법의 데비안 설치는 권하지 않습니다.
</p>

<p>자세한 것은
<a href="$(HOME)/releases/stable/installmanual">여러분의 아키텍처 설치 매뉴얼</a>, 특히
<q>TFTP 네트워크 부팅 준비 파일</q>을 참고하세요.</p>
<p>여기 그 이미지 파일이 있습니다(MANIFEST 파일에 정보가 있습니다):
</p>

<stable-netboot-images />
</div>
</div>
