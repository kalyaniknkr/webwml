#use wml::debian::template title="Vår filosofi: varför vi gör det och hur vi gör det" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Vårt uppdrag: Att skapa ett fritt operativsystem</a></li>
    <li><a href="#how">Våra värderingar: Hur Debiangemenskapen fungerar</a></li>
  </ul>
</div>

<h2><a id="freedom">Vårt uppdrag: Att skapa ett fritt operativsystem</a></h2>

<p>Debianprojektet är en samling av individer som har ett gemensamt mål:
Vi vill skapa ett fritt operativsystem, fritt tillgängligt för alla.
Nu när vi använder ordet "fritt" (engelska free), så talar vi inte om
fritt som i gratis, utan refererar istället till <em>mjukvarufrihet</em>.</p>


<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span>
<a href="free">Vår definition av fri mjukvara</a></button> <button type="button">
<span class="fas fa-book-open fa-2x"></span>
<a href="https://www.gnu.org/philosophy/free-sw">Läs Free Software Foundation's redogörelse</a></button></p>

<p>Kanske du undrar varför så många väljer att spendera så mycket tid på
att skriva mjukvara, noggrant paketera och underhålla denna, bara för att ge
bort den utan att ta betalt för den? Det finns många orsaker, och här följer
några av dem:</p>

<ul>
  <li>Några tycker helt enkelt om att hjälpa andra, och att bidra till ett
      fri mjukvaruprojekt är ett utmärkt sätt att åstadkomma detta.</li>
  <li>Många utvecklare skriver program för att lära sig mer om datorer, olika
      arkitekturer, och programmeringsspråk.</li>
  <li>Några utvecklare bidrar för att säga "tack" för all fantastisk fri
      mjukvara som dom har fått från andra.</li>
  <li>Många inom den akademiska världen skapar fri mjukvara för att dela med sig
      av resultaten inom deras forskning.</li>
  <li>Företag hjälper också till med att underhålla fri mjukvara: för att
      påverka hur en mjukvara utvecklas eller för att implementera ny
      funktionalitet snabbt.</li>
  <li>Självklart så deltar dom flesta Debianutvecklarna för att dom tycker att
      det är kul!</li>
</ul>

<p>Även om vi tror på fri mjukvara, respekterar vi att folk ibland
måste installera icke-fri mjukvara på sina maskiner - oavsett om dom
vill det eller inte. Vi har valt att stödja dessa användare, när det
är möjligt. Det finns ett växande antal paket som installerar icke-fri
mjukvara på Debiansystem.</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Vi är engagerade i fri
mjukvara och vi har formaliserat detta åtagande i ett dokument:
vårt <a href="$(HOME)/social_contract">Sociala kontrakt</a></p>
</aside>

<h2><a id="how">Våra värderingar: Hur Debiangemenskapen fungerar</a></h2>

<p>Debianprojektet har mer än tusen aktiva <a
href="people">utvecklare och bidragslämnare</a> över <a
href="$(DEVEL)/developers.loc">hela världen</a>. Ett projekt av denna
storlek behöver en noggrant <a href="organization">organiserad struktur</a>.
Så, om du undrar hur Debianprojektet fungerar och om Debiangemenskapen har
regler och riktlinjer, ta en titt på följande uttalanden:</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">Debians konstitution</a>: <br>
            Detta dokument beskriver organisationsstrukturen och förklarar hur
            Debianprojektet fattar sina formella beslut.</li>
        <li><a href="../social_contract">Det sociala kontraktet och riktlinjerna för fri mjukvara</a>: <br>
            Debians sociala kontrakt och Debians riktlinjer för fri mjukvara (DFSG) som en del av detta kontrakt, beskriver vårt engagemang för fri mjukvara och fri mjukvarugemenskapen.</li>
        <li><a href="diversity">Mångfaldserkännandet:</a> <br>
            Debianprojektet välkomnar och uppmuntrar alla att delta, oberoende av hur du identifierar dig eller hur andra uppfattar dig.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">Uppförandekod:</a> <br>
            Vi har antagit en uppförandekod för deltagare i våra sändlistor, IRC-kanaler, osv.</li>
        <li><a href="../doc/developers-reference/">Utvecklarreferensen:</a> <br>
            Detta dokument tillhandahåller en överblick över rekommenderade procedurer och tillgängliga resurser för Debianutvecklare och paketansvariga.</li>
        <li><a href="../doc/debian-policy/">Debians policy:</a> <br>
            En manual som beskriver policykraven för Debiandistributionen, t.ex Debianarkivets struktur och innehåll, tekniska krav som alla paket måste följa för att inkluderas, osv.</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> På insidan av Debian: <a href="$(DEVEL)/">Utvecklarhörnan</a></button></p>
