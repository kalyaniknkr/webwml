#use wml::debian::translation-check translation="67e7c0f3759a5673d880c47304c13ca117ee5d0c"
<define-tag pagetitle>Uppdaterad Debian 11; 11.10 utgiven</define-tag>
<define-tag release_date>2024-06-29</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin tionde uppdatering till dess
gamla stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den gamla stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction allegro5 "Fix buffer overflow issues [CVE-2021-36489]">
<correction amavisd-new "Handle multiple boundary parameters that contain conflicting values [CVE-2024-28054]">
<correction bart "Fix build test failures by relaxing a floating-point comparison">
<correction bart-cuda "Fix build test failures by relaxing a floating-point comparison">
<correction base-files "Update for the point release">
<correction cloud-init-22.4.2 "Introduce later-versioned replacement for cloud-init package">
<correction cpu "Provide exactly one definition of globalLdap in ldap plugin">
<correction curl "Fix memory leak when HTTP/2 server push is aborted [CVE-2024-2398]">
<correction debian-installer "Increase Linux kernel ABI to 5.10.0-30; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debsig-verify "Rebuild for outdated Built-Using">
<correction deets "Rebuild for outdated Built-Using">
<correction distro-info-data "Declare intentions for bullseye/bookworm; fix past data; add Ubuntu 24.10">
<correction django-mailman3 "Scrub messages before archiving">
<correction dns-root-data "Update root hints; update expired security information">
<correction emacs "Protect against unsafe remote resources [CVE-2024-30203 CVE-2024-30204 CVE-2024-30205]; fix memory leak in patch for CVE-2022-48337">
<correction galera-4 "New upstream bugfix release; update upstream release signing key; prevent date-related test failures">
<correction gdk-pixbuf "ANI: Reject files with multiple anih chunks [CVE-2022-48622]; ANI: Reject files with multiple INAM or IART chunks; ANI: Validate anih chunk size">
<correction glib2.0 "Fix a (rare) memory leak">
<correction gnutls28 "Fix assertion failure verifying a certificate chain with a cycle of cross signatures [CVE-2024-0567]; fix timing side-channel attack inside RSA-PSK key exchange [CVE-2024-0553]">
<correction gross "Fix stack-based buffer overflow [CVE-2023-52159]">
<correction hovercraft "Depend on python3-setuptools">
<correction imlib2 "Fix heap-buffer overflow vulnerability when using the tgaflip function in loader_tga.c [CVE-2024-25447 CVE-2024-25448 CVE-2024-25450]">
<correction intel-microcode "Fixes for INTEL-SA-INTEL-SA-00972 [CVE-2023-39368], INTEL-SA-INTEL-SA-00982 [CVE-2023-38575], INTEL-SA-INTEL-SA-00898 [CVE-2023-28746], INTEL-SA-INTEL-SA-00960 [CVE-2023-22655] and INTEL-SA-INTEL-SA-01045 [CVE-2023-43490]; mitigate for INTEL-SA-01051 [CVE-2023-45733], INTEL-SA-01052 [CVE-2023-46103], INTEL-SA-01036 [CVE-2023-45745,  CVE-2023-47855] and unspecified functional issues on various Intel processors">
<correction jose "Fix potential denial-of-service issue [CVE-2023-50967]">
<correction json-smart "Fix excessive recursion leading to stack overflow [CVE-2023-1370]; fix denial of service via crafted request [CVE-2021-31684]">
<correction lacme "Fix post-issuance validation logic">
<correction libapache2-mod-auth-openidc "Fix missing input validation leading to DoS [CVE-2024-24814]">
<correction libjwt "Fix a timing side channel via strcmp() [CVE-2024-25189]">
<correction libkf5ksieve "Prevent leaking passwords into server-side logs">
<correction libmicrohttpd "Fix out of bounds read with crafted POST requests [CVE-2023-27371]">
<correction libssh2 "Fix out of bounds memory check in _libssh2_packet_add [CVE-2020-22218]">
<correction links2 "Rebuild for outdated Built-Using">
<correction nano "Fix malicious symlink issue [CVE-2024-5742]">
<correction ngircd "Respect <q>SSLConnect</q> option for incoming connections; server certificate validation on server links (S2S-TLS); METADATA: Fix unsetting <q>cloakhost</q>">
<correction nvidia-graphics-drivers "End support for Tesla 450 drivers; build libnvidia-fbc1 for arm64; upstream security fixes [CVE-2022-42265 CVE-2024-0074 CVE-2024-0078]; new upstream stable release; security fixes [CVE-2024-0090 CVE-2024-0092]; fix build on ppc64el">
<correction nvidia-graphics-drivers-tesla-450 "Convert to transitional packages">
<correction nvidia-graphics-drivers-tesla-470 "New upstream LTS release [CVE-2024-0074 CVE-2024-0078 CVE-2022-42265 CVE-2024-0090 CVE-2024-0092]; fix build on ppc64el">
<correction nvidia-settings "New upstream bugfix release; build for ppc64el">
<correction org-mode "Protect against unsafe remote resources [CVE-2024-30203 CVE-2024-30204 CVE-2024-30205]">
<correction php-composer-xdebug-handler "Force system dependency loading">
<correction php-doctrine-annotations "Force system dependency loading">
<correction php-phpseclib "Force system dependency loading; guard isPrime() and randomPrime() for BigInteger [CVE-2024-27354]; limit OID length in ASN1 [CVE-2024-27355]; fix BigInteger getLength()">
<correction php-proxy-manager "Force system dependency loading">
<correction php-symfony-contracts "Force system dependency loading">
<correction php-zend-code "Force system dependency loading">
<correction phpseclib "Force system dependency loading; guard isPrime() and randomPrime() for BigInteger [CVE-2024-27354]; limit OID length in ASN1 [CVE-2024-27355]; fix BigInteger getLength()">
<correction postfix "Upstream bugfix release">
<correction postgresql-13 "New upstream stable release">
<correction pypdf2 "Fix quadratic runtime with malformed PDF missing xref marker [CVE-2023-36810]; fix infinite loop with crafted input [CVE-2022-24859]">
<correction python-aiosmtpd "Fix SMTP smuggling issue [CVE-2024-27305]; fix STARTTLS unencrypted command injection issue [CVE-2024-34083]">
<correction python-dnslib "Validate transaction ID in client.py">
<correction python-idna "Fix denial of service issue [CVE-2024-3651]">
<correction python-stdnum "Fix FTBFS when test date is not far enough in the future">
<correction qtbase-opensource-src "Security fixes [CVE-2022-25255 CVE-2023-24607 CVE-2023-32762 CVE-2023-32763 CVE-2023-33285 CVE-2023-34410 CVE-2023-37369 CVE-2023-38197 CVE-2023-51714 CVE-2024-25580]">
<correction reportbug "Fix suite name to codename mappings to reflect the bookworm release">
<correction rust-cbindgen-web "New source package to support builds of newer Firefox ESR versions">
<correction rustc-web "Support firefox-esr and thunderbird in bullseye for LTS">
<correction sendmail "Fix SMTP smuggling issue [CVE-2023-51765]; add forgotten configuration for rejecting NUL by default">
<correction symfony "Force system dependency loading; DateTypeTest: ensure submitted year is accepted choice">
<correction systemd "Meson: drop arch filtering in syscall list; unset TZ before timezone-sensitive unit tests are run">
<correction wpa "Fix authentication bypass issue [CVE-2023-52160]">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den gamla stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2022 5146 puma>
<dsa 2023 5360 emacs>
<dsa 2023 5575 webkit2gtk>
<dsa 2023 5580 webkit2gtk>
<dsa 2024 5596 asterisk>
<dsa 2024 5616 ruby-sanitize>
<dsa 2024 5618 webkit2gtk>
<dsa 2024 5619 libgit2>
<dsa 2024 5620 unbound>
<dsa 2024 5621 bind9>
<dsa 2024 5622 postgresql-13>
<dsa 2024 5624 edk2>
<dsa 2024 5625 engrampa>
<dsa 2024 5627 firefox-esr>
<dsa 2024 5628 imagemagick>
<dsa 2024 5630 thunderbird>
<dsa 2024 5631 iwd>
<dsa 2024 5632 composer>
<dsa 2024 5635 yard>
<dsa 2024 5637 squid>
<dsa 2024 5638 libuv1>
<dsa 2024 5640 openvswitch>
<dsa 2024 5641 fontforge>
<dsa 2024 5643 firefox-esr>
<dsa 2024 5644 thunderbird>
<dsa 2024 5645 firefox-esr>
<dsa 2024 5646 cacti>
<dsa 2024 5647 samba>
<dsa 2024 5650 util-linux>
<dsa 2024 5651 mediawiki>
<dsa 2024 5652 py7zr>
<dsa 2024 5653 gtkwave>
<dsa 2024 5657 xorg-server>
<dsa 2024 5659 trafficserver>
<dsa 2024 5660 php7.4>
<dsa 2024 5662 apache2>
<dsa 2024 5663 firefox-esr>
<dsa 2024 5664 jetty9>
<dsa 2024 5666 flatpak>
<dsa 2024 5667 tomcat9>
<dsa 2024 5669 guix>
<dsa 2024 5670 thunderbird>
<dsa 2024 5671 openjdk-11>
<dsa 2024 5672 openjdk-17>
<dsa 2024 5673 glibc>
<dsa 2024 5678 glibc>
<dsa 2024 5679 less>
<dsa 2024 5681 linux-signed-amd64>
<dsa 2024 5681 linux-signed-arm64>
<dsa 2024 5681 linux-signed-i386>
<dsa 2024 5681 linux>
<dsa 2024 5682 glib2.0>
<dsa 2024 5682 gnome-shell>
<dsa 2024 5684 webkit2gtk>
<dsa 2024 5685 wordpress>
<dsa 2024 5686 dav1d>
<dsa 2024 5688 atril>
<dsa 2024 5690 libreoffice>
<dsa 2024 5691 firefox-esr>
<dsa 2024 5692 ghostscript>
<dsa 2024 5693 thunderbird>
<dsa 2024 5695 webkit2gtk>
<dsa 2024 5698 ruby-rack>
<dsa 2024 5700 python-pymysql>
<dsa 2024 5702 gst-plugins-base1.0>
<dsa 2024 5703 linux-signed-amd64>
<dsa 2024 5703 linux-signed-arm64>
<dsa 2024 5703 linux-signed-i386>
<dsa 2024 5703 linux>
<dsa 2024 5704 pillow>
<dsa 2024 5707 vlc>
<dsa 2024 5709 firefox-esr>
<dsa 2024 5711 thunderbird>
<dsa 2024 5713 libndp>
<dsa 2024 5714 roundcube>
<dsa 2024 5715 composer>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction phppgadmin "Security issues">
<correction pytest-salt-factories "Only needed for to-be-removed salt">
<correction pytest-testinfra "Only needed for to-be-removed salt">
<correction salt "Unsupportable, unmaintained">
<correction snort "Security concerns, unmaintained">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
gamla stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Föreslagna uppdateringar till den gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Information om den gamla stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


