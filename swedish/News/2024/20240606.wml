# Status: published
# $Rev$
#use wml::debian::translation-check translation="27cb73692f0206ae5f0a3f04e7090405b4df902a"

<define-tag pagetitle>Uttalande om Daniel Pocock</define-tag>
<define-tag release_date>2024-06-06</define-tag>
#use wml::debian::news

<p>Efter den otillböriga registreringen av Debians varumärke i Schweiz
av en före detta Debianutvecklares företag - som har genomfört en stark
smutskastningskampanj mot Debian sedan 2018 -, har Debianprojektet vidtagit
kraftfulla åtgärder för att säkra sina varumärken och intressen över hela
världen.</p>

<p>I november 2023, efter rättsliga åtgärder som väckts av Debian, <a
href="https://www.debian.org/News/2024/judgement.pdf">beslutade</a>
Tribunal Cantonal i kantonen Vaud att registreringen av Debian-varumärket
i Schweiz av Open Source Developer Freedoms SA i likvidation (OSDF) - tidigare
Software Freedom Institute SA - var en förskingring av Debians varumärke,
eftersom det senare är välkänt och etablerat inom IT-sektorn över hela världen.
Ingen överklagan överklagades av domen som därmed bär laga kraft.</p>

<p>I sin dom konstaterade domstolen att:</p>

<p><em>"Daniel Pocock är den enda administratören för den tilltalade [OSDF]."</em></p>

<p><em>"Genom att gå tillväga på detta sätt, har svaranden, som är medveten
om existensen av det omtvistade varumärket och dess rykte, och som visste att
käranden var dess ägare, eftersom dess enda administratör var en
Debianutvecklare och en före detta medlem av gemenskapen med samma namn,
tillskansat sig ovan nämnda varumärke och skapat förvirring hos allmänheten."</em></p>

<p>Tribunalen beordrade att den schweiziska varumärkesregistreringen skulle
överföras till Debianprojektets betrodda organisation, Software in the Public
Interest (SPI) Inc., och kostnaderna för åtgärden betalades av svaranden. OSDF
beordrades också att publicera domen på sin hemsida. Innan domstolens
slutgiltiga avgörande kunde meddelas avbröt OSDF abrupt registreringen och
trädde senare i likvidation utan att meddela SPI och utan att betala
kostnaderna för talan. Hittills har Daniel Pocock misslyckats med att följa
något av domstolens beslut och Debian har tvingats inleda återkravsåtgärder för
sina kostnader.</p>

<p>Under tiden, mellan 2020 och februari 2024, blev Debian medvetna om
åtminstone 14 domänregistreringar och assoicierade webbplatser som strider mot
vår varumärkespolicy. Alla domäner registrerades och kontrolleras av
Daniel Pocock. I maj 2024 beordrade World Intellectual Property Organization
(WIPO) de relevanta domänregistren att
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2024-0770">\
överföra</a> alla 14 registreringar till SPI Inc. i förtroende för Debian, och
<a href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">\
noterade</a>:</p>

<p><em>"[...] Svaranden registrerade och använde de omtvistade domännamnen i
ond tro, vilket skapade en sannolikhet för förväxling beträffande källan eller
anknytningen till den klagandes varumärke, för såväl kommersiella som kritiska
ändamål och med full kännedom om den klagandes varumärke och
varumärkespolicy."</em></p>

<p>Debianprojektet kommer att fortsätta att vidta alla nödvändiga åtgärder för
att skydda och försvara sina varumärken och andra intressen. Vår
varumärkespolicy har stått emot granskning i flera jurisdiktioner.</p>

<p>Vi tar därför tillfället i akt att tacka medlemmarna i vår gemenskap som
investerat betydande ansträngningar för att dokumentera Debians användning och
distribution runt om i världen som förberedelser för att utmana den schweiziska
varumärkesregistreringen, trots den tunga – och orättvisa -
smutskastningskampanj många av våra volontärer fick möta under de senaste
åren.</p>

<p>Vi är fortfarande engagerade i att eftersträva lämpliga rättsliga medel för
att skydda vår gemenskap och andra från ytterligare trakasserier..</p>

<h2>Ytterligare information</h2>

<p>Domen från Tribunal Cantonal i kantonen Vaud, daterad den 27 november 2023,
återfinns på <url "https://www.debian.org/News/2024/judgement.pdf"> och
och WIPO:s fullständiga beslutstext, daterad den 3 maj , 2024, finns
tillgänglig på <a
href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">\
https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770</a></p>



<h2>Om Debian</h2>

<p>Debianprojektet är en sammanslutning av fri mjukvaruutvecklare som
frivilligt lägger sin tid och ansträngning för att producera det helt fria
operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debian webbplats på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på engelska)
till &lt;press@debian.org&gt;.</p>

