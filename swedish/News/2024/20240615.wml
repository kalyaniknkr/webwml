# Status: published
# $Rev$
#use wml::debian::translation-check translation="ca2375b4ef2eebfd1c7fb93df901295ca6ea6586"
<define-tag pagetitle>Debian 10 långtidsstöd når slutet på sin livslängd</define-tag>
<define-tag release_date>2024-06-15</define-tag>
#use wml::debian::news

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
Debians grupp för långtidsstöd tillkännager härmed att stöd för Debian 10
<q>buster</q> kommer nå slutet på sin livslängd 30 juni, 2024, nästan fem år
efter dess ursprungliga utgåva den 6 juki, 2019.
</p>

<p>
Med start i juli kommer Debian inte längre att tillhandahålla
säkerhetsuppdateringar för Debian 10. En delmängd av paket i <q>buster</q>
kommer att stödjas av externa grupper. Detaljerad information kan hittas
på sidan för <a href="https://wiki.debian.org/LTS/Extended">Utökad LTS</a>.
</p>

<p>
Debians LTS-grupp kommer att preparera övergången till Debian 11
<q>bullseye</q>, den nuvarande gamla stabila utgåvan. Tack vare den samlade
insatsen av olika grupper inklusive säkerhetsgruppen, utgåvegruppen och
LTS-gruppen kommer livslängden för Debian 11 också att omfatta fem år.
För att göra livscykeln för Debianutgåvor lättare att följa har de relaterade
Debiangrupperna kommer överens om följande schema: tre år av reguljärt stöd
plus två års långtidsstöd. LTS-gruppen kommer att ta över stödet från
säkerhetsgruppen och utgåvegruppen 14 augusti 2024, tre år efter den
ursprungliga utgåvan 14 augusti, 2021. Den slutgiltiga punktuppdateringen av
<q>bullseye</q> kommer att publiceras strax efter den slutliga
säkerhetsbulletinen (DSA - Debian security advisory) har utfärdats.
</p>

<p>
Debian 11 kommer att få långtidsstöd fram till 31 augusti 2026. Arkitekturerna
med stöd förblir amd64, i386, arm64 och armhf.
</p>

<p>
För ytterligare information om användning av <q>bullseye</q> LTS och att
uppgradera från <q>buster</q> LTS, vänligen se
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
</p>

<p>
Debian och dess LTS-grupp skulle vilja tacka alla bidragande användare,
utvecklare, sponsorer och andra Debian-grupper som gör det möjligt att förlänga
stödet av tidigare stabila utgåvor.
</p>

<p>
Om du är beroende av Debian LTS, vänligen överväg att 
<a href="https://wiki.debian.org/LTS/Development">gå med i gruppen</a>,
tillhandahåll patchar, testning eller 
<a href="https://wiki.debian.org/LTS/Funding">bidra till finansiering av insatsen</a>.
</p>


<h2>Om Debian</h2>

<p>
	Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
	i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
	vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
	Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
	och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
	en stor mängd datortyper, kallar sig Debian det <q>universella 
	operativsystemet</q>.
</p>

<h2>Kontaktinformation</h2>

<p>
	För ytterligare information, vänligen besök Debians webbplats på
	<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på 
	engelska) till &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>Prenumerera / Avbryt prenumeration</h2>
<p><a href="https://lists.debian.org/debian-announce/">Prenumerera på eller 
avbryt prenumeration från</a> sändlistan för Debian-tillkännagivanden
(debian-announce)</p>
