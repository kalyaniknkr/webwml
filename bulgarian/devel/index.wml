#use wml::debian::template title="Разработване на Дебиан" MAINPAGE="true"
#use wml::debian::translation-check translation="9a3deb4236c92e083ea1e494362dc1ff242f6a8c"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Информацията на тази страница, макар и публична, е от интерес главно за сътрудниците на Дебиан.</p>
</aside>

<ul class="toc">
<li><a href="#basic">Обща информация</a></li>
<li><a href="#packaging">Изготвяне на пакети</a></li>
<li><a href="#workinprogress">Текуща работа</a></li>
<li><a href="#projects">Проекти</a></li>
<li><a href="#miscellaneous">Разни</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Обща информация</a></h2>
      <p>Списък на сътрудниците, начини за включване в проекта, връзки към базата данни с информация за сътрудниците, конституцията, процеса на вземане на решения чрез гласуване, издания и архитектури.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Структура на проекта Дебиан</a></dt>
        <dd>Над хиляда доброволци участват в проекта Дебиан. На тази страница ще намерите описание на отделните звена, списък с екипите и техните членове, както и адресите за връзка.</dd>
        <dt><a href="$(HOME)/intro/people">Хората</a></dt>
        <dd>Членовете на Дебиан са <a href="https://wiki.debian.org/DebianDeveloper">разработчици</a> (пълноправни членове на проекта Дебиан) и <a href="https://wiki.debian.org/DebianMaintainer">помощници</a>. Ето списъци на <a href="https://nm.debian.org/public/people/dd_all/">разработчиците</a> и <a href="https://nm.debian.org/public/people/dm_all/">помощниците</a>. <a href="developers.loc">Карта</a> на местоположението на разработчиците по света.</dd>
        <dt><a href="join/">Включване в Дебиан</a></dt>
        <dd>Искате ли да сте част от проекта? Винаги се оглеждаме за нови сътрудници и ентусиасти на тема свободен софтуер с технически или други умения. Посетете страницата за повече информация.</dd>
        <dt><a href="https://db.debian.org/">База данни на членовете</a></dt>
        <dd>Основната информация в базата данни е достъпна публично, а по-личната информация — само за членове. Базата данни съдържа разнообразна информация като списък на <a href="https://db.debian.org/machines.cgi">сървърите на проекта</a> и ключове за GnuPG за всички членове.
        За извличане на ключа на даден сътрудник изберете връзката „PGP/GPG отпечатък“.
        <a href="https://db.debian.org/password.html">формуляр за смяна на паролата</a> и <a href="https://db.debian.org/forward.html">инструкция за настройване на пренасочването на писмата</a> до пощенската кутия в Дебиан. <p>Преди да използвате някоя от машините на Дебиан се запознайте с <a href="dmup">Политиката за използване на машините на Дебиан</a>.</dd>
        <dt><a href="constitution">Конституцията</a></dt>
        <dd>Документът описва структурите и начините за взимане на решения в Дебиан.
        </dd>
        <dt><a href="$(HOME)/vote/">Информация за гласуване</a></dt>
        <dd>Как избираме лидер, лого и изобщо как гласуваме.</dd>
        <dt><a href="$(HOME)/releases/">Издания</a></dt>
        <dd>Списък с активни издания (<a href="$(HOME)/releases/stable/">стабилно</a>, <a href="$(HOME)/releases/testing/">тестово</a> и <a href="$(HOME)/releases/unstable/">нестабилно</a>), както и списък на старите издания и техните кодови имена.</dd>
        <dt><a href="$(HOME)/ports/">Архитектури</a></dt>
        <dd>Дебиан може да се използва на различни компютърни архитектури. Страницата съдържа информация информация за всички тях - някои базирани на ядро Линукс, а други на ядрата на FreeBSD, NetBSD или Hurd.</dd>

     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Изготвяне на пакети</a></h2>
      <p>Връзки към политиките за пакетиране и други свързани документи, процедури и ресурси за разработчици, включително ръководството за нови отговорници.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Политики на Дебиан</a></dt>
        <dd>Описва изискванията на дистрибуцията Дебиан. Това включва структура и съдържанието на архива, съображения относно дизайна на операционната система, както и технически изисквания към пакетите.

        <p>С две думи — <strong>задължително четиво</strong>.</p>
        </dd>
      </dl>

      <p>Няколко документа са свързани с Политиките и заслужават специално
      внимание:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Стандартна йерархия на файловата система</a> (FHS)
        <br />FHS указва в кои директории (и файлове) трябва да се появяват
            отделните части на операционната система. Изисква се съответсвтие
            с версия 3.0 (вж. <a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">глава
        9</a> от политиките.</li>
        <li>Списък на <a href="$(DOC)/packaging-manuals/build-essential">пакетите, задължително достъпни при компилиране</a>
        <br />Това са пакети, които се очаква да бъдат достъпни при
            компилиране на софтуер. Тези пакети няма нужда да бъдат описвани в
            полето <code>Build-Depends</code> на пакетите при описване на
            <a
        href="https://www.debian.org/doc/debian-policy/ch-relationships.html">зависимостите</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Система за менюта</a>
        <br />Структура на менютата в Дебиан. Вижте и
        <a href="$(DOC)/packaging-manuals/menu.html/">документацията на
        системата за менюта</a> за допълнителна информация.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Политики за Emacs</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Политики за Java</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Политики за Perl</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Политики за Python</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Спецификация на Debconf</a>.</li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Политики за приложения, работещи с бази данни</a> (проект)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Политики за Tcl и Tk</a> (проект)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Политики за Ада</a></li>
      </ul>

      <p>Прегледайте и списъка с <a
      href="https://bugs.debian.org/debian-policy">предложения за промени в
      Политиките</a>.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Справочник на разработчика</a></dt>

        <dd>
        Обзор върху препоръчваните практики и ресурсите, достъпни за членове на
        Дебиан. Още едно <strong>задължително четиво</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">Ръководство за отговорници</a></dt>

        <dd>
        Достъпно описание на техниките за пакетиране с включено много
        примери. Препоръчвано четиво за всички, които искат да помогнат в
        поддръжката на пакети или желаят да се присъединят към проекта.
        </dd>
      </dl>


    </div>

  </div>

</div>


<h2><a id="workinprogress">В процес на разработка: Връзки за активни сътрудници на Дебиан</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Тестова дистрибуция</a></dt>
  <dd>
    Създава се от съдържането на нестабилната дистрибуция, при спазване на
    определени правила. Това е мястото, в което се подготвя следващото издание на
    Дебиан.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Проблеми, критични за изданието</a></dt>
  <dd>
    Това е списък на грешките, които могат да предизвикат премахване
    на съответния пакет от тестовата дистрибуция, а понякога могат да причинят и
    забавяне на новото издание. Всички доклади с ниво на критичност
    &lsquo;serious&rsquo; или по-високо се включват в списъка. Винаги
    поправяйте такива грешки в пакетите си възможно най-бързо.
  </dd>

  <dt><a href="$(HOME)/Bugs/">Системата за проследяване на проблеми</a></dt>
    <dd>
    Служи за докладване, обсъждане и поправяне на грешки. Системата се използва
    и от потребителите, и от сътрудниците на Дебиан.
    </dd>
        
  <dt>Преглед на пакетите от гледна точка на отговорника</dt>
    <dd>
      Страниците за <a
      href="https://qa.debian.org/developer.php">информация</a> и <a
      href="https://tracker.debian.org/">следене</a> на пакети предоставят
      обобщена информация, полезна за отговорниците. Сътрудниците, които искат
      да следят развитието на чужди пакети могат да се абонират (чрез електронна
      поща) към услуга, която ще им изпраща копия от всички съобщения до
      системата за проследяване на проблемите, както и известия при обновяване
      на пакета. За подробности вижте <a
      href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">наръчника на за използване на системата за проследяване на пакетите</a>.
    </dd>

    <dt><a href="wnpp/">Пакети, имащи нужда от помощ</a></dt>
      <dd>
      Списък на пакети, които имат нужда от нов отговорник, както и такива,
      които още не са част от Дебиан.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Входяща система</a></dt>
      <dd>
      Всички пакети се изпращат първо до входящата система на вътрешните
      сървъри, обслужващи архива. Приетите пакети са достъпни почти незабавно
      чрез уеб четец и четири пъти на ден се разпространяват до
      <a href="$(HOME)/mirror/">огледалните сървъри</a>.
      <br />
      <strong>Забележка</strong>: Поради същността на входящата система не
      препоръчваме създаването на огледални нейни копия.
      </dd>

    <dt><a href="https://lintian.debian.org/">Доклади от Линтиан</a></dt>
      <dd>
      <a href="https://packages.debian.org/unstable/devel/lintian">
      Линтиан</a> е програма, която проверява дали даден пакет спазва
      изискванията на политиките на Дебиан. Препоръчваме използването ѝ преди
      изпращане на пакети към архива на Дебиан.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Експериментална дистрибуция</a></dt>
      <dd>
      Дистрибуцията <em>experimental</em> се използва като временно решение
      за експерименти с нови и вероятно нестабилни софтуерни версии.
      Използвайте <a href="https://packages.debian.org/experimental/">пакети от
      <em>experimental</em></a> само ако вече сте в състояние да боравите
      с нестабилната дистрибуция <em>unstable</em>.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Помощ за Дебиан</a></dt>
      <dd>
      Уикито на Дебиан съдържа съвети и идеи за разработчици и други сътрудници.
      </dd>
</dl>

<h2><a id="projects">Проекти</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Уеб страници на Дебиан</a></li>
<li><a href="https://ftp-master.debian.org/">Архив на Дебиан</a></li>
<li><a href="$(DOC)/ddp">Проект за документиране на Дебиан</a></li>
<li><a href="https://qa.debian.org/">Качествен контрол</a></li>
<li><a href="$(HOME)/CD/">Компактдискове с Дебиан</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Подписване на ключове</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Координация на подписване на ключове</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Дебиан IPv6</a></li>
<li><a href="buildd/">Мрежа за автоматично компилиране</a> и <a href="https://buildd.debian.org/">журналите</a> от нея.</li>
<li><a href="$(HOME)/international/l10n/ddtp">Превод на описанията на пакетите</a></li>
<li><a href="debian-installer/">Инсталатор на Дебиан</a></li>
<li><a href="debian-live/">Дебиан на живо</a></li>
<li><a href="$(HOME)/women/">Жените в Дебиан</a></li>
<li><a href="$(HOME)/blends/">Дестилати на Дебиан</a></li>

</ul>


<h2><a id="miscellaneous">Разни</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://peertube.debian.social/home">Записи</a> от лекциите, изнесени по време на наши конференции в PeerTube или в
    <a href="https://debconf-video-team.pages.debian.net/videoplayer/">алтернативен</a> интерфейс</li>
<li><a href="passwordlessssh">Конфигуриране на </a> така, че да не пита за парола</li>
<li>Как да <a href="$(HOME)/MailingLists/HOWTO_start_list">заявим създаване на нов пощенски списък</a>.</li>
<li>Информация за <a href="$(HOME)/mirror/">огледалните сървъри</a> на Дебиан.</li>
<li><a href="https://qa.debian.org/data/bts/graphs/all.png">Графика на докладите за проблеми</a>.</li>
<li><a href="https://ftp-master.debian.org/new.html">Нови пакети, чакащи одобрение за включване в Дебиан</a> (опашка NEW).</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Нови пакети от последната седмица</a>.</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Пакети, премахнати от Дебиан</a>.</li>
</ul>
