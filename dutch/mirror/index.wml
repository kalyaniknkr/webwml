#use wml::debian::template title="Debian Spiegelservers" MAINPAGE="true"
#use wml::debian::translation-check translation="34422e94132789c2bcd0f6d41dc8680b49c7f3ab"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> De spiegelservers van Debian worden onderhouden door vrijwilligers. Als u schijfruimte en connectiviteit kunt doneren, overweeg dan om een spiegelserver te creëren om Debian toegankelijker te maken. Ga voor meer informatie naar <a href="ftpmirror">deze pagina</a>.</p>
</aside>

<p>Debian wordt wereldwijd verspreid (ook wel gespiegeld genoemd) op honderden servers die allemaal dezelfde inhoud aanbieden.
Op die manier kunnen we een betere toegang tot ons archief bieden.</p>

<p>Op onze spiegelservers vindt u de volgende inhoud:</p>

<dl>
<dt><strong>Debian pakketten</strong> (<code>/debian</code>)</dt>
  <dd>De Debian pakketpool: dit omvat de overgrote meerderheid van
      <code>.deb</code>-pakketten, installatiemateriaal en de broncode.
      <br>
      Bekijk de lijst van <a href="list">Debian spiegelservers</a> die het
      archief <code>debian/</code> bevatten.
  </dd>
<dt><strong>cd-images</strong> (<code>debian-cd/</code>)</dt>
  <dd>Het depot van cd-images: Jigdo-bestanden en ISO imagebestanden.
      <br>
      Bekijk de lijst van <a href="$(HOME)/CD/http-ftp/#mirrors">Debian spiegelservers</a>
      die het archief <code>debian-cd/</code> bevatten.
  </dd>
<dt><strong>Oude releases</strong> (<code>debian-archive/</code>)</dt>
  <dd>Het archief van oudere versies van Debian, die in het verleden werden uitgebracht.
      <br>
      Zie de pagina <a href="$(HOME)/distrib/archive">Distributie-archieven</a>
      voor nadere informatie.
  </dd>
</dl>

<p>Overzicht van de status van
<a href="https://mirror-master.debian.org/status/mirror-status.html">
Debian spiegelservers
</a></p>


