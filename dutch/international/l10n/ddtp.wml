#use wml::debian::template title="Het Debian project voor de vertaling van pakketbeschrijvingen"
#use wml::debian::toc
#use wml::debian::translation-check translation="0bc9cf0a839d304e8615fd04426caf3bd6af63de"

<p>
Het Debian project voor de vertaling van pakketbeschrijvingen, in het Engels
<a href="https://ddtp.debian.org">Debian Description Translation Project &mdash; DDTP</a>,
(dat geïmplementeerd werd door
<a href="mailto:Michael%20Bramer%20%3Cgrisu@debian.org%3E">Michael Bramer</a>)
heeft als doel om vertaalde pakketbeschrijvingen aan te leveren en de
infrastructuur om hun vertaling te ondersteunen. Hoewel het al een paar jaar
bestaat, werd het gedeactiveerd na een inbraak in een Debian computer en heeft
het momenteel in vergelijking met het verleden slechts een basisfunctionaliteit.
</p>

<p>
Het project ondersteunt:
</p>
<ul>
  <li>Ophalen van huidige (sid) en oudere pakketbeschrijvingen.</li>
  <li>Hergebruik van reeds vertaalde paragrafen uit de ene beschrijving in een
      andere pakketbeschrijving.</li>
  <li>Voorzien in <tt>Translation-*</tt>-bestanden voor spiegelservers en
      APT.</li>
</ul>

<p>
De secties non-free en non-free-firmware in het Debian archief zijn op dit
moment niet vertaalbaar omdat er licentieproblemen
<!-- Ik dacht dat zelfs voor niet-vrije projecten het materiaal dat betrekking
     heeft op de verpakking ervan door Debian (debian/) vrij was, maar het
     blijkt dat dit niet vereist is!? -->
zouden kunnen bestaan die bijvoorbeeld vertaling verbieden en waarop zorgvuldig
gecontroleerd zou moeten worden.
</p>

<p>
Het vertalen van meer dan 71000 pakketbeschrijvingen is een grote uitdaging.
Help ons alstublieft om ons doel te bereiken. Zie ook onze
<a href="#todo">to-dolijst</a> voor resterende problemen.
</p>

<toc-display/>

<toc-add-entry>Interfaces met het DDTP</toc-add-entry>

<p>
Aangezien alle interfaces de DDTP-backend gebruiken, moet u eerst nagaan of uw
taal al wordt ondersteund. Dit zou voor de meeste talen het geval moeten zijn.
Als dergelijke ondersteuning niet bestaat, schrijf dan naar
<email debian-i18n@lists.debian.org> of
<a href="https://salsa.debian.org/l10n-team/ddtp/-/issues">maak op Salsa een
item aan</a>
zodat uw taal geactiveerd kan worden.
</p>

<h3 id="DDTSS">De web-frontend</h3>
<p>
Er bestaat een mooie web-frontend met de naam
<a href="https://ddtp.debian.org/ddtss/index.cgi/xx">DDTSS</a>, die
geschreven werd door
<a href="mailto:Martijn%20van%20Oosterhout%20%3Ckleptog@gmail.com%3E">Martijn
van Oosterhout</a> en die de taken van vertalen en proeflezen probeert te
vereenvoudigen.
</p>

<h4>Overzicht</h4>
<p>
Hij ondersteunt het leveren van een bijdrage aan vertalingen en maakt bovendien
proeflezen mogelijk. Hij ondersteunt een aangepaste configuratie voor elk
taalteam, zodat elk team kan beslissen hoeveel revisies er nodig zijn voordat
de beschrijving wordt overgedragen naar de DDTP. Ook is het mogelijk om
gebruikersrechten aan te vragen, zodat slechts een beperkte groep mensen
bepaalde handelingen kan uitvoeren. U hoeft zich ook niet te bekommeren om de
codering, de DDTSS regelt dit voor u.
</p>

<p>
Huidige standaardeigenschappen:
</p>
<dl>
  <dt>aantal revisies:</dt><dd>3</dd>
  <dt>ondersteunde talen:</dt><dd>alle van de DDTP</dd>
  <dt>gebruikersrechten:</dt><dd>neen, hij staat open voor iedereen</dd>
</dl>

<p>
Het is mogelijk om een standaard woordenlijst voor een taal op te geven. Deze
wordt gebruikt om standaardvertalingen te leveren via tekstballonnetjes. Deze
lijst is beschikbaar via een link onderaan de taalpagina.
</p>

<h4>Werkwijze</h4>
<p>
De DDTSS biedt de volgende items voor alle talen:
</p>

<h5>Openstaande vertaling</h5>
<p>
Een lijst met openstaande vertalingen. Dit zijn beschrijvingen welke de
gebruiker vrij kan selecteren om te vertalen. Dit ziet eruit als:
</p>
<pre>
exim4 (prioriteit 52)
exim4-config (prioriteit 52)
ibrazilian (prioriteit 47, bezet)
postgresql-client (priority 47)
postgresql-contrib (prioriteit 47)
grap (prioriteit 45)
</pre>

<p>
Een taalteam moet proberen om pakketten met een hoge prioriteit (die wordt
berekend aan de hand van de categorie, b.v. essentieel, basis, ...) het eerst
te vertalen. De pakketten worden gesorteerd om dit te bereiken.
</p>

<p>
Een beschrijving die gemarkeerd is als bezet, is al gereserveerd door een
gebruiker en kan gedurende maximaal 15 minuten niet worden geselecteerd. Als er
gedurende deze tijd geen vertaling neergelegd werd, wordt de beschrijving
opnieuw gemarkeerd als vrij beschikbaar.
</p>

<p>
Een beschrijving moet volledig zijn vertaald voordat de frontend deze
accepteert. Zorg er dus voor dat u de volledige tekst kunt vertalen voordat u
begint. Selecteer <q>Submit</q> (Insturen) om uw vertaling toe te voegen,
<q>Abandon</q> (Ophouden) als u besluit de beschrijving niet te vertalen, of
<q>Unfetch</q> (Verwijderen) als de beschrijving niet vertaald moet worden
(bijvoorbeeld omdat ze eerst verbeterd moet worden). Het is ook
mogelijk dat u geluk hebt en dat er al een vertaling is voor een vorige versie
van het Engelse sjabloon, samen met een diff van de wijzigingen in de Engelse
vertaling die u in uw vertaling moet integreren. U kunt deze oude vertaling
vanaf het onderste deel van de pagina kopiëren en plakken en op gepaste wijze
bijwerken.
</p>

<p>
# Werkt nog niet als gewenst
Om lelijke fluctuaties van de tekstbreedte te voorkomen, wordt aangeraden om
niet handmatig regeleinden in te voeren, tenzij dat nodig is (zoals voor
lijstonderdelen). Regelafbreking gebeurt automatisch. Onthoud dat een gebruiker
tijdens het proeflezen kleine onderdelen kan toevoegen of verwijderen, wat
anders zou kunnen resulteren in een inconsistente regellengte. Dit ook
repareren maakt de diff met het verschil dat bij het nazicht is gemaakt,
moeilijk leesbaar.
</p>

<p>
Het is ook mogelijk om voorkeurspakketten op naam te selecteren. Dit is handig
om een reeks gelijksoortige pakketten, zoals manpages-de, manpages-es, na
elkaar te vertalen om eerdere vertalingen te kunnen kopiëren en plakken.
</p>

<p>
Zelfs reeds vertaalde pakketten kunnen op deze manier opnieuw worden opgehaald
om ze te verbeteren.
</p>

<h5>Openstaand nazicht</h5>
<p>
Een lijst van vertaalde beschrijvingen die nog moeten worden gereviseerd. Deze
lijst kan er als volgt uitzien:
</p>

<pre>
 1. aspell-es (nazicht nodig, kreeg er 1)
 2. bookmarks (eerste nazicht nodig)
 3. doc-linux-ja-html (eerste nazicht nodig)
 4. doc-linux-ja-text (eerste nazicht nodig)
 5. gnome-menus (eerste nazicht nodig)
 6. geany (nazicht nodig, kreeg er 2)
 7. initramfs-tools (eerste nazicht nodig)
 8. inn2 (eerste nazicht nodig)
</pre>

<p>
Er bestaan de volgende vlaggen:</p>
<dl>
    <dt lang="en">eerste nazicht nodig:</dt>
    <dd>De huidige versie van deze vertaling heeft nog geen enkel nazicht
        gekregen.</dd>

    <dt lang="en">nazicht nodig:</dt>
    <dd>De huidige versie van deze vertaling heeft nog verder nazicht nodig,
        maar heeft er minstens al één gekregen.</dd>

    <dt lang="en">nagezien:</dt>
    <dd>Deze beschrijving werd nagezien zonder dat de gebruiker iets
        veranderde. Andere gebruikers moeten dit proeflezen.</dd>

    <dt lang="en">eigenaar:</dt>
    <dd>Deze beschrijving werd vertaald of tijdens het proeflezen gewijzigd
        door de gebruiker. Andere gebruikers moeten dit proeflezen.</dd>
</dl>

<p>
Als er al een nazicht met correcties heeft plaatsgevonden, krijgt u wanneer u
het pakket selecteert een mooie kleurendiff met alle wijzigingen van het laatste
nazicht.
</p>

<h5>Recent vertaald</h5>
<p>
Een lijst met naar de DDTP overgedragen beschrijvingen. Er worden ten hoogste
twintig pakketten vermeld, samen met de datum van de overdracht.
</p>

<toc-add-entry name="rules">Algemene vertaalregels</toc-add-entry>
<p>
Het is belangrijk dat u tijdens het vertalen geen Engelse beschrijvingen
wijzigt. Als u er fouten in aantreft, moet u een bugrapport indienen tegen het
betreffende pakket. Zie <a href="$(HOME)/Bugs/Reporting">Een bug rapporteren in
Debian</a> voor details.
</p>

<p>
Vertaal de onvertaalde delen van elke bijlage die zijn gemarkeerd met
&lt;trans&gt;. Het is belangrijk dat u geen regels wijzigt die alleen een punt
in de eerste kolom bevatten. Dit zijn scheidingstekens tussen alinea's en
zullen niet zichtbaar zijn in APT-frontends.
</p>

<p>
Alinea's die al vertaald zijn, zijn ofwel hergebruikt uit andere beschrijvingen
of uit een oudere vertaling (en geven dus aan dat deze oorspronkelijke Engelse
alinea sindsdien niet meer veranderd is). Zodra u een dergelijke alinea
wijzigt, verandert dit niet ook alle andere beschrijvingen met dezelfde alinea.
</p>

<p>
Houd er ook rekening mee dat elk taalteam zijn eigen voorkeuren heeft, zoals
woordenlijsten of de stijl van aanhalingstekens. Volg deze normen zo nauwkeurig
mogelijk. De belangrijkste regels worden
<a href="https://ddtp.debian.org/ddtss/index.cgi/nl/wordlist">gepubliceerd</a>.
# Translators: create a link to your rules from "published"
Het is raadzaam om te beginnen met het bekijken van bestaande vertalingen,
hetzij via <a href="#DDTSS">DDTSS</a> of door te bladeren in beschrijvingen in
pakketbeheersystemen zoals
<a href="https://packages.debian.org/aptitude">aptitude</a>, om een idee te
krijgen van de vertaalvoorkeuren. Neem bij twijfel contact op met uw
<a href="https://lists.debian.org/debian-l10n-dutch/">taalteam</a>.
# Translators: create a link to your language team (mailing list, ...)
</p>

<toc-add-entry>Nazicht en fouten verbeteren</toc-add-entry>
# General proofread suggestions, not DDTSS specific
<p>
Alleen het DDTSS implementeert momenteel een werkwijze die nazicht vereist en
stuurt alleen vertalingen naar de DDTP die een vast aantal revisies hebben
gekregen.
</p>

<p>
Wanneer u veelvoorkomende typefouten of andere gemakkelijk te corrigeren fouten
opmerkt, zoals coderingsproblemen, is het mogelijk om elk nazichtproces te
omzeilen en dit voor alle pakketten in één keer op te lossen met behulp van een
script. Er wordt aangeraden dat slechts één vertrouwde vertaalcoördinator al
deze problemen verzamelt en het script toepast.
</p>

<p>
Omdat het nazicht veel tijd in beslag kan nemen (vooral als er steeds maar
kleine problemen worden opgelost), kan het een optie zijn om eenvoudige
typefouten en inconsistenties tijdens het nazicht gewoon te negeren en later
een controle te starten op al deze (hopelijk verzamelde) problemen. Dit
versnelt het nazicht en maakt het mogelijk om deze correcties later in alle
beschrijvingen toe te passen..
</p>

<toc-add-entry>Gebruik van de vertalingen</toc-add-entry>
<p>
Goede ondersteuning voor vertaalde pakketbeschrijvingen is beschikbaar sinds
het APT-pakket in
<a href="https://packages.debian.org/lenny/admin/apt">lenny</a>.
Met dit pakket kan elke gebruiker de beschrijvingen in de taal van zijn
voorkeur lezen in alle programma's die APT gebruiken. Dit omvat
<tt>apt-cache</tt>, <tt>aptitude</tt>, <tt>synaptic</tt> en diverse andere.
</p>

<p>
APT downloadt <tt>Translation-<var>taal</var></tt>-bestanden van Debian
spiegelservers. Zij zijn enkel beschikbaar voor lenny en recentere distributies.
De plaats van deze bestanden op de speiegelservers is
# Translators: use a proper mirror!
<a href="http://ftp.nl.debian.org/debian/dists/sid/main/i18n/">dists/main/sid/i18n/</a>.
</p>

<p>
Het is ook mogelijk om het gebruik van vertalingen uit te schakelen. Om dit te
bereiken voegt u gewoon
</p>
<pre>
APT::Acquire::Translation "none";
</pre>
<p>
toe aan <tt>/etc/apt/apt.conf</tt>. Naast <tt>none</tt> wordt ook een taalcode ondersteund.
</p>

<toc-add-entry name="todo">Te doen</toc-add-entry>

<p>
Hoewel er enige vooruitgang is geboekt met het DDTP, is er nog veel te doen:
</p>
<ul>
  <li>Alle vertaalteams zoeken momenteel naar nieuwe vertalers en personen die
      het nazicht van vertalingen op zich nemen om te helpen bij het verwerken
      van de echt grote lijst pakketten.</li>
  <li>Ondersteuning toevoegen voor het verbeteren van Engelse
      pakketbeschrijvingen tijdens het vertaal-/nazichtproces. Misschien kan
      dit worden gerealiseerd door een nieuwe Engelse pseudo-taal toe te voegen
      met de verbeterde beschrijving als vertaling, gekoppeld aan het
      automatisch indienen van een bugrapport na een succesvol nazicht.</li>
</ul>
<p>
U vindt <a href="https://salsa.debian.org/l10n-team/ddtp/-/issues">een meer
omvattende lijst op Salsa</a>, waar u ook uw ideeën en suggesties kwijt kunt.
</p>


