#use wml::debian::blend title="Ondersteuning bij de specifieke uitgave"
#use "navbar.inc"
#use wml::debian::translation-check translation="12c97b4a86d8b7e64c32dee1081bbf47b0bc84a1"

<h2>Documentatie</h2>

<p>Voordat u ondersteuning zoekt bij iemand anders, is het meestal goed om zelf een antwoord op uw probleem te proberen te vinden. Op die manier krijgt u meestal de antwoorden die u nodig heeft, en zelfs als dat niet het geval is, zal de ervaring met het lezen van de documentatie waarschijnlijk voor u in de toekomst nuttig zijn.</p>

<p>Zie de <a href="./docs">amateurradio-specifieke documentatiepagina</a> of de <a href="../../doc/">Debian documentatiepagina</a> voor een lijst met beschikbare documentatie.</p>

<h2>Mailinglijsten</h2>

<p>Debian wordt ontwikkeld via gedistribueerde ontwikkeling over de hele wereld. Daarom wordt de voorkeur gegeven aan e-mail om verschillende onderwerpen te bespreken. Een groot deel van de conversatie tussen Debian-ontwikkelaars en -gebruikers verloopt via verschillende mailinglijsten.</p>

# Translators: the first mailing list is English-only. I'll leave it up to you
# as to whether or not you remove reference to this list.

<p>Voor ondersteuning specifiek met betrekking tot het gebruik van amateurradiosoftware kunt u terecht op de Engelstalige <a
href="https://lists.alioth.debian.org/mailman/listinfo/pkg-hamradio-user">mailinglijst van Debian voor amateurradiogebruikers</a>.</p>

# Translators: please swap this next one out for the local language's list.

<p>Voor meer algemene ondersteuning in verband met Debian die geen betrekking heeft op het gebruik van amateurradiosoftware, kunt u terecht op de <a
href="https://lists.debian.org/debian-user-dutch/">Debian mailinglijst voor Nederlandstalige gebruikers</a>.</p>

<p>Voor gebruikersondersteuning in andere talen kunt u de <a href="https://lists.debian.org/users.html">tabel met mailinglijsten voor gebruikers</a> raadplegen.</p>

<h2>Onlinehulp in realtime via IRC</h2>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> is een manier om in realtime te chatten met mensen van over de hele wereld.
IRC-kanalen gewijd aan Debian zijn te vinden op
<a href="http://www.oftc.net/">OFTC</a>.</p>

<p>Om verbinding te maken, heeft u een IRC-client nodig. Enkele van de populairste clients zijn
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> en
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
die allemaal zijn verpakt voor Debian. Zodra u de client hebt geïnstalleerd, moet u hem vertellen dat hij verbinding moet maken met de server. In de meeste clients kunt u dat doen door het volgende te typen:</p>

<pre>
/server irc.debian.org
</pre>

<p>Zodra u verbonden bent, kunt u zich aanmelden bij het kanaal <code>#debian-hamchat</code> door het volgende te typen:</p>

<pre>
/join #debian-hamchat
</pre>

<p>Opmerking: clients zoals HexChat hebben vaak een andere, grafische gebruikersinterface voor het verbinden met servers/kanalen.</p>

<p>Er zijn ook een aantal andere IRC-netwerken waar u over Debian kunt praten.</p>
