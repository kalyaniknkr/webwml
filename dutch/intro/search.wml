#use wml::debian::template title="De Debian zoekmachine gebruiken" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

# Translator:       Bas Zoetekouw <bas@debian.org>
# Translation Date: Mon Jun 17 19:18:29 CEST 2002

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p>
Het Debian-project biedt zijn eigen zoekmachine aan op <a
href="https://search.debian.org/">https://search.debian.org/</a>.
Hier volgen enkele tips over hoe u deze kunt gebruiken en eenvoudige
zoekopdrachten kunt starten, maar ook meer complexe zoekopdrachten met
Booleaanse operatoren.
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>Eenvoudige zoekopdracht</h3>
      <p>De eenvoudigste manier om de zoekmachine te gebruiken is één enkel woord in het zoekveld in te voeren en op [Enter] te drukken. U kunt ook op de knop <em>Zoeken</em> klikken. De zoekmachine zal dan alle pagina's op onze website weergeven die dat woord bevatten. Voor de meeste zoekopdrachten zou dit goede resultaten moeten opleveren.</p>
      <p>U kunt ook op meer dan één woord zoeken. Ook dan zou u alle pagina's van de Debian-website moeten zien die alle woorden bevatten die u hebt ingevoerd. Om naar zinsdelen te zoeken, zet u ze tussen aanhalingstekens ("). Merk op dat de zoekmachine niet hoofdlettergevoelig is, dus zoeken naar <code>gcc</code> komt zowel overeen met "gcc" als met "GCC".</p>
      <p>Onder het zoekveld kunt u aangeven hoeveel resultaten u per pagina wilt zien. Het is ook mogelijk om een andere taal te kiezen; de websitezoekfunctie van Debian ondersteunt bijna 40 verschillende talen.</p>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>Booleaanse zoekopdracht</h3>
      <p>Als een eenvoudige zoekopdracht niet voldoende is, kunt u gebruik maken van Booleaanse zoekoperatoren. U kunt kiezen tussen <em>AND</em>, <em>OR</em>, en <em>NOT</em> of alle drie combineren. Zorg ervoor dat u hoofdletters gebruikt voor alle operatoren, zodat de zoekmachine ze herkent.</p>

      <ul>
        <li><b>AND</b> combineert twee uitdrukkingen en geeft pagina's die beide woorden bevatten. Bijvoorbeeld, <code>gcc AND patch</code> vindt alle pagina's die zowel, "gcc" als "patch" bevatten. In dit geval krijgt u dezelfde resultaten als wanneer u zoekt naar <code>gcc patch</code>, maar een expliciete <code>AND</code> kan nuttig zijn in combinatie met andere operatoren.</li>
        <li><b>OR</b> geeft resultaten als een van beide woorden in de pagina voorkomt. <code>gcc OR patch</code> vindt elke pagina die ofwel "gcc" ofwel "patch" bevat.</li>
        <li><b>NOT</b> wordt gebruikt om zoektermen uit de resultaten uit te sluiten. Bijvoorbeeld, <code>gcc NOT patch</code> vindt alles dat "gcc" bevat, maar niet "patch". <code>gcc AND NOT patch</code> geeft dezelfde resultaten, maar zoeken naar <code>NOT patch</code> wordt niet ondersteund. </li>
        <li><b>(...)</b> kan worden gebruikt om uitdrukkingen te groeperen. <code>(gcc OR make) NOT patch</code> zal bijvoorbeeld alle pagina's vinden die "gcc" of "make" bevatten, maar niet "patch".</li>
      </ul>
    </div>
  </div>
</div>

