# Status: published
# $Rev$
#use wml::debian::translation-check translation="ca2375b4ef2eebfd1c7fb93df901295ca6ea6586"
<define-tag pagetitle>Langetermijnondersteuning van Debian 10 bereikt einde levensduur</define-tag>
<define-tag release_date>2024-06-15</define-tag>
#use wml::debian::news

##
## Translators:
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
Het langetermijnondersteuningsteam (LTS) van Debian kondigt hierbij aan dat de
ondersteuning voor Debian 10 <q>buster</q> het einde van zijn levensduur bereikt
op 30 juni 2024, bijna vijf jaar na de initiële release op 6 juli 2019.
</p>

<p>
Vanaf juli zal Debian geen beveiligingsupdates meer leveren voor Debian 10. Een
beperkt aantal <q>buster</q>-pakketten zal worden ondersteund door externe
partijen. Gedetailleerde informatie is te vinden op de wiki-pagina
<a href="https://wiki.debian.org/LTS/Extended"> Extended LTS</a> over deze
voortgezette beperkte langetermijnondersteuning.
</p>

<p>
Het Debian LTS-team zal daarna de overgang naar Debian 11 <q>bullseye</q>, de
huidige, oude stabiele release, voorbereiden. Dankzij de gezamenlijke inspanning
van verschillende teams, waaronder het beveiligingsteam, het releaseteam en het
LTS-team, zal de levensduur van Debian 11 ook vijf jaar bedragen. Om de
levenscyclus van de releases van Debian gemakkelijker te kunnen volgen, zijn de
betrokken Debian-teams het volgende schema overeengekomen: drie jaar reguliere
ondersteuning plus twee jaar langetermijnondersteuning. Het LTS-team zal op 14
augustus 2024 de ondersteuning overnemen van het beveiligings- en het
releaseteam, drie jaar na de eerste release op 14 augustus 2021. De laatste
tussenrelease voor <q>bullseye</q> zal worden uitgebracht kort nadat het laatste
Debian 11 Security Advisory (DSA - beveiligingsadvies) is gepubliceerd.
</p>

<p>
Debian 11 zal langetermijnondersteuning krijgen tot 31 augustus 2026. De
ondersteunde architecturen blijven amd64, i386, arm64 en armhf.
</p>

<p>
Voor meer informatie over het gebruik van <q>bullseye</q>-LTS en het opwaarderen
vanuit <q>buster</q>-LTS, raadpleeg de wikipagina
<a href="https://wiki.debian.org/LTS/Using"> LTS/Using</a>.
</p>

<p>
Debian en het LTS-team willen graag alle gebruikers, ontwikkelaars, sponsors en
andere Debian-teams bedanken die een bijdrage hebben geleverd en het mogelijk
hebben gemaakt om de levensduur van eerdere stabiele releases te verlengen.
</p>

<p>
Als u aangewezen bent op Debian-LTS, overweeg dan
<a href="https://wiki.debian.org/LTS/Development">om u bij het team aan te
sluiten</a>, patches te leveren, tests uit te voeren of bij te dragen aan de
<a href="https:/ /wiki.debian.org/LTS/Funding">financiering van het project</a>.
</p>


<h2>Over Debian</h2>

<p>
Het Debian-project werd in 1993 opgericht door Ian Murdock als een echt vrij
gemeenschapsproject. Sindsdien is het project uitgegroeid tot een van de
grootste en meest invloedrijke opensource-projecten. Duizenden vrijwilligers van
over de hele wereld werken samen om Debian-software te creëren en te
onderhouden. Debian is beschikbaar in 70 talen en ondersteunt een groot aantal
computertypes. Het noemt zichzelf het <q>universele besturingssysteem</q>.
</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a> of stuur een e-mail naar
&lt;press@debian.org&gt;.</p>

<h2>Inschrijven / Uitschrijven</h2>
<p><a href="https://lists.debian.org/debian-announce/">Aanmelden of afmelden</a>
voor de mailinglijst met aankondigingen van Debian</p>
