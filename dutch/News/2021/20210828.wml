#use wml::debian::translation-check translation="eebf73f6f1306851590138e0b9fc3917cf4580a6"
<define-tag pagetitle>DebConf21-online afgesloten</define-tag>

<define-tag release_date>2021-08-28</define-tag>
#use wml::debian::news

<p>
Op zaterdag 28 augustus 2021 kwam er een einde aan de jaarlijkse Debian
conferentie voor ontwikkelaars en medewerkers.
</p>

<p>
Door de pandemie van het coronavirus (COVID-19) vond voor de tweede
keer DebConf21 online plaats.
</p>

<p>
Alle sessies werden gestreamd en er waren verschillende manieren om deel te
nemen: via IRC-berichten, via online collaboratieve tekstdocumenten en
via vergaderruimten voor videoconferenties.
</p>

<p>
Met 740 geregistreerde deelnemers uit meer dan 15 verschillende landen en in
totaal meer dan 70 lezingen, discussiesessies, BoF-bijeenkomsten (Wikipedia:
"een Birds of a Feather (BoF)-bijeenkomst is een informele ontmoeting op
conferenties, waar de aanwezigen groeperen op basis van een gedeeld belang en
discussies voeren zonder enige vooraf geplande agenda") en andere activiteiten,
was <a href="https://debconf21.debconf.org"> DebConf21</a> een groot succes.
</p>

<p>
Het voor eerdere online evenementen uitgewerkte opzet met Jitsi, OBS, Voctomix,
SReview, nginx en Etherpad, een webgebaseerde frontend voor voctomix, werd
verbeterd en met succes gebruikt voor DebConf21.
Alle onderdelen van de video-infrastructuur zijn vrije software, en worden
geconfigureerd via de publieke opslagplaats
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a>
van het Video Team.
</p>

<p>
Het <a href="https://debconf21.debconf.org/schedule/">schema</a> van DebConf21
bevatte een breed gamma van evenementen, gegroepeerd binnen verschillende
sporen:
</p>
<ul>
<li>Inleiding tot Vrije Software &amp; Debian,</li>
<li>Verpakking, beleid en Debian-infrastructuur,</li>
<li>Systeembeheer, automatisering en orkestratie,</li>
<li>Cloud en containers,</li>
<li>Beveiliging,</li>
<li>Gemeenschap, diversiteit, lokaal bereik en sociale context,</li>
<li>Internationalisering, lokalisering en toegankelijkheid,</li>
<li>Ingebedde toepassingen &amp; Kernel,</li>
<li>Doelgroepspecifieke uitgaven van Debian en van Debian afgeleide distributies,</li>
<li>Debian in kunst &amp; wetenschap</li>
<li>en andere.</li>
</ul>
<p>
De lezingen werden gestreamd vanuit twee zalen en verschillende van deze
activiteiten vonden plaats in uiteenlopende talen: Telugu, Portugees,
Malayalam, Kannada, Hindi, Marathi en Engels, waardoor een meer divers publiek
ervan kon genieten en eraan kon deelnemen.
</p>

<p>
Tussen de lezingen door toonde de videostream een lus met de gebruikelijke
sponsors, maar ook enkele extra clips waaronder foto's van vorige conferenties
van Debian, leuke weetjes over Debian en korte video's met berichtjes die door
aanwezigen werden gestuurd om met hun Debian-vrienden te communiceren.
</p>

<p>Het publiciteitsteam van Debian bracht de gebruikelijke "rechtstreekse
berichtgeving" door met micronews de verschillende evenementen aan te kondigden
om deelname aan te moedigen. Het DebConf-team zorgde ook voor verschillende
<a href="https://debconf21.debconf.org/schedule/mobile/">opties om het
programma via de mobilofoon te volgen</a>.
</p>

<p>
Voor degenen die niet konden deelnemen, zijn de meeste lezingen en sessies al
beschikbaar via de
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/">website met gearchiveerde Debian-bijeenkomsten</a>,
en de overige zullen in de komende dagen verschijnen.
</p>

<p>
De <a href="https://debconf21.debconf.org/">DebConf21</a>-website blijft actief
voor archiveringsdoeleinden en zal links blijven bieden naar de presentaties en
de video's van lezingen en evenementen.
</p>

<p>
Naar verwachting zal <a href="https://wiki.debian.org/DebConf/22">DebConf22</a>
volgend jaar in juli 2022 plaats vinden in Prizren, Kosovo.
</p>

<p>
DebConf streeft naar een veilige en gastvrije omgeving voor alle deelnemers.
Tijdens de conferentie waren verschillende teams (receptie, welkomstteam en
gemeenschapsteam) beschikbaar om de deelnemers te helpen de conferentie zo goed
mogelijk te ervaren, en oplossingen te vinden voor eventuele problemen.
Raadpleeg voor meer informatie hierover op de website van DebConf21 de
webpagina over de <a href="https://debconf21.debconf.org/about/coc/">gedragscode</a>.
</p>

<p>
Debian dankt de talrijke
<a href="https://debconf21.debconf.org/sponsors/">sponsors</a> voor hun
inzet bij het ondersteunen van DebConf21, in het bijzonder onze Platina
Sponsors:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://code4life.roche.com/">Roche</a>,
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>
en <a href="https://google.com/">Google</a>.
</p>

<h2>Over Debian</h2>
<p>
Het Debian-project werd in 1993 door Ian Murdock opgericht als een echt vrij
gemeenschapsproject. Sindsdien is het project uitgegroeid tot een van de
grootste en meest invloedrijke opensourceprojecten. Duizenden vrijwilligers van
over de hele wereld werken samen om Debian-software te maken en te onderhouden.
Debian is beschikbaar in 70 talen en ondersteunt een groot aantal
computertypes. Het noemt zichzelf het <q>universele besturingssysteem</q>.
</p>

<h2>Over DebConf</h2>

<p>
DebConf is de conferentie van de ontwikkelaars van het Debian-project. Naast een
volledig programma van technische, sociale en beleidslezingen, biedt DebConf
een kans voor ontwikkelaars, medewerkers en andere geïnteresseerden om elkaar
persoonlijk te ontmoeten en nauwer samen te werken. Het heeft sinds 2000
jaarlijks plaatsgevonden op uiteenlopende locaties als Schotland, Argentinië en
Bosnië en Herzegovina. Meer informatie over DebConf is te vinden op
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Over Lenovo</h2>
<p>
Als wereldwijde technologieleider die een breed gamma geconnecteerde producten
fabriceert, waaronder smartphones, tablets, pc's en werkstations, evenals
AR/VR-apparaten en slimme oplossingen voor thuis en op kantoor en
datacentertoepassingen aanbiedt, begrijpt
<a href="https://www.lenovo.com">Lenovo</a> hoe essentieel open systemen en
platforms zijn in een geconnecteerde wereld.
</p>

<h2>Over Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is het grootste
webhostingbedrijf van Zwitserland en het biedt ook back-up- en opslagdiensten,
oplossingen voor organisatoren van evenementen, livestreaming en
video-on-demanddiensten. Het is de volledige eigenaar van zijn datacenters en
van alle elementen die van cruciaal belang zijn voor de werking van de door het
bedrijf aangeboden diensten en producten (zowel software als hardware).
</p>

<h2>Over Roche</h2>
<p>
<a href="https://code4life.roche.com/">Roche</a> is een grote internationale
leverancier van farmaceutische producten en het is een onderzoeksbedrijf dat
zich toelegt op gepersonaliseerde gezondheidszorg. Wereldwijd werken meer dan
100.000 medewerkers met behulp van wetenschap en technologie aan het oplossen
van enkele van de grootste uitdagingen voor de mensheid. Roche is sterk
betrokken bij door de overheid gefinancierde onderzoeksprojecten in
samenwerking met andere industriële en academische partners en ondersteunt
DebConf sinds 2017.
</p>

<h2>Over Amazon Web Services (AWS)</h2>
<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> is een van
's werelds meest omvattende en meest gebruikte cloudplatformen en het biedt
meer dan 175 volledig functionele diensten aan vanuit datacenters over de hele
wereld (in 77 beschikbaarheidszones binnen 24 geografische regio's). Tot de
klanten van AWS behoren snelgroeiende startups, grote ondernemingen en
toonaangevende overheidsinstanties.
</p>

<h2>Over Google</h2>
<p>
<a href="https://google.com/">Google</a> is een van de grootste
technologiebedrijven ter wereld, dat een breed gamma aan internetgerelateerde
diensten en producten aanbiedt, zoals online advertentietechnologieën,
zoekfuncties, clouddiensten, software en hardware.
</p>
<p>
Google ondersteunt Debian al meer dan tien jaar door DebConf te sponsoren, en
het is ook een Debian-partner die delen sponsort van de continue
integratie-infrastructuur van <a href="https://salsa.debian.org">Salsa</a>
binnen het Google cloudplatform.

</p>

<h2>Contactinformation</h2>

<p>Raadpleeg voor verdere informatie de webpagina van DebConf21 op
<a href="https://debconf21.debconf.org/">https://debconf21.debconf.org/</a>
of stuur een e-mail naar &lt;press@debian.org&gt;.</p>
