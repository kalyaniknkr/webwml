#use wml::debian::template title="Debian verkrijgen"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="47e0a86f4e71194a409b72e7ed1faaa3dc3160d5"


<p>Deze pagina bevat opties voor het installeren van Debian Stable, de stabiele release van Debian.


<ul>
<li> <a href="../CD/http-ftp/#mirrors">Spiegelservers voor het downloaden</a> van installatie-images
<li> <a href="../releases/stable/installmanual">Installatiehandleiding</a> met gedetailleerde installatie-instructies
<li> <a href="../releases/stable/releasenotes">Notities bij de release</a>
<li> <a href="../devel/debian-installer/">ISO-images voor Debian testing</a>
<li> <a href="../CD/verify">De authenticiteit van images van Debian verifiëren</a>
</ul>
</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Een installatie-image downloaden</a></h2>
   <ul>
     <li>Een <a href="netinst"><strong>klein installatie-image</strong></a>:
          kan snel gedownload worden en moet op een verwijderbare schijf geschreven
          worden. Om dit te kunnen gebruiken, heeft uw machine een internetverbinding
          nodig.
      <ul class="quicklist downlist">
        <li><a title="Het installatieprogramma voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bits
            pc netinst iso</a></li>
        <li><a title="Het installatieprogramma voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bits
            pc netinst iso</a></li>
	  <li><a title="Cd-torrents voor 64-bits Intel en AMD pc's downloaden"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bits pc netinst torrents</a></li>
	  <li><a title="Cd-torrents voor reguliere 32-bits Intel en AMD pc's downloaden"
		 href="<stable-images-url/>/i386/bt-cd/">32-bits pc netinst torrents</a></li>
      </ul>
     </li>
     <li>Een groter <a href="../CD/"><strong>compleet
      installatie-image</strong></a>: bevat meer pakketten, zodat het eenvoudiger te
      installeren is op een machine zonder internetverbinding.
      <ul class="quicklist downlist">
	  <li><a title="Het installatieprogramma voor 64-bits Intel en AMD pc's downloaden"
	         href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">64-bits
	      pc dvd-1 iso</a></li>
	  <li><a title="Het installatieprogramma voor reguliere 32-bits Intel en AMD pc's downloaden"
		 href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">32-bits
	      pc dvd-1 iso</a></li>
        <li><a title="Dvd-torrents voor 64-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/amd64/bt-dvd/">64-bits pc torrents (dvd)</a></li>
        <li><a title="Dvd-torrents voor reguliere 32-bits Intel en AMD pc's downloaden"
               href="<stable-images-url/>/i386/bt-dvd/">32-bits pc torrents (dvd)</a></li>
      </ul>
     </li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Debian live uitproberen voordat u installeert</a></h2>
    <p>
      U kunt Debian uitproberen door een live-systeem op te starten vanaf een cd, dvd of USB-stick zonder bestanden op de computer te installeren. U kunt ook het meegeleverde <a href="https://calamares.io">Calamares-installatieprogramma</a> uitvoeren. Alleen beschikbaar voor 64-bits pc.
      Meer <a href="../CD/live#choose_live">informatie over deze methode</a> lezen.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Gnome live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live-Gnome</a></li>
      <li><a title="Xfce live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live-Xfce</a></li>
      <li><a title="KDE live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live-KDE</a></li>
      <li><a title="Ander live-ISO voor 64-bits Intel en AMD pc downloaden"
            href="<live-images-url/>/amd64/iso-hybrid/">Ander live-ISO</a></li>
      <li><a title="Live-torrents voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/bt-hybrid/">Live-torrents</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Cd's, dvd's of USB-sticks kopen bij een leverancier van Debian-installatiemedia</a></h2>

   <p>
      Veel van de leveranciers verkopen de distributie voor minder dan 5 US$ plus verzendkosten (kijk op hun webpagina om te zien of ze internationaal verzenden).
   </p>

   <p>Dit zijn de belangrijkste voordelen van cd's:</p>

   <ul>
     <li>U kunt de installatie uitvoeren op computers zonder internetverbinding.</li>
	 <li>U kunt Debian installeren zonder zelf alle pakketten te downloaden.</li>
   </ul>

   <h2><a href="pre-installed">Een computer kopen waarop Debian vooraf is geïnstalleerd</a></h2>
   <p>Dit biedt een aantal voordelen:</p>
   <ul>
    <li>U hoeft Debian niet te installeren.</li>
    <li>De installatie is vooraf geconfigureerd voor de hardware.</li>
    <li>De leverancier kan technische ondersteuning bieden.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Een Debian cloudimage gebruiken</a></h2>
    <p>Een officieel <a href="https://cloud.debian.org/images/cloud/"><strong>cloudimage</strong></a>,
            gebouwd door het cloudteam, kan gebruikt worden op:</p>    <ul>
      <li>uw OpenStack provider, in qcow2 formaat of raw formaat.
      <ul class="quicklist downlist">
	   <li>64-bits AMD/Intel (<a title="OpenStack image voor 64-bits AMD/Intel qcow2" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="OpenStack image voor 64-bits AMD/Intel raw" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>64-bits ARM (<a title="OpenStack image voor 64-bits ARM qcow2" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="OpenStack image voor 64-bits ARM raw" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
	   <li>64-bits Little Endian PowerPC (<a title="OpenStack image voor 64-bits Little Endian PowerPC qcow2" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="OpenStack image voor 64-bits Little Endian PowerPC raw" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>een lokale QEMU virtuele machine, in de indelingen qcow2 of raw.
      <ul class="quicklist downlist">
	   <li>64-bits AMD/Intel (<a title="QEMU image voor 64-bits AMD/Intel qcow2" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="QEMU image voor 64-bits AMD/Intel raw" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
       <li>64-bits ARM (<a title="QEMU image voor 64-bits ARM qcow2" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="QEMU image voor 64-bits ARM raw" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
	   <li>64-bits Little Endian PowerPC (<a title="QEMU image voor 64-bits Little Endian PowerPC qcow2" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="QEMU image voor 64-bits Little Endian PowerPC raw" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, ofwel als een machine-image of via de AWS Marktplaats.
	   <ul class="quicklist downlist">
	    <li><a title="Amazon machine-images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon machine-images</a></li>
	    <li><a title="AWS Marktplaats" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplaats</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, op de Azure Marktplaats.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 12 op Azure Marktplaats" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
        <li><a title="Debian 11 op Azure Marktplaats" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
	   </ul>
      </li>
    </ul>
 </div>
</div>
