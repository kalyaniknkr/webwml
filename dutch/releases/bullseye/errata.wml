#use wml::debian::template title="Debian 11 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="9a0ce821cf7f93dde0c808cadde4b960836cfd50"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Bekende problemen</toc-add-entry>
<toc-add-entry name="security">Beveiligingsproblemen</toc-add-entry>

<p>Het Debian veiligheidsteam brengt updates uit voor pakketten uit de stabiele
release waarin problemen in verband met beveiliging vastgesteld werden.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina's</a> voor
informatie over eventuele beveiligingsproblemen die in <q>bullseye</q> ontdekt
werden.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan
<tt>/etc/apt/sources.list</tt> om toegang te hebben tot de laatste
beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Een
dergelijke bijwerking wordt gewoonlijk aangeduid als een tussenrelease.</p>

<ul>
  <li>De eerste tussenrelease, 11.1, werd uitgebracht op
      <a href="$(HOME)/News/2021/20211009">9 oktober 2021</a>.</li>
  <li>De tweede tussenrelease, 11.2, werd uitgebracht op
      <a href="$(HOME)/News/2021/20211218">18 december 2021</a>.</li>
  <li>De derde tussenrelease, 11.3, werd uitgebracht op
      <a href="$(HOME)/News/2022/20220326">26 maart 2022</a>.</li>
  <li>De vierde tussenrelease, 11.4, werd uitgebracht op
      <a href="$(HOME)/News/2022/20220709">9 juli 2022</a>.</li>
  <li>De vijfde tussenrelease, 11.5, werd uitgebracht op
      <a href="$(HOME)/News/2022/2022091002">10 september 2022</a>.</li>
  <li>De zesde tussenrelease, 11.6, werd uitgebracht op
      <a href="$(HOME)/News/2022/20221217">17 december 2022</a>.</li>
  <li>De zevende tussenrelease, 11.7, werd uitgebracht op
      <a href="$(HOME)/News/2023/20230429">29 april 2023</a>.</li>
  <li>De achtste tussenrelease, 11.8, werd uitgebracht op
      <a href="$(HOME)/News/2023/2023100702">7 oktober 2023</a>.</li>
  <li>De negende tussenrelease, 11.9, werd uitgebracht op
      <a href="$(HOME)/News/2024/2024021002">10 februari 2024</a>.</li>
  <li>De tiende tussenrelease, 11.10, werd uitgebracht op
      <a href="$(HOME)/News/2024/2024062902">29 juni 2024</a>.</li>
</ul>

<ifeq <current_release_bullseye> 11.0 "

<p>Er zijn nog geen tussenreleases voor Debian 11.</p>" "

<p>Zie de <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
ChangeLog</a>
voor details over wijzigingen tussen 11 en <current_release_bullseye/>.</p>"/>


<p>Verbeteringen voor de uitgebrachte stabiele distributie gaan dikwijls door een
uitgebreide testperiode voordat ze in het archief worden aanvaard.
Deze verbeteringen zijn echter wel beschikbaar in de map
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> van elke Debian archief-spiegelserver.</p>

<p>Als u APT gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# voorgestelde toevoegingen aan een tussenrelease van 11
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installatiesysteem</toc-add-entry>

<p>
Raadpleeg voor informatie over errata en updates van het installatiesysteem
de pagina met <a href="debian-installer/">installatie-informatie</a>.
</p>
