#use wml::debian::template title="Obtendo o Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="47e0a86f4e71194a409b72e7ed1faaa3dc3160d5"

<p>Esta página contém as opções para instalar o Debian estável (stable).
  <ul>
    <li><a href="../CD/http-ftp/#mirrors">Espelhos para baixar</a> imagens para instalação</li>
    <li><a href="../releases/stable/installmanual">Manual de instalação</a> com instruções detalhadas de instalação</li>
    <li><a href="../releases/stable/releasenotes">Notas de lançamento</a></li>
    <li><a href="../releases/">Outras versões</a> como test (testing) ou instável (unstable)</li>
    <li> <a href="../devel/debian-installer/">Imagens ISO do Debian test (testing)</a>
    <li> <a href="../CD/verify">Verificar autenticidade das imagens do Debian</a>
  </ul>
</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Baixe uma imagem para instalação</a></h2>
    <ul>
      <li>Uma <a href="netinst"><strong>imagem pequena para instalação</strong></a>:
          pode ser baixada rapidamente e deve ser gravada em um disco removível.
          Para isso, você precisará de uma máquina com conexão à Internet.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">Iso netinst
	      para PC de 64 bits</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">Iso netinst
              para PC de 32 bits</a></li>
	  <li><a title="Download CD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-cd/">Torrents netinst para PC de 64 bits</a></li>
	  <li><a title="Download CD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-cd/">Torrents netinst para PC de 32 bits</a></li>
	</ul>
      </li>
      <li>Uma <a href="../CD/"><strong>imagem grande e completa para instalação</strong></a>:
        contém mais pacotes, facilitando a instalação em máquinas sem conexão com a Internet.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">Iso DVD-1
	         para PC de 64 bits</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">Iso DVD-1
	         para PC de 32 bits</a></li>
	  <li><a title="Download DVD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-dvd/">Torrents para PC de 64 bits (DVD)</a></li>
	  <li><a title="Download DVD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-dvd/">Torrents para PC de 32 bits (DVD)</a></li>
	</ul>
       </li>
     </ul>
   </div>
   <div class="item col50 lastcol">
     <h2><a href="../CD/live/">Experimente o Debian live antes de instalar</a></h2>
     <p>
       Você pode experimentar o Debian inicializando um sistema live a partir de
       um CD, DVD ou pendrive USB sem instalar nenhum arquivo no computador.
       Você também pode executar o <a href="https://calamares.io">instalador Calamares</a>
       que já vem incluído. Disponível apenas para PC de 64 bits.
       Leia mais <a href="../CD/live#choose_live">informações sobre esse método</a>.
     </p>
    <ul class="quicklist downlist">
      <li><a title="Download Gnome live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live com GNOME</a></li>
      <li><a title="Download Xfce live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live com Xfce</a></li>
      <li><a title="Download KDE live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live com KDE</a></li>
      <li><a title="Download other live ISO for 64-bit Intel and AMD PC"
            href="<live-images-url/>/amd64/iso-hybrid/">Outras ISOs live</a></li>
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">Torrents live</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Compre CDs, DVDs ou pendrives em um(a) fornecedor(a) de
      mídia de instalação do Debian</a></h2>

    <p>
       Vários(as) fornecedores(as)) vendem a distribuição por menos de US$ 5 mais frete
       (verifique as página de fornecedores(as)) na web para ver se enviam
       internacionalmente).
    </p>

    <p>Aqui estão as vantagens básicas dos CDs:</p>

    <ul>
      <li>Você pode instalar em máquinas sem conexão com a Internet.</li>
      <li>Você pode instalar o Debian sem baixar todos os pacotes.</li>
    </ul>

    <h2><a href="pre-installed">Compre um computador com Debian pré-instalado</a></h2>
    <p>Há uma série de vantagens nisso:</p>
    <ul>
     <li>Você não precisa instalar o Debian.</li>
     <li>A instalação é pré-configurada para corresponder ao hardware.</li>
     <li>O(A) vendedor(a)) pode fornecer suporte técnico.</li>
    </ul>
   </div>

  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Use uma imagem para nuvem (cloud) Debian</a></h2>
    <p>Uma <a href="https://cloud.debian.org/images/cloud/"><strong>imagem para nuvem (cloud)</strong></a>, criada pela equipe cloud, pode ser usada em:</p>
    <ul>
       <li>Seu provedor OpenStack, em formato qcow2 ou raw.
      <ul class="quicklist downlist">
          <li>AMD/Intel de 64 bits (<a title="Imagem OpenStack qcow2 para AMD/Intel de 64 bits" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="Imagem OpenStack raw para AMD/Intel de 64 bits" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
          <li>ARM de 64 bits (<a title="Imagem OpenStack qcow2 para ARM de 64 bits" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="Imagem OpenStack raw para ARM de 64 bits" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
          <li>PowerPC Little Endian de 64 bits (<a title="Imagem OpenStack qcow2 para PowerPC Little Endian de 64 bits" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="Imagem OpenStack raw para PowerPC Little Endian de 64 bits" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
       </ul>
       </li>
       <li>Uma máquina virtual QEMU local, em formato qcow2 ou raw.
      <ul class="quicklist downlist">
          <li>AMD/Intel de 64 bits (<a title="Imagem QEMU qcow2 para AMD/Intel de 64 bits" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="Imagem QEMU raw para AMD/Intel de 64 bits" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
        <li>ARM de 64 bits (<a title="Imagem QEMU qcow2 para ARM de 64 bits" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="Imagem QEMU raw para ARM de 64 bits" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
        <li>PowerPC Little Endian de 64 bits (<a title="Imagem QEMU qcow2 para PowerPC Little Endian de 64 bits" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="Imagem QEMU raw para PowerPC Little Endian de 64 bits" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
       </ul>
       </li>
      <li>Amazon EC2, como imagem para máquina ou por meio do AWS Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Imagens para máquina Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Imagens para máquina Amazon</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, no Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 12 no Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 (Bookworm)</a></li>
	    <li><a title="Debian 11 no Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 (Bullseye)</a></li>
	   </ul>
      </li>
    </ul>
  </div>
</div>
