#use wml::debian::template title="Informações do Debian 2.1 (slink)" BARETITLE=yes
#use wml::debian::release
#use wml::debian::translation-check translation="9d55b2307fc18a23f41144b417c9ec941cc3abf8"

<strong>O Debian 2.1 foi substituido.</strong>

<p>Desde que as <a href="../">versões mais recentes</a> foram feitas, a versão
2.1 foi substituída. Essas páginas estão sendo mantidas para fins históricos.
Você deve estar ciente de que o Debian 2.1 não é mais mantido.<p>

  As seguintes arquiteturas são suportadas no Debian 2.1:

<ul>
<li> alpha
<li> i386
<li> mk68k
<li> sparc
</ul>

<h2><a name="release-notes"></a>Notas de lançamento</h2>

<p>
Para descobrir o que há de novo no Debian 2.1, consulte as notas de lançamento
para a sua arquitetura. As notas de lançamento também contêm instruções para
usuários(as) que estão atualizando de versões anteriores.

<ul>
<li><a href="alpha/release-notes.txt">Notas de lançamento para o Alpha<a></li>
<li><a href="i386/release-notes.txt">Notas de lançamento para o PC 32-bit (i386)<a></li>
<li><a href="m68k/release-notes.txt">Notas de lançamento para o Motorola 680x0<a></li>
<li><a href="sparc/release-notes.txt">Notas de lançamento para O SPARC<a></li>
</ul>

<h2><a name="errata"></a>Errata</h2>

<p>
Às vezes, no caso de problemas críticos ou atualizações de segurança, a versão
já lançada (neste caso, o slink) é atualizada. Geralmente eles são indicados
como lançamentos pontuais. A atual versão pontual é o Debian 2.1r5. Você pode
encontrar o
<a href="http://archive.debian.org/debian/dists/slink/ChangeLog">ChangeLog</a>
em qualquer espelho do repositório Debian.

<p>
O slink é certificado para uso com a série 2.0.x de kernels do Linux. Se você
deseja executar o kernel do Linux 2.2.x com o slink, consulte a
<a href="running-kernel-2.2">lista de problemas conhecidos</a>.

<h3>APT</h3>

<p>
Uma versão atualizada do <code>apt</code> está disponível no Debian, a partir
do 2.1r3. O benefício desta versão atualizada é principalmente poder lidar com
a instalação a partir de vários CD-ROMs. Isso torna desnecessária a opção de
aquisição do <code>dpkg-multicd</code> no <code>dselect</code>. No entanto,
seu CD-ROM 2.1 pode conter um <code>apt</code> mais antigo, então você pode
querer atualizar usando o que está agora no slink.

<h2><a name="acquiring"></a>Obtendo o Debian 2.1</h2>

<p>
O Debian está disponível eletronicamente ou a partir de vendedores(as) de CD.

<h3>Comprar o Debian em CD</h3>

<p>
Mantemos uma <a href="../../CD/vendors/">lista de vendedores(as) de CD</a>
que vendem CDs do Debian 2.1.

<h3>Downloading Debian Over the Internet</h3>

<p>
Mantemos uma <a href="../../distrib/ftplist">lista de sites</a> que
espelham a distribuição.

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
