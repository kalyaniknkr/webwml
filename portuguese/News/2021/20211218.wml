#use wml::debian::translation-check translation="43ee4d624248d2eac95a5997ba86398be35914ec"
<define-tag pagetitle>Debian 11 atualizado: 11.2 lançado</define-tag>
<define-tag release_date>2021-12-18</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian tem o prazer de anunciar a segunda atualização da
versão Debian estável (stable) <release> (codinome <q><codename></q>).
Essa atualização pontual adiciona principalmente correções para problemas
de segurança, juntamente com alguns ajustes para problemas sérios.
Os avisos de segurança
já foram publicados separadamente e são referenciados quando disponíveis.</p>

<p>Por favor, note que este lançamento pontual não constitui uma nova versão do
Debian <release>, mas apenas atualiza alguns dos pacotes inclusos. Não há
necessidade de substituir a mídia <q><codename></q>. Após sua instalação,
os pacotes podem ser atualizados para a versão mais recente utilizando um
espelho Debian atualizado.</p>

<p>Para quem instala atualizações frequentemente de
security.debian.org, não terá que atualizar muitos pacotes, e a maioria das
atualizações estão incluídas no lançamento pontual.</p>

<p>Novas imagens de instalação estarão disponíveis em breve nos repositórios
oficiais.</p>

<p>A atualização de uma instalação existente para esta nova revisão pode
ser realizada apontando o sistema de gerenciamento de pacotes para um dos vários
espelhos HTTP do Debian.
Uma lista de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction authheaders "Nova versão upstream com correções de bugs">
<correction base-files "Atualiza /etc/debian_version para a versão pontual 11.2">
<correction bpftrace "Corrige indexação de vetor">
<correction brltty "Corrige operação sob X ao usar sysvinit">
<correction btrbk "Corrige regressão na atualização para CVE-2021-38173">
<correction calibre "Corrige erro de sintaxe">
<correction chrony "Corrige vinculação a socket para um dispositivo de rede com um nome maior que 3 caracteres quando o filtro de chamadas de sistema está habilitado">
<correction cmake "Adiciona PostgreSQL 13 em versões conhecidas">
<correction containerd "Nova versão upstream estável; lida com processamento de manifesto OCI ambíguo [CVE-2021-41190]; suporta <q>clone3</q> no perfil seccomp padrão">
<correction curl "Remove -ffile-prefix-map de curl-config, corrigindo coinstalabilidade de libcurl4-gnutls-dev sob multiarch">
<correction datatables.js "Corrige escape insuficiente de vetores passados para função de entidades de escape HTML [CVE-2021-23445]">
<correction debian-edu-config "pxe-addfirmware: Corrige caminho do servidor TFTP; melhora suporte para configuração e manutenção de chroot LTSP">
<correction debian-edu-doc "Atualiza manual do Edu Bullseye a partir da wiki; atualiza traduções">
<correction debian-installer "Reconstrói contra proposed-updates; atualiza kernel ABI para -10">
<correction debian-installer-netboot-images "Reconstrói contra proposed-updates">
<correction distro-info-data "Atualização inclui dados para Ubuntu 14.04 e 16.04 ESM; adiciona Ubuntu 22.04 LTS">
<correction docker.io "Corrige possível alteração de permissões de sistema de arquivos do host [CVE-2021-41089]; bloqueia permissões de arquivo em /var/lib/docker [CVE-2021-41091]; previne que credenciais sejam enviadas para o registro padrão [CVE-2021-41092]; adiciona suporte para chamada de sistema <q>clone3</q> na política seccomp padrão">
<correction edk2 "Vulnerabilidade Address Boot Guard TOCTOU [CVE-2019-11098]">
<correction freeipmi "Instala arquivos pkgconfig no local correto">
<correction gdal "Corrige suporte a BAG 2.0 Extract no driver LVBAG">
<correction gerbv "Corrige problema de escrita fora dos limites [CVE-2021-40391]">
<correction gmp "Corrige problema de estouro de inteiro e buffer [CVE-2021-43618]">
<correction golang-1.15 "Nova versão upstream estável; corrige <q>net/http: panic due to racy read of persistConn after handler panic</q> [CVE-2021-36221]; corrige <q>archive/zip: overflow in preallocation check can cause OOM panic</q> [CVE-2021-39293]; corrige problema de over-run de buffer [CVE-2021-38297], problema de leitura fora dos limites [CVE-2021-41771], problemas com negação de serviço [CVE-2021-44716 CVE-2021-44717]">
<correction grass "Corrige processamento de formatos GDAL quando a descrição contém dois-pontos">
<correction horizon "Reabilita traduções">
<correction htmldoc "Corrige problemas de estouro de buffer [CVE-2021-40985 CVE-2021-43579]">
<correction im-config "Prefere Fcitx5 em vez de Fcitx4">
<correction isync "Corrige problemas múltiplos de estouro de buffer [CVE-2021-3657]">
<correction jqueryui "Corrige problemas de execução de código não confiável [CVE-2021-41182 CVE-2021-41183 CVE-2021-41184]">
<correction jwm "Corrige quebra ao usar o item de menu <q>Move</q>">
<correction keepalived "Corrige política DBus excessivamente abrangente [CVE-2021-44225]">
<correction keystone "Resolve vazamento de informações que permitem determinação da existência de usuário(a) [CVE-2021-38155]; aplica algumas melhorias de desempenho para o arquivo padrão keystone-uwsgi.ini">
<correction kodi "Corrige estouro de buffer nas playlists PLS [CVE-2021-42917]">
<correction libayatana-indicator "Escala ícones ao carregar de arquivo; previne quebras regulares nos miniaplicativos indicadores">
<correction libdatetime-timezone-perl "Atualiza dados incluídos">
<correction libencode-perl "Corrige vazamento de memória em Encode.xs">
<correction libseccomp "Adiciona suporte a chamadas de sistema até Linux 5.15">
<correction linux "Nova versão upstream estável; aumenta ABI para 10; RT: atualiza para 5.10.83-rt58">
<correction linux-signed-amd64 "Nova versão upstream estável; aumenta ABI para 10; RT: atualiza para 5.10.83-rt58">
<correction linux-signed-arm64 "Nova versão upstream estável; aumenta ABI para 10; RT: atualiza para 5.10.83-rt58">
<correction linux-signed-i386 "Nova versão upstream estável; aumenta ABI para 10; RT: atualiza para 5.10.83-rt58">
<correction lldpd "Corrige problema de estouro de heap [CVE-2021-43612]; não configura etiqueta VLAN se o cliente não configurá-la">
<correction mrtg "Corrige erros em nomes variáveis">
<correction node-getobject "Resolve problema de poluição de protótipo [CVE-2020-28282]">
<correction node-json-schema "Resolve problema de poluição de protótipo [CVE-2021-3918]">
<correction open3d "Garante que python3-open3d dependa de python3-numpy">
<correction opendmarc "Corrige opendmarc-import; aumenta tamanho máximo suportado de tokens em cabeçalhos ARC_Seal, resolvendo quebras">
<correction plib "Corrige problema de estouro de inteiro [CVE-2021-38714]">
<correction plocate "Corrige problema pelo qual caracteres não ASCII poderiam escapar erroneamente">
<correction poco "Corrige instalação de arquivos CMake">
<correction privoxy "Corrige vazamentos de memória [CVE-2021-44540 CVE-2021-44541 CVE-2021-44542]; corrige problema de scripts entre sites [CVE-2021-44543]">
<correction publicsuffix "Atualiza dados incluídos">
<correction python-django "Nova versão upstream estável: corrige desvio potencial de um controle de acesso upstream baseado em caminhos de URL [CVE-2021-44420]">
<correction python-eventlet "Corrige compatibilidade com dnspython 2">
<correction python-virtualenv "Corrige quebra ao usar --no-setuptools">
<correction ros-ros-comm "Corrige problema de negação de serviço [CVE-2021-37146]">
<correction ruby-httpclient "Usa loja de certificados de sistema">
<correction rustc-mozilla "Novo pacote-fonte para suportar construção das versões mais novas de firefox-esr e thunderbird">
<correction supysonic "Link simbólico de jquery em vez de carregá-lo diretamente; arquivos CSS bootstrap minimizados com links simbólicos corretos">
<correction tzdata "Atualiza dados para Fiji e Palestina">
<correction udisks2 "Opções de montagem: usa sempre errors=remount-ro para sistemas de arquivo ext [CVE-2021-3802]; usa comando mkfs para formatar partições exfat; adiciona Recommends exfatprogs como alternativa preferida">
<correction ulfius "Corrige uso de alocadores personalizados com ulfius_url_decode e ulfius_url_encode">
<correction vim "Corrige estouros de heap [CVE-2021-3770 CVE-2021-3778], problema use after free [CVE-2021-3796]; remove alternativas vim-gtk durante vim-gtk -&gt; transição para vim-gtk3, facilitando atualizações a partir do buster">
<correction wget "Corrige downloads de mais de 2GB em sistemas 32-bit">
</table>


<h2>Atualizações de segurança</h2>


<p>Essa revisão adiciona as seguintes atualizações de segurança da versão
estável (stable).
A equipe de segurança já lançou um comunicado para cada uma dessas
atualizações:</p>

<table border=0>
<tr><th>ID Consultivo</th> <th>Pacote</th></tr>
<dsa 2021 4980 qemu>
<dsa 2021 4981 firefox-esr>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4984 flatpak>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4988 libreoffice>
<dsa 2021 4989 strongswan>
<dsa 2021 4992 php7.4>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4996 wpewebkit>
<dsa 2021 4998 ffmpeg>
<dsa 2021 5002 containerd>
<dsa 2021 5003 ldb>
<dsa 2021 5003 samba>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5007 postgresql-13>
<dsa 2021 5008 node-tar>
<dsa 2021 5009 tomcat9>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5016 nss>
<dsa 2021 5017 xen>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5022 apache-log4j2>
</table>



<h2>Debian Installer</h2>
<p>O instalador foi atualizado para incluir correções incorporadas na versão
estável (stable) pelo lançamento pontual.</p>

<h2>URLs</h2>

<p>A lista completa de pacotes que sofreram mudanças com esta revisão:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A versão estável (stable) atual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (notas de lançamento, errata, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios e informações de segurança:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O Projeto Debian é uma associação de desenvolvedores(as) de software livre
que voluntariamente doam seu tempo e esforço para produzir o sistema
operacional completamente livre Debian.</p>

<h2>Informações para contato</h2>

<p>Para mais informações, por favor visite a página do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, ou envie um e-mail (em inglês)
para &lt;press@debian.org&gt;, ou entre em contato com a equipe de lançamento
da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>


