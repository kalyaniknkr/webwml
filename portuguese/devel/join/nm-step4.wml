#use wml::debian::template title="Etapa 4: tarefas e habilidades" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="517d7a4f999561b8f28e207214a40990fdc3da49"

<p>As informações nesta página, embora públicas, são de interesse
principalmente dos(as) futuros(as) desenvolvedores(as) Debian.</p>

<h2>Etapa 4: tarefas e habilidades</h2>

<p>A maioria dos(as) membros(as) atuais do
<a href="./newmaint#DebianProject">projeto Debian</a> mantém um ou mais pacotes
para a distribuição.
No entanto, existem muitos outros trabalhos que precisam ser realizados que não
envolvem gerenciamento de pacotes.</p>

<p>O(A) <a href="./newmaint#AppMan">gestor(a) de candidaturas (AM)</a> combinará
com o(a) <a href="./newmaint#Applicant">candidato(a)</a> quais tarefas ele(a)
se voluntaria para executar. Depois disso, o(a) candidato(a) precisará
demonstrar suas habilidades nessa área.</p>

<p>As tarefas a seguir são exemplos óbvios dos vários trabalhos disponíveis
para o(a) candidato(a), mas não incluem necessariamente tudo o que um(a)
candidato(a) pode achar interessante e produtivo para o grupo. Tarefas
adicionais podem ser definidas pelo AM e pelo(a) candidato(a).</p>

<p>Alguns exemplos de tarefas são:</p>

<ul>
 <li><h4>Gerenciamento de pacotes</h4>
  Ao manter um pacote, um(a) futuro(a) desenvolvedor(a) pode mostrar seu
  entendimento da <a href="$(DOC)/debian-policy/">política Debian</a> e como
  ele(a) trabalha com usuários(as) Debian e remetentes de bugs.
 </li>

 <li><h4>Documentação</h4>
  O(A) candidato(a) pode demonstrar suas habilidades nessa área escrevendo
  páginas de manual para executáveis que não possuem uma, atualizando um
  documento desatualizado e criando uma nova documentação exigida pelos(as)
  usuários(as) mas que ainda está ausente na distribuição.

 </li>

 <li><h4>Depuração, teste e aplicação de patches</h4>
  O(A) candidato(a) pode mostrar habilidades nessa área trabalhando na
  correção de bugs com a equipe de QA, ou testando o processo de instalação, ou
  em pacotes individuais trabalhando com a equipe de testes. O(A) candidato(a)
  pode corrigir bugs em pacotes Debian existentes ou enviar relatórios de bugs
  para o BTS do Debian descrevendo problemas e anexando patches.

 </li>
</ul>

<p>Tarefas alternativas de demonstração podem ser elaboradas entre o(a)
candidato(a) e o(a) gestor(a) de candidatura. Tais tarefas alternativas
precisam ser coordenadas com a <a href="./newmaint#FrontDesk">secretaria</a>
e o(a)
<a href="./newmaint#DAM">gestor(a) de contas Debian</a>.</p>

<hr>
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
