#use wml::debian::template title="Debian GNU/Hurd --- Documentação" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="f4f6753d2f1e1d5bb9708ce8b3f7dde77940b870"

<h1>Debian GNU/Hurd</h1>

<p>O Dirk Ritter me enviou o texto a seguir que contém a saída da opção
<tt>--help</tt> de todo programa servidor do Hurd. Isto pode ser um bom
ponto de partida para documentações adicionais sobre programas servidores,
especialmente aqueles úteis para um(a) usuário(a), como ext2fs, ufs, isofs,
ftpfs, crash, etc.
</p>

<p>
Desde então, eu atualizei o texto usando excertos de e-mail das
listas de discussão do Hurd. Obrigado(a), Martin von Loewis.

<h2>Descrição preliminar da interface de usuário(a) do GNU/Hurd</h2>

<p>
Atualmente não existe quase nada, mas isso é melhor do que nada mesmo, então
por favor enviem reclamações, correções e adições para
<a href="mailto:dirk@gnumatic.s.bawue.de">Dirk Ritter</a>,
&lt;dirk@gnumatic.s.bawue.de&gt;. Por favor, note que minhas habilidades
de programação são <em>bem</em> limitadas, então você não deve esperar que eu
realmente entenda sobre coisas relacionadas a desenho e implementação
de sistemas operacionais.</p>

<table border="2"
       summary="Index of HURD servers and translators:">

<caption><em>Índice de servidores e tradutores do HURD:</em></caption>

<tr>
  <th><a href="#auth" name="TOC_auth" type="text/html">
      O servidor de autenticação</a></th>
  <th>&quot;<code>auth</code>&quot;</th>
</tr>
<tr>
  <th><a href="#crash" name="TOC_crash" type="text/html">
      O servidor crash</a></th>
  <th>&quot;<code>crash</code>&quot;</th>
</tr>
<tr>
  <th><a href="#exec" name="TOC_exec" type="text/html">
      O servidor exec</a></th>
  <th>&quot;<code>exec</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ext2fs" name="TOC_ext2fs" type="text/html">
      O servidor ext2fs</a></th>
  <th>&quot;<code>ext2fs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#fifo" name="TOC_fifo" type="text/html">
      O tradutor fifo</a></th>
  <th>&quot;<code>fifo</code>&quot;</th>
</tr>
<tr>
  <th><a href="#firmlink" name="TOC_firmlink" type="text/html">
      O tradutor firmlink</a></th>
  <th>&quot;<code>firmlink</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ftpfs" name="TOC_ftpfs" type="text/html">
      O tradutor de sistema de arquivo ftp</a></th>
  <th>&quot;<code>ftpfs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#fwd" name="TOC_fwd" type="text/html">
      O servidor fwd</a></th>
  <th>&quot;<code>fwd</code>&quot;</th>
</tr>
<tr>
  <th><a href="#hostmux" name="TOC_hostmux" type="text/html">
      O servidor hostmux</a></th>
  <th>&quot;<code>hostmux</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ifsock" name="TOC_ifsock" type="text/html">
      O servidor ifsock</a></th>
  <th>&quot;<code>ifsock</code>&quot;</th>
</tr>
<tr>
  <th><a href="#init" name="TOC_init" type="text/html">
      O servidor init</a></th>
  <th>&quot;<code>init</code>&quot;</th>
</tr>
<tr>
  <th><a href="#isofs" name="TOC_isofs" type="text/html">
      O servidor de arquivo de sistema iso</a></th>
  <th>&quot;<code>isofs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#magic" name="TOC_magic" type="text/html">
      O servidor magic</a></th>
  <th>&quot;<code>magic</code>&quot;</th>
</tr>
<tr>
  <th><a href="#new-fifo" name="TOC_new-fifo" type="text/html">
      O servidor new-fifo</a></th>
  <th>&quot;<code>new-fifo</code>&quot;</th>
</tr>
<tr>
  <th><a href="#nfs" name="TOC_nfs" type="text/html">
      O servidor nfs</a></th>
  <th>&quot;<code>nfs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#null" name="TOC_null" type="text/html">
      O servidor null</a></th>
  <th>&quot;<code>null</code>&quot;</th>
</tr>
<tr>
  <th><a href="#pfinet" name="TOC_pfinet" type="text/html">
      O servidor pfinet</a></th>
  <th>&quot;<code>pfinet</code>&quot;</th>
</tr>
<tr>
  <th><a href="#pflocal" name="TOC_pflocal" type="text/html">
      O servidor pflocal</a></th>
  <th>&quot;<code>pflocal</code>&quot;</th>
</tr>
<tr>
  <th><a href="#proc" name="TOC_proc" type="text/html">
      O servidor de processo</a></th>
  <th>&quot;<code>proc</code>&quot;</th>
</tr>
<tr>
  <th><a href="#storeio" name="TOC_storeio" type="text/html">
      O tradutor de armazenamento</a></th>
  <th>&quot;<code>storeio</code>&quot;</th>
</tr>
<tr>
  <th><a href="#symlink" name="TOC_symlink" type="text/html">
      O tradutor de link simbólico</a></th>
  <th>&quot;<code>symlink</code>&quot;</th>
</tr>
<tr>
  <th><a href="#term" name="TOC_term" type="text/html">
      O servidor de terminal</a></th>
  <th>&quot;<code>term</code>&quot;</th>
</tr>
<tr>
  <th><a href="#ufs" name="TOC_ufs" type="text/html">
      O servidor ufs</a></th>
  <th>&quot;<code>ufs</code>&quot;</th>
</tr>
<tr>
  <th><a href="#usermux" name="TOC_usermux" type="text/html">
      O servidor usermux</a></th>
  <th>&quot;<code>usermux</code>&quot;</th>
</tr>

# Uncomment and fill the blanks...
#<tr>
#  <th><a href="#" name="TOC_" type="text/html">
#      The  server</a></th>
#  <th>&quot;<code></code>&quot;</th>
#<tr>
</table>


<h2 class="center"><a href="#TOC_auth" name="auth" type="text/html">
O servidor de autenticação - &quot;<code>auth</code>&quot;</a></h2>

<p>
Passa credenciais quando dois servidores mutuamente não confiáveis se comunicam.
De certo modo, cada servidor auth estabelece um domínio de confiança.
(Martin von Loewis, 10 de outubro de 1996)</p>

<P>
Uma de suas funcionalidades interessantes é que ele torna possível que
processos personifiquem diversas identidades ao mesmo tempo, e também
que dinamicamente adquiram ou renunciem a identidades.</p>

<p>
Executar &quot;<code>/hurd/auth --help</code>&quot; exibe:
<br>
<pre>
Usage: auth [OPTION...]

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Report bugs to bug-hurd@gnu.org.
</pre>


<h2 class="center"><a href="#TOC_crash" name="crash" type="text/html">
O servidor crash - &quot;<code>crash</code>&quot;</a></h2>

<p>
O servidor crash fica ativo sempre que uma tarefa recebe um sinal de erro
fatal, por exemplo porque limites de memória foram violados (falha de
segmentação). O servidor crash tem três modos de operação: suspender o
grupo do processo (pgrp) da tarefa transgressora, terminá-la ou descarregar
um arquivo central. Este último ainda não está implementado. Pense no
servidor crash como um airbag.</p>
<p>
Você pode definir o modo de operação com <code>settrans</code>, claro, mas
também com <code>fsysopts</code> durante a execução. Isto é verdade para todo o
sistema padrão e geralmente requer privilégios de
root. Um(a) usuário(a) pode selecionar um padrão diferente com a variável
de ambiente <code>CRASHSERVER</code>. Você define essa variável para um
inode que tem um servidor <code>crash</code> anexado a ele. No sistema
padrão do Debian GNU/Linux, essas três operações padrões têm os respectivos
tradutores em <code>/servers/crash-*</code>.</p>

<pre>

 These options specify the disposition of a crashing process:
  -s, --suspend              Suspend the process
  -k, --kill                 Kill the process
  -c, --core-file            Dump a core file
      --dump-core
</pre>

<h2 class="center"><a href="#TOC_exec" name="exec" type="text/html">
O servidor de execução - &quot;<code>exec</code>&quot;</a></h2>

<p>
O exec gerencia a criação de uma nova imagem de processo de um arquivo de
imagem.</p>

<P>
Na verdade, este servidor tem suporte para criar uma imagem de processo em
execução a partir de qualquer arquivo de imagem reconhecido pela biblioteca
BFD (isto inclui a.out, ELF e muitos outros). Executáveis armazenados em gzip
também são reconhecidos (útil para boot em disquete).</p>

<p>
Executar &quot;<code>/hurd/exec --help</code>&quot; exibe:
<br>
<pre>
Usage: exec [OPTION...]
Hurd standard exec server

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_ext2fs" name="ext2fs" type="text/html">
O servidor de sistema de arquivo ext2 - &quot;<code>ext2fs</code>&quot;</a></h2>

<p>
Este servidor gerencia sistemas de arquivos do tipo ext2. Ele faz o mesmo
que <code>ext2fs.static</code>, mas o <code>ext2fs.static</code>
é um executável vinculado estaticamente.

<p>
Executar  &quot;<code>/hurd/ext2fs --help</code>&quot; exibe:
<br>
<pre>
Usage: ext2fs [OPTION...] DEVICE...

If neither --interleave or --layer is specified, multiple DEVICEs are
concatenated.

  -E, --no-exec              Don't permit any execution of files on this
                             filesystem
  -I, --interleave=BLOCKS    Interleave in runs of length BLOCKS
  -L, --layer                Layer multiple devices for redundancy
  -n, --no-sync              Don't automatically sync data to disk
  -r, --readonly             Never write to disk or allow opens for writing
  -s, --sync[=INTERVAL]      If INTERVAL is supplied, sync all data not
                             actually written to disk every INTERVAL seconds,
                             otherwise operate in synchronous mode (the default
                             is to sync every 30 seconds)
  -S, --no-suid              Don't permit set-uid or set-gid execution
  -T, --store-type=TYPE      Each DEVICE names a store of type TYPE
  -w, --writable             Use normal read/write behavior

 Boot options:
      --bootflags=FLAGS
  -C, --directory=DIRECTORY
      --device-master-port=PORT
      --exec-server-task=PORT
      --host-priv-port=PORT

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

If neither --interleave or --layer is specified, multiple DEVICEs are
concatenated.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_fifo" name="fifo" type="text/html">
O servidor fifo - &quot;<code>fifo</code>&quot;</a></h2>

<p>
O tradutor fifo implementa pipes nomeados.

<p>
Executar &quot;<code>/hurd/fifo --help</code>&quot; exibe:
<br>
<pre>
Usage: fifo [OPTION...]
Translator for fifos

  -d, --dgram                Reads reflect write record boundaries
  -m, --multiple-readers     Allow multiple simultaneous readers
  -n, --noblock              Don't block on open
  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_firmlink" name="firmlink" type="text/html">
O servidor firmlink - &quot;<code>firmlink</code>&quot;</a></h2>

<p>
Um tradutor para firmlinks.

<p>
Executar &quot;<code>/hurd/firmlink --help</code>&quot; exibe:
<br>
<pre>
Usage: firmlink [OPTION...] TARGET
A translator for firmlinks

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

A firmlink is sort of half-way between a symbolic link and a hard link;

Like a symbolic link, it is `by name', and contains no actual reference to the
target.  However, the lookup returns a node which will redirect parent lookups
so that attempts to find the cwd that go through the link will reflect the link
name, not the target name.  The target referenced by the firmlink is looked up
in the namespace of the translator, not the client.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_ftpfs" name="ftpfs" type="text/html">
O servidor de sistema de arquivo ftp - &quot;<code>ftpfs</code>&quot;</a></h2>

<p>
Um servidor para sistemas de arquivo ftp.

<p>
Executar &quot;<code>/hurd/ftpfs --help</code>&quot; exibe:
<br>
<pre>
Usage: ftpfs [OPTION...] REMOTE_FS [SERVER]
Hurd ftp filesystem translator

  -D, --debug[=FILE]         Print debug output to FILE

 Parameters:
      --bulk-stat-period=SECS   Period for detecting bulk stats (default 10)
      --bulk-stat-threshold=SECS   Number of stats within the bulk-stat-period
                             that trigger a bulk stat (default 5)
      --name-timeout=SECS    Time directory names are cached (default 300)
      --node-cache-size=ENTRIES   Number of recently used filesystem nodes that
                             are cached (default 50)
      --stat-timeout=SECS    Time stat information is cached (default 120)

  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

If SERVER is not specified, an attempt is made to extract it from REMOTE_FS,
using `SERVER:FS' notation.  SERVER can be a hostname, in which case anonymous
ftp is used, or may include a user and password like `USER:PASSWORD@HOST' (the
`:PASSWORD' part is optional).

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_fwd" name="fwd" type="text/html">
O servidor fwd - &quot;<code>fwd</code>&quot;</a></h2>

<p>
Quando acessado, o tradutor fwd encaminha requisições para outro servidor.
Ele é usado nos servidores fifo e symlink. A ideia é que você não tenha
um zilhão de servidores para coisas triviais; o fwd é usado para coordenar
a presença de um servidor que lida convenientemente com muitos e diferentes
nodes.

<p>
Executar &quot;<code>/hurd/fwd --help</code>&quot; exibe:
<br>
<pre>
Usage: /hurd/fwd SERVER [TRANS_NAME [TRANS_ARG...]]
</pre>



<h2 class="center"><a href="#TOC_hostmux" name="hostmux" type="text/html">
O servidor hostmux - &quot;<code>hostmux</code>&quot;</a></h2>

<p>
Eu não faço ideia do que faz esse servidor.

<small>
(Um servidor para procura de hosts?)
</small>

<p>
Executar &quot;<code>/hurd/hostmux --help</code>&quot; exibe:
<br>
<pre>
Usage: hostmux [OPTION...] TRANSLATOR [ARG...]
A translator for invoking host-specific translators

  -H, --host-pattern=PAT     The string to replace in the translator
                             specification with the hostname; if empty, or
                             doesn't occur, the hostname is appended as
                             additional argument instead (default `${host}')
  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

This translator appears like a directory in which hostnames can be looked up,
and will start TRANSLATOR to service each resulting node.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_ifsock" name="ifsock" type="text/html">
O servidor ifsock - &quot;<code>ifsock</code>&quot;</a></h2>

<p>
O <code>ifsock</code> somente lida com nodes de sistema de arquivo
<code>S_IFSOCK</code> para sistemas de arquivo que não fazem isso por si
mesmos, agindo como um hook sobre o qual pendurar endereços de socket de
domínio Unix.  pfinet e pflocal implementam o socket API.
(Thomas Bushnell, 10 de outubro de 1996)

<p>
Executar &quot;<code>/hurd/ifsock --help</code>&quot; exibe:
<br>
<pre>
/hurd/ifsock: Must be started as a translator
</pre>



<h2 class="center"><a href="#TOC_init" name="init" type="text/html">
O servidor de inicialização - &quot;<code>init</code>&quot;</a></h2>

<p>
Um servidor para procedimentos de boot de sistema e para configurações
básicas em tempo de execução.

<p>
Executar &quot;<code>/hurd/init --help</code>&quot; exibe:
<br>
<pre>
Usage: init [OPTION...]
Start and maintain hurd core servers and system run state

  -d, --debug
  -f, --fake-boot            This hurd hasn't been booted on the raw machine
  -n, --init-name
  -q, --query                Ask for the names of servers to start
  -s, --single-user          Startup system in single-user mode
  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_isofs" name="isofs" type="text/html">
O servidor de sistema de arquivo iso - &quot;<code>isofs</code>&quot;</a></h2>

<p>
Um servidor para sistemas de arquivo do tipo iso, normalmente usados para CDs.

<p>
Executar &quot;<code>/hurd/isofs --help</code>&quot; exibe:
<br>
<pre>
Usage: isofs [OPTION...] DEVICE...

If neither --interleave or --layer is specified, multiple DEVICEs are
concatenated.

  -E, --no-exec              Don't permit any execution of files on this
                             filesystem
  -I, --interleave=BLOCKS    Interleave in runs of length BLOCKS
  -L, --layer                Layer multiple devices for redundancy
  -n, --no-sync              Don't automatically sync data to disk
  -r, --readonly             Never write to disk or allow opens for writing
  -s, --sync[=INTERVAL]      If INTERVAL is supplied, sync all data not
                             actually written to disk every INTERVAL seconds,
                             otherwise operate in synchronous mode (the default
                             is to sync every 30 seconds)
  -S, --no-suid              Don't permit set-uid or set-gid execution
  -T, --store-type=TYPE      Each DEVICE names a store of type TYPE
  -w, --writable             Use normal read/write behavior

 Boot options:
      --bootflags=FLAGS
  -C, --directory=DIRECTORY
      --device-master-port=PORT
      --exec-server-task=PORT
      --host-priv-port=PORT

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

If neither --interleave or --layer is specified, multiple DEVICEs are
concatenated.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_magic" name="magic" type="text/html">
O servidor magic - &quot;<code>magic</code>&quot;</a></h2>

<p>
Um tradutor que retorna o resultado <code>MAGIC</code> da nova tentativa de
magic.

Usuários(as) finais normais provavelmente não precisam saber muito sobre ele,
já que é usado, por exemplo, para facilitar entrada/saída do terminal.
Programadores(as) podem se beneficiar das seguintes informações que
Thomas Bushnell forneceu:

<blockquote>
<p>
A questão aqui é saber o que é um &quot;magic retry result&quot;.
O que deve ser feito é examinar o <code>dir_lookup</code> <abbr>RPC</abbr>
documentado em <code>&lt;hurd/fs.defs&gt;</code> e
<code>&lt;hurd/hurd_types.defs&gt;</code>.

<br>
Os resultados de pesquisa do magic basicamente são para casos onde a busca
precisa de informação que é parte do estado de chamada do processo, e não é
parte do procedimento normal de busca por nome. Essas &quot;pinçadas
no estado da chamada&quot; têm que ser implementadas especialmente na
biblioteca C (veja <code>libc/hurd/hurdlookup.c</code>) e têm que cobrir
os casos conhecidos para fornecer a funcionalidade que muitos outros sistemas
oferecem.
</p>
</blockquote>

<p>
Ele gentilmente também explicou um comportamento particular que geralmente
confunde os(as) usuários(as) finais normais quando se deparam pela primeira
vez com ele:

<blockquote>
<p>
A incapacidade para fazer &quot;<kbd>ls /dev/fd</kbd>&quot; ocorre porque o
tradutor não sabe quais descritores de arquivo você tem que abrir, então ele
não pode te dizer qual existe. Mas o comportamento dele é exatamente como
em outros sistemas.
</p>
</blockquote>

<p>
Executar &quot;<code>/hurd/magic --help</code>&quot; exibe:
<br>
<pre>
Usage: magic [OPTION...] MAGIC
A translator that returns the magic retry result MAGIC

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_new-fifo" name="new-fifo" type="text/html">
O servidor new-fifo - &quot;<code>new-fifo</code>&quot;</a></h2>

<p>
Servidor alternativo para pipes nomeados.

<p>
Executar &quot;<code>/hurd/new-fifo --help</code>&quot; exibe:
<br>
<pre>
Usage: new-fifo [OPTION...]

  -d, --dgram                Reflect write record boundaries
  -n, --noblock              Don't block on open
  -r, --multiple-readers     Allow multiple simultaneous readers
  -s, --server               Operate in server mode
  -S, --standalone           Don't attempt to use a fifo server
  -U, --use-server=NAME      Attempt use server NAME
  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_nfs" name="nfs" type="text/html">
O servidor de sistema de arquivo de rede - &quot;<code>nfs</code>&quot;</a></h2>

<p>
Suporte ao sistema de arquivo de rede para o sistema de arquivo de rede da Sun.

<p>
Executar &quot;<code>/hurd/nfs --help</code>&quot; exibe:
<br>
<pre>
Usage: nfs [OPTION...] REMOTE_FS [HOST]
Hurd nfs translator

  -h, --hard                 Retry file systems requests until they succeed
  -s, --soft[=RETRIES]       File system requests will eventually fail, after
                             RETRIES tries if specified, otherwise 3
  -R, --read-size=BYTES, --rsize=BYTES
                             Max packet size for reads (default 8192)
  -W, --write-size=BYTES, --wsize=BYTES
                             Max packet size for writes (default 8192)

 Timeouts:
      --cache-timeout=SEC    Timeout for cached file data (default 3)
      --init-transmit-timeout=SEC
      --max-transmit-timeout=SEC
      --name-cache-neg-timeout=SEC
                             Timeout for negative directory cache entries
                             (default 3)
      --name-cache-timeout=SEC   Timeout for positive directory cache entries
                             (default 3)
      --stat-timeout=SEC     Timeout for cached stat information (default 3)

 Server specification:
      --default-mount-port=PORT   Port for mount server, if none can be found
                             automatically
      --default-nfs-port=PORT   Port for nfs operations, if none can be found
                             automatically
      --mount-port=PORT      Port for mount server
      --mount-program=ID[.VERS]
      --nfs-port=PORT        Port for nfs operations
      --nfs-program=ID[.VERS]
      --pmap-port=SVC|PORT

  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

If HOST is not specified, an attempt is made to extract it from REMOTE_FS,
using either the `HOST:FS' or `FS@HOST' notations.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_null" name="null" type="text/html">
O saco sem fundo - &quot;<code>null</code>&quot;</a></h2>

<p>
Um servidor com muito espaço livre e inumeráveis zeros, que implementa
<code>/dev/null</code> e <code>/dev/zero</code>.

<p>
Executar &quot;<code>/hurd/null --help</code>&quot; exibe:
<br>
<pre>
Usage: null [OPTION...]
Endless sink and null source

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_pfinet" name="pfinet" type="text/html">
O servidor de TCP/IP - &quot;<code>pfinet</code>&quot;</a></h2>

<p>
Um servidor para TCP/IP que implementa a família de protocolo PF_INET
(IPv4). O servidor que implementará a família de protocolo IPv6 será
chamado pfinet6 no atual esquema.</p>
<p>
Configurá-lo não é nada difícil. Ele é sempre colocado em
<code>/servers/socket/2</code>, porque lá é onde o glibc o procura.
Certifique-se de instalá-lo desta maneira:
<code>settrans /servers/socket/2 /hurd/pfinet -6 /servers/socket/26 --interface=/dev/eth0 OPÇÕES</code> e
<code>settrans /servers/socket/26 /hurd/pfinet -4 /servers/socket/2 --interface=/dev/eth0 OPÇÕES</code>,
onde <code>OPÇÕES</code>
especifica seu endereço IP, máscara de rede e gateway (se houver). Somente uma
interface de rede é suportada atualmente. Posteriormente, você também pode
definir interfaces adicionais como <code>eth1</code> e outras, com o mesmo
comando.</p>
<p>
Se você não tem uma placa de rede, você deve ao menos instalar uma interface
loopback de modo que localhost funcione (importante para enfileiramento de
impressão e outras coisas úteis). Faça isso com o comando acima, mas não
especifique qualquer interface ou <code>OPÇÕES</code>. Um simples
<code>settrans /servers/socket/1/hurd/pfinet</code> resolve.</p>
<p>
Executar &quot;<code>/hurd/pfinet --help</code>&quot; exibe:
<br>
<pre>
Swansea University Computer Society TCP/IP for NET3.019
IP Protocols: ICMP, UDP, TCP
Usage: pfinet [OPTION...]
Interface-specific options before the first interface specification apply to
the first following interface; otherwise they apply to the previously specified
interface.

  -i, --interface=DEVICE     Network interface to use

 These apply to a given interface:
  -a, --address=ADDRESS      Set the network address
  -g, --gateway=ADDRESS      Set the default gateway
  -m, --netmask=MASK         Set the netmask
  -s, --shutdown             Shut it down

  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_pflocal" name="pflocal" type="text/html">
O servidor pflocal - &quot;<code>pflocal</code>&quot;</a></h2>

<p>
Implementa sockets de domínio UNIX. Necessário para pipes, por exemplo.

<p>
Executar &quot;<code>/hurd/pflocal --help</code>&quot; exibe:
<br>
<pre>
Usage: /hurd/pflocal
</pre>

<h2 class="center"><a href="#TOC_proc" name="proc" type="text/html">
O servidor de processo - &quot;<code>proc</code>&quot;</a></h2>

<p>
O servidor proc atribui PIDs e estruturas de processo para tarefas, e gerencia
tudo no nível de processo como wait, bits de fork, suporte à biblioteca C.

<p>
Executar &quot;<code>/hurd/proc --help</code>&quot; exibe:
<br>
<pre>
Usage: proc [OPTION...]
Hurd process server

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_storeio" name="storeio" type="text/html">
O tradutor de armazenamento - &quot;<code>storeio</code>&quot;</a></h2>

<p>
Um tradutor para dispositivos e outros armazenamentos.

<p>
Executar &quot;<code>/hurd/storeio --help</code>&quot; exibe:
<br>
<pre>
Usage: storeio [OPTION...] DEVICE...
Translator for devices and other stores

  -I, --interleave=BLOCKS    Interleave in runs of length BLOCKS
  -L, --layer                Layer multiple devices for redundancy
  -n, --rdev=ID              The stat rdev number for this node; may be either
                             a single integer, or of the form MAJOR,MINOR
  -r, --readonly             Disallow writing
  -T, --store-type=TYPE      Each DEVICE names a store of type TYPE
  -w, --writable             Allow writing
  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

If neither --interleave or --layer is specified, multiple DEVICEs are
concatenated.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_symlink" name="symlink" type="text/html">
O servidor de link simbólico - &quot;<code>symlink</code>&quot;</a></h2>

<p>
Um servidor para links simbólicos para sistemas de arquivo que não têm
esse suporte.

<p>
Executar &quot;<code>/hurd/symlink --help</code>&quot; exibe:
<br>
<pre>
?
</pre>
(Não houve nenhuma saída? Estranho...)


<h2 class="center"><a href="#TOC_term" name="term" type="text/html">
O servidor de terminal - &quot;<code>term</code>&quot;</a></h2>

<p>
Implementa um terminal POSIX.

<p>
Executar &quot;<code>/hurd/term --help</code>&quot; exibe:
<br>
<pre>
Usage: term ttyname type arg
</pre>



<h2 class="center"><a href="#TOC_ufs" name="ufs" type="text/html">
O servidor ufs - &quot;<code>ufs</code>&quot;</a></h2>

<p>
Um servidor para sistemas de arquivo do tipo ufs. Ele faz o mesmo que
<code>ufs.static</code>, só que <code>ufs.static</code>
é um executável vinculado estaticamente.

<p>
Executar &quot;<code>/hurd/ufs --help</code>&quot; exibe:
<br>
<pre>
Usage: ufs [OPTION...] DEVICE...

If neither --interleave or --layer is specified, multiple DEVICEs are
concatenated.

  -C, --compat=FMT           FMT may be GNU, 4.4, or 4.2, and determines which
                             filesystem extensions are written onto the disk
                             (default is GNU)
  -E, --no-exec              Don't permit any execution of files on this
                             filesystem
  -I, --interleave=BLOCKS    Interleave in runs of length BLOCKS
  -L, --layer                Layer multiple devices for redundancy
  -n, --no-sync              Don't automatically sync data to disk
  -r, --readonly             Never write to disk or allow opens for writing
  -s, --sync[=INTERVAL]      If INTERVAL is supplied, sync all data not
                             actually written to disk every INTERVAL seconds,
                             otherwise operate in synchronous mode (the default
                             is to sync every 30 seconds)
  -S, --no-suid              Don't permit set-uid or set-gid execution
  -T, --store-type=TYPE      Each DEVICE names a store of type TYPE
  -w, --writable             Use normal read/write behavior

 Boot options:
      --bootflags=FLAGS
      --device-master-port=PORT
      --directory=DIRECTORY
      --exec-server-task=PORT
      --host-priv-port=PORT

  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

If neither --interleave or --layer is specified, multiple DEVICEs are
concatenated.

Report bugs to bug-hurd@gnu.org.
</pre>



<h2 class="center"><a href="#TOC_usermux" name="usermux" type="text/html">
O tradutor usermux - &quot;<code>usermux</code>&quot;</a></h2>

<p>
Um tradutor para invocar tradutores específicos a usuários(as).

<p>
Executar &quot;<code>/hurd/usermux --help</code>&quot; exibe:
<br>
<pre>
Usage: usermux [OPTION...] [TRANSLATOR [ARG...]]
A translator for invoking user-specific translators

  -C, --clear-patterns       Reset all patterns to empty; this option may then
                             be followed by options to set specific patterns
      --home-pattern=PAT     The string to replace in the translator
                             specification with the user's home directory
                             (default `${home}')
      --uid-pattern=PAT      The string to replace in the translator
                             specification with the uid (default `${uid}')
      --user-pattern=PAT     The string to replace in the translator
                             specification with the user name (default
                             `${user}')
  -?, --help                 Give this help list
      --usage                Give a short usage message

This translator appears like a directory in which user names can be looked up,
and will start TRANSLATOR to service each resulting node.  If no pattern occurs
in the translator specification, the users's home directory is appended to it
instead; TRANSLATOR defaults to /hurd/symlink.

Report bugs to bug-hurd@gnu.org.
</pre>



# Uncomment and fill the blanks...
#<h2 class="center"><a href="#TOC_" name="" type="text/html">
#The  server - &quot;<code></code>&quot;</a></h2>
#
#<p>
#A server for .
#
#<p>
#Running &quot;<code>/hurd/ --help</code>&quot; exibe:
#<br>
#<pre>
#
#</pre>
