# Brazilian Portuguese translation for Debian website newsevents.pot
# Copyright (C) 2003-2015 Software in the Public Interest, Inc.
#
# Philipe Gaspar <philipegaspar@terra.com.br>, 2002
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2007
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2015.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-02-04 14:55-0300\n"
"Last-Translator: Thiago Pezzo (tico) <pezzo@protonmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr "Notícias do Debian"

#: ../../english/News/news.rdf.in:19
msgid "Debian Latest News"
msgstr "Últimas Notícias do Debian"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "p<get-var page />"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr "O boletim informativo para a comunidade Debian"

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Próximas Atrações"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "o link pode não ser mais válido"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "Quando"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Onde"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Mais Informações"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Envolvimento do Debian"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Coordenador(a) Principal"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Coordenador(a) do</th><th>Projeto</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Links Relacionados"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Últimas Notícias"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr "Baixar entrada no calendário"

#: ../../english/template/debian/news.wml:9
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr ""
"Voltar para: outras <a href=\"./\">notícias do Debian</a> || <a href="
"\"m4_HOME/\">página inicial do Projeto Debian</a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (link quebrado)"

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""
"Bem-vindo(a) à <get-var issue /> edição deste ano da DPN, o boletim "
"informativo para a comunidade Debian. Os tópicos abordados nesta edição "
"incluem:"

#: ../../english/template/debian/projectnews/boilerplates.wml:43
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community."
msgstr ""
"Bem-vindo(a) à <get-var issue /> edição deste ano da DPN, o boletim "
"informativo para a comunidade Debian."

#: ../../english/template/debian/projectnews/boilerplates.wml:49
msgid "Other topics covered in this issue include:"
msgstr "Outros tópicos cobertos por esta edição incluem:"

#: ../../english/template/debian/projectnews/boilerplates.wml:69
#: ../../english/template/debian/projectnews/boilerplates.wml:90
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""
"De acordo com a <a href=\"https://udd.debian.org/bugs.cgi\">interface de "
"pesquisa de bugs do banco de dados Ultimate Debian Database</a>, a próxima "
"versão, Debian  <q><get-var release /></q>, é atualmente afetada por <get-"
"var testing /> bugs críticos ao lançamento (Release-Critical). Ignorando "
"bugs que são facilmente resolvidos ou a caminho de serem resolvidos, a "
"grosso modo em torno de <get-var tobefixed /> bugs \\\"Release-Critical\\\" "
"restam serem resolvidos para que o lançamento aconteça."

#: ../../english/template/debian/projectnews/boilerplates.wml:70
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""
"Existem também algumas <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">dicas de como interpretar</a> estes números."

#: ../../english/template/debian/projectnews/boilerplates.wml:91
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""
"Existem também <a href=\"<get-var url />\">estatísticas mais detalhadas</a>, "
"assim como algumas <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">dicas de como interpretar</a> esses números."

#: ../../english/template/debian/projectnews/boilerplates.wml:115
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""
"<a href=\"<get-var link />\">Atualmente</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> pacotes estão órfãos</a> e <a href=\"m4_DEVEL/wnpp/"
"rfa\"><get-var rfa /> pacotes estão para adoção</a>: por favor, visite a "
"lista completa de <a href=\"m4_DEVEL/wnpp/help_requested\">pacotes que "
"necessitam de sua ajuda</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:127
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""
"Por favor, ajude-nos a criar esse boletim informativo. Ainda precisamos de "
"mais escritores(as) voluntários(as) para observar a comunidade Debian e "
"relatar sobre o que está acontecendo. Por favor, veja a <a href=\"https://"
"wiki.debian.org/ProjectNews/HowToContribute\">página sobre contribuição</a> "
"para descobrir como ajudar. Estamos aguardando ansiosamente sua mensagem (em "
"inglês) em <a href=\"mailto:debian-publicity@lists.debian.org\">debian-"
"publicity@lists.debian.org</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:188
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""
"Por favor note que esta é uma seleção dos alertas de segurança mais "
"importantes das últimas semanas. Caso precise manter-se atualizado(a) sobre "
"os alertas de segurança divulgados pela Equipe de segurança do Debian, por "
"favor assine a <a href=\"<get-var url-dsa />\">lista de discussão de "
"segurança</a> (e a <a href=\"<get-var url-bpo />\">lista backports</a> "
"separada, além da <a href=\"<get-var url-stable-announce />\">lista de "
"atualizações da versão estável</a>) para anúncios."

#: ../../english/template/debian/projectnews/boilerplates.wml:189
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""
"Por favor note que esta é uma seleção dos alertas de segurança mais "
"importantes das últimas semanas. Caso precise manter-se atualizado(a) sobre "
"os alertas de segurança divulgados pela Equipe de segurança do Debian, por "
"favor assine a <a href=\"<get-var url-dsa />\">lista de discussão de "
"segurança</a> (e a <a href=\"<get-var url-bpo />\">lista backports</a> "
"separada, a <a href=\"<get-var url-stable-announce />\">lista de "
"atualizações da versão estável</a> ou a <a href=\"<get-var url-volatile-"
"announce />\">lista volatile</a>, para <q><get-var old-stable /></q>, a "
"distribuição estável antiga) para anúncios."

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""
"A Equipe de lançamento da versão estável (stable) do Debian publicou um "
"anúncio de atualização para o pacote: "

#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:226
#: ../../english/template/debian/projectnews/boilerplates.wml:357
#: ../../english/template/debian/projectnews/boilerplates.wml:371
msgid ", "
msgstr ", "

#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:227
#: ../../english/template/debian/projectnews/boilerplates.wml:358
#: ../../english/template/debian/projectnews/boilerplates.wml:372
msgid " and "
msgstr " e "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid ". "
msgstr ". "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid "Please read them carefully and take the proper measures."
msgstr "Por favor, leia-os com atenção e tome as medidas adequadas."

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid "Debian's Backports Team released advisories for these packages: "
msgstr "A Equipe de backports do Debian publicou alertas para estes pacotes: "

#: ../../english/template/debian/projectnews/boilerplates.wml:224
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""
"A Equipe de segurança do Debian recentemente publicou alertas para estes "
"pacotes (entre outros): "

#: ../../english/template/debian/projectnews/boilerplates.wml:253
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""
"<get-var num-newpkg /> pacotes foram adicionados ao repositório do Debian "
"instável (unstable) recentemente."

#: ../../english/template/debian/projectnews/boilerplates.wml:255
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr " <a href=\"<get-var url-newpkg />\">Entre muitos outros</a> são:"

#: ../../english/template/debian/projectnews/boilerplates.wml:282
msgid "There are several upcoming Debian-related events:"
msgstr "Existem vários eventos próximos relacionados ao Debian:"

#: ../../english/template/debian/projectnews/boilerplates.wml:288
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""
"Você pode encontrar mais informações sobre eventos relacionados e palestras "
"ao Debian na <a href=\"<get-var events-section />\">seção eventos</a> do "
"site web do Debian, ou assinar uma de nossas listas de discussão de eventos "
"para diferentes regiões: <a href=\"<get-var events-ml-eu />\">Europa</a>, <a "
"href=\"<get-var events-ml-nl />\">Holanda</a>, <a href=\"<get-var events-ml-"
"ha />\">América Hispânica</a>, <a href=\"<get-var events-ml-na />\">América "
"do Norte</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:313
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""
"Quer organizar um estande do Debian ou uma festa de instalação do Debian? "
"Está sabendo de outros eventos relacionados do Debian? Fez uma palestra do "
"Debian e quer adicionar o link em nossa <a href=\"<get-var events-talks />"
"\">página de palestras</a>? Envie um e-mail para a <a href=\"<get-var events-"
"team />\">equipe de eventos do Debian</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:335
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""
"<get-var dd-num /> pretendentes foram <a href=\"<get-var dd-url />"
"\">aceitos(as)</a> como Desenvolvedores(as) Debian"

#: ../../english/template/debian/projectnews/boilerplates.wml:342
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""
"<get-var dm-num /> pretendentes foram <a href=\"<get-var dm-url />"
"\">aceitos(as)</a> como Mantenedores(as) Debian"

#: ../../english/template/debian/projectnews/boilerplates.wml:349
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""
"<get-var uploader-num /> pessoas <a href=\"<get-var uploader-url />"
"\">começaram a manter pacotes</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""
"<get-var eval-newcontributors-text-list /> desde a edição anterior das "
"Notícias do Projeto Debian. Boas vindas <get-var eval-newcontributors-name-"
"list /> ao nosso projeto!"

#: ../../english/template/debian/projectnews/boilerplates.wml:407
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""
"A <get-var issue-devel-news /> edição das <a href=\"<get-var url-devel-news /"
">\">notícias diversas para desenvolvedores(as)</a> foi publicada e aborda os "
"seguintes tópicos:"

#: ../../english/template/debian/projectnews/footer.wml:6
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Para receber este boletim em sua caixa postal, <a href=\"https://lists."
"debian.org/debian-news-portuguese/\">inscreva-se na lista debian-news-"
"portuguese</a> (versão em português brasileiro). Para receber o boletim em "
"inglês, <a href=\"https://lists.debian.org/debian-news/\">inscreva-se na "
"lista debian-news</a>."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr ""
"<a href=\"../../\">Edições anteriores</a> deste boletim estão disponíveis."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />As Notícias do Projeto Debian são editadas por <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />As Notícias do Projeto Debian são editadas por <a href="
"\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Esta edição das Notícias do Projeto Debian foi "
"editada por <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Esta edição das Notícias do Projeto Debian foi editada "
"por <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr ""
"<void id=\"singular\" />Ela foi traduzida por <a href=\"mailto:debian-l10n-"
"portuguese@lists.debian.org\">%s</a>."

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr ""
"<void id=\"plural\" />Ela foi traduzida por <a href=\"mailto:debian-l10n-"
"portuguese@lists.debian.org\">%s</a>."

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr "<void id=\"singularfemale\" />Ela foi traduzida por %s."

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr "<void id=\"pluralfemale\" />Ela foi traduzida por %s."

#: ../../english/template/debian/weeklynews/footer.wml:6
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Para receber este boletim semanalmente em sua caixa postal, <a href="
"\"https://lists.debian.org/debian-news-portuguese/\">inscreva-se na lista "
"debian-news-portuguese</a> (versão em português brasileiro). Para receber o "
"boletim em inglês, <a href=\"https://lists.debian.org/debian-news/"
"\">inscreva-se na lista debian-news</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />As Notícias Semanais Debian são editadas por <a href="
"\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />As Notícias Semanais Debian são editadas por <a href="
"\"mailto:dwn@debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Esta edição das Notícias Semanais Debian foi editada "
"por <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Esta edição das Notícias Semanais Debian foi editada "
"por <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "Abstract"
#~ msgstr "Resumo"

#~ msgid "Author:"
#~ msgstr "Autor(a):"

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Voltar para a <a href=\"./\">página de palestrantes do Debian</a>."

#~ msgid "Date:"
#~ msgstr "Data:"

#~ msgid "Email:"
#~ msgstr "E-mail:"

#~ msgid "Event:"
#~ msgstr "Evento:"

#~ msgid "HTML"
#~ msgstr "HTML"

#~ msgid "Language:"
#~ msgstr "Idioma:"

#~ msgid "Languages:"
#~ msgstr "Idiomas:"

#~ msgid "List of Speakers"
#~ msgstr "Lista de Palestrantes"

#~ msgid "Location:"
#~ msgstr "Localização:"

#~ msgid "MagicPoint"
#~ msgstr "MagicPoint"

#~ msgid "Name:"
#~ msgstr "Nome:"

#~ msgid "PDF"
#~ msgstr "PDF"

#~ msgid "Previous Talks:"
#~ msgstr "Palestras Anteriores:"

#~ msgid "Slides:"
#~ msgstr "Slides:"

#~ msgid "Title:"
#~ msgstr "Título:"

#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Para receber este boletim quinzenalmente em sua caixa postal, <a href="
#~ "\"https://lists.debian.org/debian-news-portuguese/\">inscreva-se na lista "
#~ "debian-news-portuguese</a> (versão em Português Brasileiro). Para receber "
#~ "o boletim em inglês, <a href=\"https://lists.debian.org/debian-news/"
#~ "\">inscreva-se na lista debian-news</a>."

#~ msgid "Topics:"
#~ msgstr "Tópicos:"

#~ msgid "source"
#~ msgstr "source"
