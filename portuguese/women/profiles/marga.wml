<define-tag pagetitle>Margarita Manterola</define-tag>
#use wml::debian::profiles
#include "$(ENGLISHDIR)/women/profiles/profiles.def"
#use wml::debian::translation-check translation="08da87bf88f0132c810bd37c7312b5d0c4675a51"

<profile name="Margarita Manterola" picture="marga.jpg">
    <URL>https://www.marga.com.ar/blog</URL>
    <email>marga@debian.org</email>

    <question1>
    <answer><p>
    Eu uso o Debian desde 2000. No começo, eu era apenas uma usuária
    comum, mas com o tempo me envolvi mais, primeiro relatando bugs,
    depois enviando patches e, em 2004, comecei a manter pacotes.
    <br /><br /> 
    Um ponto de virada na minha vida foi a <a
    href="https://wiki.debian.org/DebConf4">DebConf4</a> no Brasil,
    onde eu pude conhecer muitos(as) desenvolvedores(as) Debian, colocar
    rostos nos nomes e aprender muito sobre como o Debian funciona.
    Eu recomendo que as pessoas participem da DebConf e conheçam
    pessoas lá. Já faz muito tempo, e a essa altura, tenho a
    sensação de que o pessoal do Debian faz parte da extensão da minha família.
    </p></answer>

    <question2>
    <answer><p>
    Sim. Eu me tornei uma desenvolvedora Debian em 13 de novembro de 2005.
    </p></answer>

    <question3>
    <answer><p>
    Eu mantenho alguns pacotes, mas não muitos, o principal em
    relação a empacotamento que faço é participar da equipe que
    mantém o ambiente de desktop Cinnamon.
    <br /><br /> 
    Além disso, estive envolvida ativamente na organização de
    várias DebConfs, particularmente na <a
    href="https://wiki.debian.org/DebConf8">DebConf8</a>, que ocorreu
    no meu país, Argentina. Mais tarde mudei-me para a Alemanha e
    também participei ativamente da organização da DebConf15, em
    Heidelberg. Além disso, gosto particularmente de fazer QA:
    fiz muitos NMUs para corrigir bugs de RC em
    pacotes que não eram adequados para um lançamento, e geralmente me
    divirto muito participando de festas de caça a bugs (bug squashing
    parties). Eu sou parte da equipe antiassédio, tentando fazer do
    Debian um lugar seguro, onde todas as pessoas sejam bem-vindas e possam se
    expressar. Também faço parte do Comitê Técnico, que é o órgão que
    ajuda a tomar decisões técnicas difíceis.
    </p></answer>

    <question4>
    <answer><p>
    A filosofia do software livre. Eu gostei do fato de ter sido
    desenvolvido por uma comunidade, e não por uma empresa, e que
    minhas contribuições poderiam ser aceitas se fossem corretas.<br />
    <br />Além disso, a mágica do apt-get e o imenso repositório. Eu
    normalmente tenho a percepção de que, se algo não está no Debian,
    não vale a pena usá-lo (e, se estiver, então posso assumir minha
    responsabilidade de garantir que ele seja empacotado e feito upload).
    O que me mantém interessada em trabalhar no Debian ao longo dos
    anos é que sempre há mais trabalho a ser feito, mais software para
    entrar no Debian, mais bugs para corrigir, mais novas ideias para
    experimentar.
    </p></answer>

    <question5>
    <answer><p>
    Há muitas coisas para fazer no Debian, e geralmente a parte mais
    difícil é encontrar onde você se encaixará. Por exemplo, se você
    gosta de programação, procure uma equipe que mantenha pacotes na
    linguagem de programação que você gosta e junte-se a ela. Se você
    é como eu e gosta de corrigir muitos bugs pequenos, consulte a
    lista de bugs e tente encontrar a solução para um.
    Existem muitos bugs fáceis, e as pessoas ficarão realmente
    agradecidas por você ter dedicado um tempo para corrigir os bugs.
    Mas mesmo se você não gosta de programar ou de correção de bugs,
    há muitas coisas a fazer. Precisamos de melhor design gráfico,
    precisamos de melhor documentação, precisamos de traduções e muitas outras
    coisas.
    </p></answer>
    
    <question7>
    # Um pouco mais sobre você...
    <answer><p>
    Tenho sido principalmente uma programadora Python há mais de 10 anos.
    Morei na Argentina até 2012 e depois me mudei para Munique, na Alemanha,
    para trabalhar no Google como engenheira de confiabilidade de site.
    <br /><br /> 
    Sou casada com Maximiliano Curia (outro DD) desde 2004 (DebConf4 foi
    a nossa viagem de lua de mel!)
    </p></answer>
</profile>
