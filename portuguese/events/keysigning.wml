#use wml::debian::template title="Assinatura de chaves"
#use wml::debian::translation-check translation="c4faf7d38264352c8c2861edac022312c173ab7e"

<p>Uma vez que diversos(as) desenvolvedores(as) encontram-se em feiras
ou conferências, estes eventos se tornaram uma boa maneira de
pessoas assinarem suas chaves OpenPGP e melhorar a cadeia de confiança.
Especialmente para pessoas que são novas no projeto, assinaturas de chaves
e encontros com outros(as) desenvolvedores(as) são muito interessantes.</p>

<p>Este documento tem como objetivo ajudar você com a organização de uma
sessão de assinatura de chaves. Note que todos os exemplos usam
<code>keyring.debian.org</code> como servidor de chaves. Se a chave em questão não
está no chaveiro do Debian, substitua <code>keyring.debian.org</code> por um servidor
público como por exemplo <code>keys.openpgp.org</code> (que é umm servir de
chaves válido.)</p>

<p>As pessoas devem assinar uma chave somente sob, pelo menos, duas condições</p>

<ol>
<li>O(A) proprietário(a) da chave convence o(a) assinador(a) que a identidade no
    UID é verdadeiramente sua própria identidade através de qualquer evidência
    que o(a) assinador(a) aceite como convincente. Normalmente isto significa
    que o(a) proprietário(a) da chave deve apresentar uma identificação expedida
    pelo governo com uma foto e informações que coincidam com o(a)
    proprietário(a) da chave. Alguns(mas) assinadores(as) sabem que
    identificações expedidas pelo governo são facilmente falsificadas e que a
    confiabilidade das autoridades emissoras é frequentemente suspeita e,
    portanto, podem solicitar evidência de identidade adicional e/ou alternativa.
</li>

<li>O(A) proprietário(a) da chave verifica que a <em>fingerprint</em> e o
tamanho da chave prestes a ser assinada é verdadeiramente sua.
</li>
</ol>

<p>
Mais importante, caso o(a) proprietário(a) da chave não esteja participando
ativamente na troca, você não estará apto a completar os requisitos 1 ou 2.
Ninguém pode completar a parte do(a) proprietário(a) da chave do
requisito 1 em nome do(a) proprietário(a) da chave, porque de outra forma
qualquer pessoa com uma identificação roubada poderia facilmente obter uma
chave OpenPGP para fingir ser um(a) representante do(a) dono(a) do chave.
Ninguém pode completar a parte do(a) proprietário(a) da chave do requisito 2
em nome do(a) proprietário(a) da chave, uma vez que o(a) representante poderia
 substituir a <em>fingerprint</em> por uma chave OpenPGP diferente com o nome
do(a) proprietário(a) da chave na mesma, e fazer com que alguém assine a
chave errada.
</p>

<ul>
<li> Você precisa de uma cópia impressa das <em>fingerprint</em> OpenPGP, dos
 tamanhos das chaves e um documento de identificação para provar sua
 identidade (passaporte, carteira de motorista ou similar).
</li>

<li> As <em>fingerprints</em> e tamanhos das chaves são dadas para outras
pessoas que devem assinar sua chave depois do encontro.
</li>

<li> Caso você não possua uma chave OpenPGP ainda, crie uma com o comando
     <code>gpg --gen-key</code>.
</li>

<li> Assine uma chave somente se a identidade da pessoa a qual você vai
     assinar a chave seja provada.
</li>

<li> Depois do encontro você terá que obter a chave OpenPGP para assiná-la.
     O comando abaixo pode ajudar:

<pre>
       gpg --keyserver keyring.debian.org --recv-keys 0xDEADBEEF
</pre>

     <p>Note que podemos usar os últimos oito dígitos hexadecimais da
       <em>fingerprint</em> nesta e em outras operações GnuPG. O <tt>0x</tt>
       na frente também é opcional.</p>
</li>

<li> Para assinar a chave, entre no menu de edição com

<pre>
       gpg --edit-key 0xDEADBEEF
</pre>
</li>

<li> No GnuPG selecione todas as UIDs para assinar com o comando
     <code>uid n</code>, onde <code>n</code> é o número da UID mostrada no
     menu. Você pode também pressionar enter para assinar todas as UIDs. </li>

<li> Para assinar uma chave, digite <code>sign</code>. Será então mostrada a
     <em>fingerprint</em> e o tamanho da chave que você deve comparar com os
 dados obtidos da pessoa que você encontrou pessoalmente.
</li>

<li> Quando perguntado sobre o nível de certificação, escolha "casual".
</li>

<li> Feche o GnuPG com <code>quit</code>
</li>

<li> Para verificar se você assinou a chave corretamente, você pode usar:

<pre>
       gpg --list-sigs 0xDEADBEEF
</pre>

     <p>Você deverá ver seu próprio nome e <em>fingerprint</em> (na forma
abreviada) na saída.</p>
</li>

<li> Uma vez que esteja tudo correto, você pode enviar a chave assinada
     para o(a) destinatário(a) usando:

<pre>
       gpg --export -a 0xDEADBEEF &gt; somepeople.key
</pre>

     <p>A opção <code>-a</code> exporta a chave no formato ASCII, assim a
mesma pode ser enviada via e-mail sem a possibilidade de corrompe-la.</p>
</li>

<li> Caso alguém assine sua chave desta maneira, você pode adicioná-la no
     chaveiro do Debian usando:

<pre>
       gpg --import --import-options merge-only mysigned.key
       gpg --keyserver keyring.debian.org --send-keys <var>&lt;your key id&gt;</var>
</pre>

     <p>Pode demorar um tempo até que os(as) mantenedores(as) do chaveiro
     atualizem a sua chave, portanto, seja paciente. Você deverá também fazer
o upload de sua chave atualizada para os servidores públicos de chaves.</p>
</li>
</ul>

<p>O pacote Debian <a
href="https://packages.debian.org/signing-party">signing-party</a>
(festa de assinaturas) oferece algumas ferramentas para ajudar você
com este processo. <tt>gpg-key2ps</tt> transforma uma chave OpenPGP em um
arquivo PostScript para imprimir bilhetes com a sua <em>fingerprint</em>, e
<tt>gpg-mailkeys</tt> vai enviar uma mensagem com uma chave
assinada para o(a) seu(sua) autor(a). O pacote também inclui o <tt>caff</tt>
que é uma ferramenta mais avançada. Veja a documentação do pacote
para mais informações.</p>

<h3>O que você não deve fazer</h3>

<p>Você nunca deve assinar uma chave para alguém que você não tenha
encontrado pessoalmente. Assinar uma chave baseado em qualquer outra
coisa que não seja o encontro pessoal, destrói a utilidade da
cadeia de confiança. Caso um(a) amigo(a) apresente o seu documento de
identificação e sua <em>fingerprint</em> para outros(as)
desenvolvedores(as), sem que você esteja lá para verificar que a
<em>fingerprint</em> pertence a você, o que os(as) outros(as)
 desenvolvedores(as) terão para relacionar a <em>fingerprint</em> com a
identificação? Eles têm apenas a palavra do(a) amigo(a), e as outras
assinaturas na sua chave -- isso não é melhor do que se eles assinassem sua
chave apenas porque outras pessoas assinaram!
</p>

<p>É legal ter mais assinaturas em uma chave, e é tentador cortar alguns
passos durante o processo. Mas possuir assinaturas confiáveis é mais
importante do que ter muitas assinaturas, portanto é muito importante
que mantenhamos o processo de assinaturas de chaves o mais puro possível.
Assinar a chave de alguém é uma confirmação de que você possui evidências
da identidade do portador da chave. Se você assinar sem realmente levar isso a
sério, a cadeia de confiança não poderá mais ser confiável.
</p>
