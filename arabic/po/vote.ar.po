# translation of templates.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-11-23 13:55+0000\n"
"Last-Translator: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language-Team: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 3.2.2\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "التاريخ"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "الوقت الزمني"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "الملخص"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "الترشيحات"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "مسحوب"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "النقاش"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "الأرصفة"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "مقترح"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "مقترح الاقتراح A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "مقترح الاقتراح B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "مقترح الاقتراح C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "مقترح الاقتراح D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "مقترح الاقتراح E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "مقترح الاقتراح F"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "مقترح الاقتراح G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "مقترح الاقتراح H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "الثواني"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "ثواني الاقتراح A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "ثواني الاقتراح B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "ثواني الاقتراح C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "ثواني الاقتراح D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "ثواني الاقتراح E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "ثواني الاقتراح F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "ثواني الاقتراح G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "ثواني الاقتراح H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "المعارضة"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "نص"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "الاقتراح A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "الاقتراح B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "الاقتراح C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "الاقتراح D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "الاقتراح E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "الاقتراح F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "الاقتراح G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "الاقتراح H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "خيارات"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "مُقترح التعديل"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "ثواني التعديل"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "نصّ التعديل"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "مُقترح التعديل أ"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "التعديلات أ"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "نصّ التعديل أ"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "مُقترح التعديل ب"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "التعديلات ب"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "نصّ التعديل ب"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "مُقترح التعديل C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "التعديلات C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "نصّ التعديل C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "التعديلات"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "الإجراءات"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "مطلب جماعي"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "بيانات وإحصائيات"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "النِصاب"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "الحدّ الأدنى للمناقشة"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "الاقتراع"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "المنتدى"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "الناتج"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "انتظار&nbsp;الرّعاة"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "قيد&nbsp;المناقشة"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "التصويت&nbsp;مفتوح"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "مُقرّر"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "مسحوب"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "أخرى"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "صفحة&nbsp;التصويت&nbsp;الرئيسيّة"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "طريقة&nbsp;عمل"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "إرسال&nbsp;اقتراح"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "إصلاح&nbsp;اقتراح"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "متابعة&nbsp;اقتراح"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "قراءة&nbsp;نتيجة"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "تصويت"
