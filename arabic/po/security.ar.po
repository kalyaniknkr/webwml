# translation of security.po to Arabic
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-11-23 13:54+0000\n"
"Last-Translator: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language-Team: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 3.2.2\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "أمن دبيان"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "تنبيهات دبيان الأمنية"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "س"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "قاموس Mitre CVE"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "قاعدة بيانات Securityfocus Bugtraq"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "بيانات CERT"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "ملاحظات نقاط ضعف US-CERT"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "المصدر:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "المكوّن غير المعتمد على البُنية:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"ملفات MD5 checksums للملفات المسردة متوفرة في <a href=\"<get-var url /"
">\">التقرير الأصلي</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"ملفات MD5 checksums للملفّات المسردة متوفرة في <a href=\"<get-var url /"
">\">التقرير المنقح</a>."

#: ../../english/template/debian/security.wml:44
msgid "Debian Security Advisory"
msgstr "تنبيه دبيان الأمني"

#: ../../english/template/debian/security.wml:49
msgid "Date Reported"
msgstr "تاريخ الإخبار"

#: ../../english/template/debian/security.wml:52
msgid "Affected Packages"
msgstr "حزم متأثرة"

#: ../../english/template/debian/security.wml:74
msgid "Vulnerable"
msgstr "ضعيف"

#: ../../english/template/debian/security.wml:77
msgid "Security database references"
msgstr "مراجع أمن قواعد البيانات"

#: ../../english/template/debian/security.wml:80
msgid "More information"
msgstr "مزيد من المعلومات"

#: ../../english/template/debian/security.wml:86
msgid "Fixed in"
msgstr "صُحح في"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "مُعرف BugTraq"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "علّة"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "في نظام تتبع علاّت دبيان:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr "في قاعدة بيانات Bugtraq (بموقع SecurityFocus):"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "في قاموس Mitre's CVE:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "نقاط ضعف CERT، إنذارات وملاحظات الحوادث:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr "لا توجد مراجع أمن قواعد بيانات خارجيّة متوفرة حاليا."
