msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:49+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Dátum"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Idővonal"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Összefoglaló"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Jelölések"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Visszavonások"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Vita"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Platformok"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Javaslattevő"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "A Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "B Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "C Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "D Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "E Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "F Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "G Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "H Javaslattevő Javaslata"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Támogatók"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "A javaslat támogatói"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "B javaslat támogatói"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "C javaslat támogatói"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "D javaslat támogatói"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "E javaslat támogatói"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "F javaslat támogatói"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "G javaslat támogatói"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "H javaslat támogatói"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Ellenzék"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Szöveg"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "A javaslat"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "B javaslat"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "C javaslat"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "D javaslat"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "E javaslat"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "F javaslat"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "G javaslat"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "H javaslat"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Választási lehetőségek"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Módosítás Javaslattevője"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Módosítás támogatói"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Módosítás szövege"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "A Módosítás javaslattevője"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "A módosítás támogatói"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "A Módosítás szövege"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "B Módosítás javaslattevője"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "B módosítás támogatói"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "B Módosítás szövege"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "C Módosítás javaslattevője"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "C módosítás támogatói"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "C Módosítás szövege"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Módosítások"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Eljárások"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Többségi követelmény"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Adat és Statisztika"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Határozatképesség"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Minimális vita"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Szavazás"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Fórum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Eredmény"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Szponzorokat&nbsp;keresünk"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Vita&nbsp;tárgya"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Szavazás alatt"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Eldőlt"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Visszavonva"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Egyéb"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Kezdőoldal - Szavazás"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Hogyan"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Adj be indítványt"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Módosíts indítványt"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Kövess indítványt"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Olvasd el a végeredményt"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Szavazz"
